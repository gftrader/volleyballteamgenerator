﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Domain;

namespace VolleyballTeamGenerator.Repository.EF
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private IDbContext context;
        public EFUnitOfWork(IDbContext context)
            : base()
        {
            this.context = context;
        }

        public IDbContext Context
        {
            get { return context; }
        }

        public void Commit()
        {
            context.Commit();
        }

        public void RegisterNew<T>(T entity, IUnitOfWorkRepository<T> uowr)  where T : class, IAggregateRoot
        {
            uowr.PersistCreationOf(entity);
        }

        public void RegisterAmmended<T>(T entity, IUnitOfWorkRepository<T> uowr) where T : class, IAggregateRoot
        {
            uowr.PersistUpdateOf(entity);
        }

        public void RegisterRemoved<T>(T entity, IUnitOfWorkRepository<T> uowr) where T : class, IAggregateRoot
        {
            uowr.PersistDeletionOf(entity);
        }
    }
}
