﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Domain;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Repository.EF
{
    public class VolleyballTeamGeneratorDataContext : DbContext, IDbContext
    {
        public VolleyballTeamGeneratorDataContext(string connectionString)
            : base(connectionString)
        {
        }

        public void Commit()
        {
            base.SaveChanges();
        }

        public new IDbSet<T> Set<T>() where T : class
        {
            return base.Set<T>();
        }
    }
}