﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain;
using VolleyballTeamGenerator.Domain.RepositoryInterfaces;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Repository.EF.Repositories
{
    public class PlayerGroupRepository : BaseRepository<PlayerGroup, int>, IPlayerGroupRepository
    {
        public PlayerGroupRepository(EFUnitOfWork uow) : base(uow) { }

        public override PlayerGroup FindById(int id)
        {
            return Context.Set<PlayerGroup>().FirstOrDefault(pg => pg.Id == id);
        }
    }
}
