﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Domain.RepositoryInterfaces;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Repository.EF.Repositories
{
    public class SavedPlayersListRepository : BaseRepository<SavedPlayersList, int>, ISavedPlayersListRepository
    {
        public SavedPlayersListRepository(EFUnitOfWork uow) : base(uow) { }

        public override SavedPlayersList FindById(int id)
        {
            return Context.Set<SavedPlayersList>().FirstOrDefault(spl => spl.Id == id);
        }

        public SavedPlayersList FindByGroupIdAndName(int playerGroupId, string savedPlayersListName)
        {
            return Context.Set<SavedPlayersList>().FirstOrDefault(spl => spl.PlayerGroupId == playerGroupId && spl.Name.Equals(savedPlayersListName));
        }

        public IList<SavedPlayersList> FindByPlayerGroupId(int playerGroupID)
        {
            return Context.Set<SavedPlayersList>().Where(spl => spl.PlayerGroupId == playerGroupID).OrderBy(spl => spl.Name).ToList();
        }
    }
}