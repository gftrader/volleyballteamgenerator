﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain;
using VolleyballTeamGenerator.Domain.RepositoryInterfaces;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Repository.EF.Repositories
{
    public class VolleyballAttributeRepository : BaseRepository<VolleyballAttribute, int>, IVolleyballAttributeRepository
    {
        public VolleyballAttributeRepository(EFUnitOfWork uow) : base(uow) { }

        public VolleyballAttribute FindByName(string skillName)
        {
            return Context.Set<VolleyballAttribute>().FirstOrDefault(va => va.Name.Equals(skillName));
        }

        public override VolleyballAttribute FindById(int id)
        {
            return Context.Set<VolleyballAttribute>().FirstOrDefault(va => va.Id == id);
        }
    }
}