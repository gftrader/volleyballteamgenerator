﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain;
using VolleyballTeamGenerator.Domain.Messages;
using VolleyballTeamGenerator.Domain.RepositoryInterfaces;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Repository.EF.Repositories
{
    public class PlayerRepository : BaseRepository<Player, int>, IPlayerRepository
    {
        public PlayerRepository(EFUnitOfWork uow) : base(uow) { }

        public override Player FindById(int id)
        {
            return Context.Set<Player>().FirstOrDefault(p => p.Id == id);
        }

        public Player FindByName(string firstName, string LastName)
        {
            return Context.Set<Player>().FirstOrDefault(p => p.FirstName.Equals(firstName) && p.LastName.Equals(LastName));
        }

        public IList<Player> FindPlayers(FindPlayersRequest fpRequest)
        {
            var players = Context.Set<Player>().AsQueryable();
            if (!String.IsNullOrEmpty(fpRequest.FirstName)) { players = players.Where(p => p.FirstName.Contains(fpRequest.FirstName)); }
            if (!String.IsNullOrEmpty(fpRequest.LastName)) { players = players.Where(p => p.LastName.Contains(fpRequest.LastName)); }
            if (!String.IsNullOrEmpty(fpRequest.Gender)) { players = players.Where(p => p.Gender.Equals(fpRequest.Gender)); }
            if (!String.IsNullOrEmpty(fpRequest.Email)) { players = players.Where(p => p.Email.Contains(fpRequest.Email)); }
            if(fpRequest.PlayerIds.Count() > 0)
            {
                players = players.Where(p => fpRequest.PlayerIds.Contains(p.Id));
            }
            if (fpRequest.PlayerGroupIds.Count() > 0)
            {
                players = players.Where(p => p.PlayerGroups.Any(pg => fpRequest.PlayerGroupIds.Contains(pg.Id)));
            }
            return players.OrderBy(p => p.FirstName)
                          .ThenBy(p => p.LastName)
                          .ToList();
        }
    }
}