﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Domain.RepositoryInterfaces;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Repository.EF.Repositories
{
    public class SavedPlayersListsTeamsListRepository : BaseRepository<SavedPlayersListsTeamsList, int>, ISavedPlayersListsTeamsListRepository
    {
        public SavedPlayersListsTeamsListRepository(EFUnitOfWork uow) : base(uow) { }

        public override SavedPlayersListsTeamsList FindById(int id)
        {
            return Context.Set<SavedPlayersListsTeamsList>().FirstOrDefault(spltl => spltl.Id == id);
        }

        public SavedPlayersListsTeamsList FindBySavedPlayersListIdAndName(int savedPlayerListId, string listName)
        {
            return Context.Set<SavedPlayersListsTeamsList>().FirstOrDefault(spltl => spltl.SavedPlayersListId == savedPlayerListId && spltl.Name.Equals(listName));
        }

        public IList<SavedPlayersListsTeamsList> FindBySavedPlayersList(int savedPlayerListId)
        {
            return Context.Set<SavedPlayersListsTeamsList>().Where(spltl => spltl.SavedPlayersListId == savedPlayerListId).ToList();
        }
    }
}