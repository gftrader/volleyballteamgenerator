﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Domain.RepositoryInterfaces;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Repository.EF.Repositories
{
    public class SavedPlayersListsTeamsLists_TeamRepository : BaseRepository<SavedPlayersListsTeamsLists_Team, int>, ISavedPlayersListsTeamsLists_TeamRepository
    {
        public SavedPlayersListsTeamsLists_TeamRepository(EFUnitOfWork uow) : base(uow) { }

        public override SavedPlayersListsTeamsLists_Team FindById(int id)
        {
            return Context.Set<SavedPlayersListsTeamsLists_Team>().FirstOrDefault(spltlt => spltlt.Id == id);
        }

        public new IList<SavedPlayersListsTeamsLists_Team> FindAll()
        {
            return Context.Set<SavedPlayersListsTeamsLists_Team>().ToList();
        }
    }
}