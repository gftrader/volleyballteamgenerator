﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Repository.EF.Repositories
{
    public abstract class BaseRepository<T, TID> : IRepository<T, TID>, IUnitOfWorkRepository<T> where T : class, IAggregateRoot
    {
        private EFUnitOfWork uow;
        public IDbContext Context 
        {
            get { return uow.Context; } 
        }

        public BaseRepository(EFUnitOfWork uow)
        {
            this.uow = uow;
        }

        public abstract T FindById(TID id);
        public IList<T> FindAll()
        {
            return Context.Set<T>().ToList();
        }

        public void Add(T entity)
        {
            uow.RegisterNew(entity, this);
        }

        public void Save(T entity)
        {
            uow.RegisterAmmended(entity, this);
        }

        public void Remove(T entity)
        {
            uow.RegisterRemoved(entity, this);
        }

        public void PersistCreationOf(T entity)
        {
            Context.Set<T>().Add(entity);
        }

        public void PersistUpdateOf(T entity)
        {
            //do nothing in EF
        }

        public void PersistDeletionOf(T entity)
        {
            Context.Set<T>().Remove(entity);
        }
    }
}