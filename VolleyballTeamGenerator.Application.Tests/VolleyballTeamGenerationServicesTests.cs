﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit;
using NUnit.Framework;
using Rhino.Mocks;

using VolleyballTeamGenerator.Application.Presenters;
using VolleyballTeamGenerator.Application.ViewInterfaces;
using VolleyballTeamGenerator.Application.ViewModels;
using VolleyballTeamGenerator.Domain.RepositoryInterfaces;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Domain.Services;

namespace VolleyballTeamGenerator.Application.Tests
{
    [TestFixture]
    public class VolleyballTeamGenerationServicesTests
    {
        MockRepository mr = null;

        [SetUp]
        public void SetupVolleyballTeamGenerationServicesTests()
        {
            mr = new MockRepository();
        }

        private List<Player> GetFakePlayerDatabase()
        {
            return new List<Player>()
            {
                BuildFakePlayer(1, "Player", "A", "player.a@gmail.com",    6, 7, 6, 6, 7, 7, 4), //
                BuildFakePlayer(2, "Player", "B", "player.b@gmail.com",    6, 8, 5, 6, 7, 4, 9),
                BuildFakePlayer(3, "Player", "C", "player.c@gmail.com",    5, 5, 8, 10, 7, 6, 5),
                BuildFakePlayer(4, "Player", "D", "player.d@gmail.com",    4, 4, 6, 6, 4, 2, 3),
                BuildFakePlayer(5, "Player", "E", "player.e@gmail.com",    6, 7, 6, 6, 6, 5, 6),
                BuildFakePlayer(6, "Player", "F", "player.f@gmail.com",    3, 3, 3, 5, 4, 4, 3),
                BuildFakePlayer(7, "Player", "G", "player.g@gmail.com",    8, 7, 5, 6, 7, 7, 6),
                BuildFakePlayer(8, "Player", "H", "player.h@gmail.com",    5, 4, 4, 6, 6, 6, 5),
                BuildFakePlayer(9, "Player", "I", "player.i@gmail.com",    5, 7, 4, 4, 4, 4, 6),
                BuildFakePlayer(10, "Player", "J", "player.j@gmail.com",   7, 6, 4, 5, 5, 5, 7),
                BuildFakePlayer(11, "Player", "K", "player.k@gmail.com",   10, 8, 6, 8, 7, 7, 8),
                BuildFakePlayer(12, "Player", "L", "player.l@gmail.com",   6, 7, 3, 5, 5, 6, 7),
                BuildFakePlayer(13, "Player", "M", "player.m@gmail.com",   4, 7, 4, 5, 4, 6, 5),
                BuildFakePlayer(14, "Player", "N", "player.n@gmail.com",   9, 8, 7, 8, 8, 9, 5),
                BuildFakePlayer(15, "Player", "O", "player.o@gmail.com",   3, 3, 7, 6, 7, 5, 3),
                BuildFakePlayer(16, "Player", "P", "player.p@gmail.com",   7, 7, 7, 7, 7, 7, 6),
                BuildFakePlayer(17, "Player", "Q", "player.q@gmail.com",   7, 7, 5, 7, 7, 5, 7),
                BuildFakePlayer(18, "Player", "S", "player.s@gmail.com",   4, 3, 4, 3, 4, 4, 4),
                BuildFakePlayer(19, "Player", "T", "player.t@gmail.com",   4, 4, 5, 6, 6, 5, 4),
                BuildFakePlayer(20, "Player", "U", "player.u@gmail.com",   6, 6, 6, 7, 7, 7, 4),
                BuildFakePlayer(21, "Player", "V", "player.v@gmail.com",   8, 8, 6, 8, 7, 6, 8),
                BuildFakePlayer(27, "Player", "W", "player.w@gmail.com",   4, 5, 3, 6, 5, 4, 5),
                BuildFakePlayer(34, "Player", "X", "player.x@gmail.com",   3, 2, 7, 6, 6, 4, 3),
            };
        }

        private List<Player> GetEdsTestDatabase()
        {
            return new List<Player>()
            {
                BuildFakePlayer(4, "Player", "D", "player.d@gmail.com",    4, 4, 6, 6, 4, 2, 3),
                BuildFakePlayer(21, "Player", "V", "player.v@gmail.com",   8, 8, 6, 8, 7, 6, 8),
                BuildFakePlayer(12, "Player", "L", "player.l@gmail.com",   6, 7, 3, 5, 5, 6, 7),
                BuildFakePlayer(10, "Player", "J", "player.j@gmail.com",   7, 6, 4, 5, 5, 5, 7),
                BuildFakePlayer(8, "Player", "H", "player.h@gmail.com",    5, 4, 4, 6, 6, 6, 5),
                BuildFakePlayer(18, "Player", "S", "player.s@gmail.com",   4, 3, 4, 3, 4, 4, 4),
                BuildFakePlayer(14, "Player", "N", "player.n@gmail.com",   9, 8, 7, 8, 8, 9, 5),
                BuildFakePlayer(9, "Player", "I", "player.i@gmail.com",    5, 7, 4, 4, 4, 4, 6),

                BuildFakePlayer(22, "Player", "Y", "player.y@gmail.com",   4, 4, 4, 4, 4, 7, 5),
                BuildFakePlayer(23, "Player", "Z", "player.z@gmail.com",   5, 5, 6, 6, 6, 7, 4),
                BuildFakePlayer(24, "Player", "AA", "player.aa@gmail.com", 5, 6, 7, 6, 6, 6, 7),
                BuildFakePlayer(25, "Player", "AB", "player.ab@gmail.com", 4, 4, 5, 6, 5, 5, 4),
                BuildFakePlayer(26, "Player", "AC", "player.ac@gmail.com", 3, 2, 7, 7, 6, 3, 2),

                BuildFakePlayer(27, "Player", "W", "player.w@gmail.com",   4, 5, 3, 6, 5, 4, 5),
                BuildFakePlayer(28, "Player", "AD", "player.ad@gmail.com", 5, 6, 6, 5, 5, 6, 3),
                BuildFakePlayer(29, "Player", "AE", "player.ae@gmail.com", 4, 3, 5, 5, 5, 4, 3),
                BuildFakePlayer(30, "Player", "AF", "player.af@gmail.com", 6, 5, 6, 6, 6, 6, 4),    
                BuildFakePlayer(31, "Player", "AG", "player.ag@gmail.com", 8, 8, 7, 7, 7, 6, 8),
                BuildFakePlayer(32, "Player", "AH", "player.ah@gmail.com", 4, 2, 5, 5, 5, 6, 3),
                BuildFakePlayer(33, "Player", "AI", "player.ai@gmail.com", 5, 6, 3, 3, 4, 4, 7),
                BuildFakePlayer(34, "Player", "X", "player.x@gmail.com",   3, 2, 7, 6, 6, 4, 3),
                BuildFakePlayer(35, "Player", "AJ", "player.aj@gmail.com", 6, 7, 4, 5, 5, 5, 7),

                //BuildFakePlayer(1, "Player", "A", "player.a@gmail.com",  6, 7, 6, 6, 7, 7, 4),   //
                //BuildFakePlayer(2, "Player", "B", "player.b@gmail.com",  6, 8, 5, 6, 7, 4, 9),
                //BuildFakePlayer(3, "Player", "C", "player.c@gmail.com",  5, 5, 8, 10, 7, 6, 5),                
                //BuildFakePlayer(5, "Player", "E", "player.e@gmail.com",  6, 7, 6, 6, 6, 5, 6),
                //BuildFakePlayer(6, "Player", "F", "player.f@gmail.com",  3, 3, 3, 5, 4, 4, 3),
                //BuildFakePlayer(7, "Player", "G", "player.g@gmail.com",  8, 7, 5, 6, 7, 7, 6),                
                //BuildFakePlayer(11, "Player", "K", "player.k@gmail.com", 10, 8, 6, 8, 7, 7, 8),
                //BuildFakePlayer(13, "Player", "L", "player.l@gmail.com", 4, 7, 4, 5, 4, 6, 5),
                //BuildFakePlayer(14, "Player", "M", "player.m@gmail.com", 9, 8, 7, 8, 8, 9, 5),
                //BuildFakePlayer(15, "Player", "N", "player.n@gmail.com", 3, 3, 7, 6, 7, 5, 3),
                //BuildFakePlayer(16, "Player", "O", "player.o@gmail.com", 7, 7, 7, 7, 7, 7, 6),
                //BuildFakePlayer(17, "Player", "P", "player.p@gmail.com", 7, 7, 5, 7, 7, 5, 7),
                
                //BuildFakePlayer(19, "Player", "R", "player.r@gmail.com", 4, 4, 5, 6, 6, 5, 5),
                //BuildFakePlayer(20, "Player", "S", "player.s@gmail.com", 6, 6, 6, 7, 7, 7, 4),
            };
        }

        private Player BuildFakePlayer(int id, string firstName, string lastName, string email, int hitting, int blocking, int setting, int vbIQ, int courtPosition, int quickness, int height)
        {
            Player newPlayer = new Player()
            {
                Id = id,
                FirstName = firstName,
                LastName = lastName,
                Email = email,
            };

            newPlayer.PlayerAttributes = BuildFakeSkills(newPlayer, hitting, blocking, setting, vbIQ, courtPosition, quickness, height);
            return newPlayer;
        }

        private List<PlayerAttribute> BuildFakeSkills(Player player, int hitting, int blocking, int setting, int vbIQ, int courtPosition, int quickness, int height)
        {
            List<PlayerAttribute> attributes = new List<PlayerAttribute>()
            {
                BuildFakePlayerSkill(player, VolleyballAttributeEnum.Hitting, hitting),
                BuildFakePlayerSkill(player, VolleyballAttributeEnum.Blocking, blocking),
                BuildFakePlayerSkill(player, VolleyballAttributeEnum.Setting, setting),
                BuildFakePlayerSkill(player, VolleyballAttributeEnum.VolleyballIQ, vbIQ),
                //BuildFakePlayerSkill(player, VolleyballAttributeEnum.CourtPosition, courtPosition),
                BuildFakePlayerSkill(player, VolleyballAttributeEnum.Mobility, quickness),
                BuildFakePlayerSkill(player, VolleyballAttributeEnum.Verticality, height)
            };

            attributes.Add(BuildFakePlayerSkill(player, VolleyballAttributeEnum.Overall, attributes.Sum(attr => attr.VolleyballAttributeLevel)));
            return attributes;
        }

        private PlayerAttribute BuildFakePlayerSkill(Player player, VolleyballAttributeEnum skillEnum, double value)
        {
            return new PlayerAttribute() { VolleyballAttribute = BuildFakeSkill((int)skillEnum, Enum.GetName(typeof(VolleyballAttributeEnum), skillEnum)), VolleyballAttributeLevel = value, Player = player };
        }

        private VolleyballAttribute BuildFakeSkill(int id, string name)
        {
            return new VolleyballAttribute() { Id = id, Name = name };
        }

        [Test]
        public void TestRandomTeamStrategy()
        {
        //    IPlayerRepository playerRepository = mr.DynamicMock<IPlayerRepository>();
        //    IPlayerGroupRepository playerGroupRepository = mr.DynamicMock<IPlayerGroupRepository>();
        //    TeamGenerationDomainServices tgds = new TeamGenerationDomainServices(playerRepository, playerGroupRepository);
        //    IList<Player> playerDB = GetFakePlayerDatabase();
        //    //List<Player> playerDB = GetEdsTestDatabase();
        //    IList<PlayerView> playerViews = PlayerMap.ConvertToPlayerViews(playerDB);
        //    playerRepository.Stub(pr => pr.FindAllByPlayerNames(Arg<List<Player>>.Is.Anything)).Return(playerDB);

        //    IGenerateTeamsView gtView = mr.Stub<IGenerateTeamsView>();
        //    gtView.Stub(gtv => gtv.NumberOfTeams).Return(4);
        //    gtView.Stub(gtv => gtv.PlayerGroupID).Return(1);
        //    gtView.Stub(gtv => gtv.TeamCompositionStrategy).Return(CompositionStrategyViewEnum.Random);
        //    gtView.Stub(gtv => gtv.PlayerViews).Return(playerViews);

        //    mr.ReplayAll();
        //    GenerateTeamsPresenter gtPresenter = new GenerateTeamsPresenter(gtView, tgds);
        //    gtPresenter.GenerateTeams();
            
            //int resultingPlayerCount = gtView.TeamViews.Sum(tv => tv.PlayerViews.Count);

            //PrintTeams(gtView.TeamViews);

            //Assert.AreEqual(playerViews.Count, resultingPlayerCount, "Player count should match original count");
        }

        [Test]
        public void TestBalancedTeamStrategy()
        {
            /*IPlayerRepository playerRepository = mr.DynamicMock<IPlayerRepository>();
            TeamGenerationDomainServices tgds = new TeamGenerationDomainServices(playerRepository);
            List<Player> playerDB = GetFakePlayerDatabase();
            //List<Player> playerDB = GetEdsTestDatabase();
            List<PlayerView> playerViews = PlayerMap.ConvertToPlayerViews(playerDB);
            playerRepository.Stub(pr => pr.FindAllByPlayerNames(Arg<List<PlayerName>>.Is.Anything)).Return(playerDB);

            GenerateTeamsApplicationRequest gtReq = new GenerateTeamsApplicationRequest()
            {
                NumberOfTeams = 4,
                TeamCompositionStrategy = CompositionStrategyViewEnum.Balanced,
                PlayerViews = playerViews
            };

            mr.ReplayAll();
            TeamGenerationApplicationServices vtgs = new TeamGenerationApplicationServices(tgds);
            GenerateTeamsApplicationResponse gtRes = vtgs.GenerateTeams(gtReq);

            PrintTeams(gtRes.TeamViews);

            Assert.AreEqual(playerViews.Count, gtRes.TeamViews.Sum(tv => tv.PlayerViews.Count), "Player count should match original count");
            
            //Assert.IsTrue(gtRes.TeamViews[0].PlayerViews.Count(pv => pv.Id == 11) > 0, "Kelly should be on team 1");
            //Assert.IsTrue(gtRes.TeamViews[0].PlayerViews.Count(pv => pv.Id == 5) > 0, "Jolly should be on team 1");
            //Assert.IsTrue(gtRes.TeamViews[0].PlayerViews.Count(pv => pv.Id == 1) > 0, "Gage should be on team 1");
            //Assert.IsTrue(gtRes.TeamViews[0].PlayerViews.Count(pv => pv.Id == 8) > 0, "Alyssa should be on team 1");
            //Assert.IsTrue(gtRes.TeamViews[0].PlayerViews.Count(pv => pv.Id == 10) > 0, "Harvey should be on team 1");
            //Assert.IsTrue(gtRes.TeamViews[0].PlayerViews.Count(pv => pv.Id == 6) > 0, "Maureen should be on team 1");

            //Assert.IsTrue(gtRes.TeamViews[1].PlayerViews.Count(pv => pv.Id == 7) > 0, "Arune should be on team 2");
            //Assert.IsTrue(gtRes.TeamViews[1].PlayerViews.Count(pv => pv.Id == 3) > 0, "Dan should be on team 2");
            //Assert.IsTrue(gtRes.TeamViews[1].PlayerViews.Count(pv => pv.Id == 12) > 0, "Tom should be on team 2");
            //Assert.IsTrue(gtRes.TeamViews[1].PlayerViews.Count(pv => pv.Id == 2) > 0, "Jonathan should be on team 2");
            //Assert.IsTrue(gtRes.TeamViews[1].PlayerViews.Count(pv => pv.Id == 9) > 0, "Chris should be on team 2");
            //Assert.IsTrue(gtRes.TeamViews[1].PlayerViews.Count(pv => pv.Id == 4) > 0, "Ed should be on team 2");
            */
        }

        [Test]
        public void TestBPATeamStrategy()
        {
            /*
            IPlayerRepository playerRepository = mr.DynamicMock<IPlayerRepository>();
            TeamGenerationDomainServices tgds = new TeamGenerationDomainServices(playerRepository);
            List<Player> playerDB = GetFakePlayerDatabase();
            //List<Player> playerDB = GetEdsTestDatabase();
            List<PlayerView> playerViews = PlayerMap.ConvertToPlayerViews(playerDB);
            playerRepository.Stub(pr => pr.FindAllByPlayerNames(Arg<List<PlayerName>>.Is.Anything)).Return(playerDB);

            GenerateTeamsApplicationRequest gtReq = new GenerateTeamsApplicationRequest()
            {
                NumberOfTeams = 4,
                TeamCompositionStrategy = CompositionStrategyViewEnum.BestPlayerAvailable,
                PlayerViews = playerViews
            };

            mr.ReplayAll();
            TeamGenerationApplicationServices vtgs = new TeamGenerationApplicationServices(tgds);
            GenerateTeamsApplicationResponse gtRes = vtgs.GenerateTeams(gtReq);

            PrintTeams(gtRes.TeamViews);

            Assert.AreEqual(playerViews.Count, gtRes.TeamViews.Sum(tv => tv.PlayerViews.Count), "Player count should match original count");
             * */
        }

        private void PrintTeams(IList<TeamView> teamViews)
        {
            foreach (TeamView tv in teamViews)
            {
                foreach (PlayerSummaryView pv in tv.Players)
                {
                    Console.WriteLine("{0}: Player: {1} {2} - {3}", tv.Name, pv.FirstName, pv.LastName, pv.OverallSkill);
                }

                Console.WriteLine("--");
            }
        }
    }
}