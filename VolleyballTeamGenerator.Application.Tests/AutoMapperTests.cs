﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using NUnit.Framework;
using VolleyballTeamGenerator.Application;

namespace VolleyballTeamGenerator.Application.Tests
{
    [TestFixture]
    public class AutoMapperTests
    {
        [Test]
        public void ConfigureAutoMapper_NoInput_ReturnsValid()
        {
            Bootstrapper.ConfigureAutoMapper();
            Mapper.AssertConfigurationIsValid();
        }
    }
}
