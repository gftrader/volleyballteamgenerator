﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

namespace VolleyballTeamGenerator.Application.Messaging
{
    public class ExportDocumentResponse
    {
        public Stream DocumentStream { get; set; }
        public string ContentType { get; set; }
    }
}
