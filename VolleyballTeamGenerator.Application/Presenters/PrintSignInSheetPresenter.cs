﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using VolleyballTeamGenerator.Application.ViewInterfaces;
using VolleyballTeamGenerator.Application.ViewModels;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Domain.Services.Interfaces;

namespace VolleyballTeamGenerator.Application.Presenters
{
    public class PrintSignInSheetPresenter
    {
        private const int MAX_PLAYERS = 26;
        private IPrintSignInSheetView view;
        private ISavedPlayerListService savedPlayerListService;
        public PrintSignInSheetPresenter(IPrintSignInSheetView view, ISavedPlayerListService savedPlayerListService)
        {
            this.view = view;
            this.savedPlayerListService = savedPlayerListService;
        }

        public void Display()
        {
            if(view.PlayerListID.HasValue)
            {
                var playersInList = Mapper.Map<IList<Player>, IList<PlayerSummaryView>>(savedPlayerListService.GetSavedPlayersList(view.PlayerListID.Value).Players)
                                       .OrderBy(pv => pv.LastName)
                                       .ThenBy(pv => pv.FirstName)
                                       .ToList();
                for(int i=playersInList.Count(); i<MAX_PLAYERS; i++)
                {
                    playersInList.Add(new PlayerSummaryView()); //add empty players to fill up the table
                }
                view.PlayersInList = playersInList;
            }
        }
    }
}
