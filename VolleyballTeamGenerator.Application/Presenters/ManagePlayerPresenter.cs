﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Domain.Services.Interfaces;
using VolleyballTeamGenerator.Application.ViewInterfaces;
using VolleyballTeamGenerator.Application.ViewModels;

namespace VolleyballTeamGenerator.Application.Presenters
{
    public class ManagePlayerPresenter
    {
        IManagePlayerView view;
        IPlayerService playerService;
        IPlayerGroupService playerGroupService;
        IVolleyballAttributeService volleyballAttributeService;

        public ManagePlayerPresenter(IManagePlayerView mpView, IPlayerService playerService, IPlayerGroupService playerGroupService, IVolleyballAttributeService volleyballAttributeService)
        {
            this.view = mpView;
            this.playerService = playerService;
            this.playerGroupService = playerGroupService;
            this.volleyballAttributeService = volleyballAttributeService;
        }

        public void Display()
        {
            if (PlayerIDIsValid())
            {
                LoadAllPlayerGroups();
                LoadPlayer();
            }
            else
            {
                ShowPlayerIDInvalidMessage();
            }
        }

        private void LoadAllPlayerGroups()
        {
            this.view.AllPlayerGroups = Mapper.Map<IList<PlayerGroup>, IList<PlayerGroupView>>(playerGroupService.GetAllPlayerGroups());
        }

        private bool PlayerIDIsValid()
        {
            try
            {
                int playerID = this.view.PlayerID;
                return playerID > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void LoadPlayer()
        {
            Player player = playerService.GetPlayer(this.view.PlayerID);
            RefreshPlayerInformation(player);
            RefreshPlayerGroups(player);
            RefreshPlayerAttributes(player);
        }

        private void RefreshPlayerInformation(Player player)
        {
            this.view.Player = Mapper.Map<Player, PlayerSummaryView>(player);
        }

        private void RefreshPlayerGroups(Player player)
        {
            this.view.PlayerGroups = Mapper.Map<IList<PlayerGroup>, IList<PlayerGroupView>>(player.PlayerGroups); 
        }

        private void RefreshPlayerAttributes(Player player)
        {
            this.view.PlayerAttributes = Mapper.Map<IList<PlayerAttribute>, IList<PlayerAttributeView>>(player.PlayerAttributes);
        }

        private void ShowPlayerIDInvalidMessage()
        {
            this.view.ErrorMessages = new List<string>() { "Player ID was either invalid or not supplied." };
        }

        public void UpdatePlayer()
        {
            try
            {
                UpdatePlayerInformation();
                HandleUpdatePlayerSuccess();
            }
            catch (Exception ex)
            {
                HandleUpdatePlayerException(ex);
            }
        }

        private void UpdatePlayerInformation()
        {
            playerService.UpdatePlayer(GetUpdatedPlayerInformation());
        }

        private Player GetUpdatedPlayerInformation()
        {
            return new Player()
            {
                Id = this.view.PlayerID,
                FirstName = this.view.UpdatedFirstName,
                MiddleName = this.view.UpdatedMiddleName,
                LastName = this.view.UpdatedLastName,
                Active = this.view.UpdatedActiveStatus,
                Email = this.view.UpdatedEmail,
                Gender = this.view.UpdatedGender,
                Comments = this.view.UpdatedComments,
            };
        }

        private void HandleUpdatePlayerException(Exception ex)
        {
            this.view.ErrorMessages = new List<string>()
            {
                String.Format("There was a problem while attempting to update player {0}: {1}", this.view.PlayerID, ex.Message)
            };
        }

        private void HandleUpdatePlayerSuccess()
        {
            this.view.SuccessMessages = new List<string>()
            {
                "Player information successfully updated"
            };
        }

        public void UpdatePlayerAttributeLevel()
        {
            try
            {
                this.playerService.UpdatePlayerAttribute(GetUpdatedPlayerAttributeInformation());
                HandleUpdatePlayerAttributeSuccess();
            }
            catch (Exception ex)
            {
                HandleUpdatePlayerAttributeException(ex);
            }
        }

        private PlayerAttribute GetUpdatedPlayerAttributeInformation()
        {
            return new PlayerAttribute()
            {
                PlayerId = this.view.PlayerID,
                PlayerGroupId = this.view.UpdatedPlayerAttributeGroupID,
                VolleyballAttributeId = this.view.UpdatedVolleyballAttributeID,
                VolleyballAttributeLevel = this.view.UpdatedVolleyballAttributeLevel,
            };
        }

        private void HandleUpdatePlayerAttributeSuccess()
        {
            RefreshPlayerAttributes();
            this.view.SuccessMessages = new List<string>()
            {
                "Player attribute information successfully updated"
            };
        }

        private void HandleUpdatePlayerAttributeException(Exception ex)
        {
            this.view.ErrorMessages = new List<string>()
            {
                String.Format("There was a problem while attempting to update player attribute {0} in Group {1}: {2}", this.view.UpdatedVolleyballAttributeID, this.view.UpdatedPlayerAttributeGroupID, ex.Message)
            };
        }

        public void RefreshPlayerInformation()
        {
            RefreshPlayerInformation(playerService.GetPlayer(this.view.PlayerID));
        }

        public void RefreshPlayerAttributes()
        {
            RefreshPlayerAttributes(playerService.GetPlayer(this.view.PlayerID));
        }

        public void RefreshPlayerGroups()
        {
            this.view.PlayerGroups = Mapper.Map<IList<PlayerGroup>, IList<PlayerGroupView>>(playerService.GetPlayer(this.view.PlayerID).PlayerGroups);
        }

        public void UpdatePlayerGroups()
        {
            try
            {
                UpdatePlayerGroupsInDataStore();
                LoadPlayer();
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("There was a problem while attempting to update the player groups: {0}", ex.Message, ex));
            }
        }

        private void UpdatePlayerGroupsInDataStore()
        {
            Player player = new Player() { Id = this.view.PlayerID };
            foreach (int groupID in this.view.SelectedPlayerGroupIDs)
            {
                PlayerGroup group = player.PlayerGroups.FirstOrDefault(pg => pg.Id == groupID);
                if (group == null)
                {
                    AddPlayerGroupToPlayer(player, groupID);
                    AddDefaultPlayerAttributesToPlayer(player, groupID);
                }
            }

            foreach (PlayerGroup pg in player.PlayerGroups.Where(pg => !this.view.SelectedPlayerGroupIDs.Contains(pg.Id)))
            {
                DeletePlayerFromPlayerGroup(player, pg.Id);
            }

            playerService.UpdatePlayerGroups(player);
        }

        private void AddPlayerGroupToPlayer(Player player, int playerGroupID)
        {
            player.PlayerGroups.Add(new PlayerGroup() { Id = playerGroupID });
        }

        private void DeletePlayerFromPlayerGroup(Player player, int playerGroupID)
        {
            player.PlayerGroups.Remove(player.PlayerGroups.Where(pg => pg.Id == playerGroupID).FirstOrDefault());
        }

        private void AddDefaultPlayerAttributesToPlayer(Player player, int playerGroupID)
        {
            IList<VolleyballAttribute> allAttributes = volleyballAttributeService.GetAllVolleyballAttributes();
            foreach (VolleyballAttribute va in allAttributes)
            {
                player.PlayerAttributes.Add(new PlayerAttribute()
                {
                    PlayerId = player.Id,
                    PlayerGroupId = playerGroupID,
                    VolleyballAttributeId = va.Id,
                    VolleyballAttributeLevel = 0
                });
            }
        }
    }
}