﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using AutoMapper;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Infrastructure.OpenXml;
using VolleyballTeamGenerator.Application.Messaging;
using VolleyballTeamGenerator.Application.ViewInterfaces;
using VolleyballTeamGenerator.Application.ViewModels;
using VolleyballTeamGenerator.Domain.Services.Interfaces;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Application.Presenters
{
    public class BulkImportExportPresenter
    {
        private IBulkImportExportView bulkIEView;
        private IExcelExporter excelExporter;
        private IPlayerService playerService;
        private IPlayerGroupService playerGroupService;
        private IVolleyballAttributeService volleyballAttributeServices;

        public BulkImportExportPresenter(IBulkImportExportView bulkIEView, IExcelExporter excelExporter, IPlayerService playerServices, IPlayerGroupService playerGroupService, IVolleyballAttributeService volleyballAttributeServices)
        {
            this.bulkIEView = bulkIEView;
            this.excelExporter = excelExporter;
            this.playerService = playerServices;
            this.playerGroupService = playerGroupService;
            this.volleyballAttributeServices = volleyballAttributeServices;
        }

        public void Display()
        {
            bulkIEView.AllPlayerGroups = Mapper.Map<IList<PlayerGroup>, IList<PlayerGroupView>>(playerGroupService.GetAllPlayerGroups());
        }

        #region "Import"
        public void PerformBulkImport()
        {
            try
            {
                ValidateSelectedGroup();
                IList<PlayerUpdateView> playersToUpdate = ProcessFileUpload();
                playerService.AddOrUpdatePlayers(ConvertPlayerViewsToPlayers(playersToUpdate));
                bulkIEView.UploadedPlayers = playersToUpdate;
            }
            catch (Exception ex)
            {
                bulkIEView.ErrorMessages = new List<string>() { String.Format("There was a problem while attempting to process the import: {0}", ex.Message) };
            }
        }

        private void ValidateSelectedGroup()
        {
            if (bulkIEView.SelectedPlayerGroupId < 0)
            {
                throw new Exception("You must select a player group to import these players");
            }
        }

        private IList<PlayerUpdateView> ProcessFileUpload()
        {
            if (bulkIEView.FileUpload != null && bulkIEView.FileUpload.ContentLength > 0)
            {
                IDocumentImporter documentImporter = DocumentImporter.CreateDocumentImporter(bulkIEView.FileUpload.FileName);
                using (Stream stream = bulkIEView.FileUpload.InputStream)
                {
                    DataTable dt = documentImporter.LoadDataTable(stream);
                    return LoadPlayerUpdateViewsFromDataTable(dt);
                }
            }
            else
            {
                throw new Exception("Uploaded file does not exist or has no content");
            }
        }

        private bool ColumnsValid(DataTable dt, ref IList<string> playerAttributeColumnNames)
        {
            List<string> errors = new List<string>();
            foreach (string requiredProperty in GetRequiredColumnNames(ref playerAttributeColumnNames))
            {
                bool propertyFound = false;
                foreach (DataColumn dc in dt.Columns)
                {
                    if (requiredProperty.ToLower().Equals(dc.ColumnName.ToLower()))
                    {
                        propertyFound = true;
                        break;
                    }
                }

                if (!propertyFound)
                {
                    errors.Add(String.Format("Column matching property '{0}' not found in import file", requiredProperty));
                }
            }
            if (errors.Count > 0) { bulkIEView.ErrorMessages = errors; }
            return errors.Count == 0;
        }

        private IList<string> GetRequiredColumnNames(ref IList<string> playerAttributeColumnNames)
        {
            List<string> requiredColumns = new List<string>();
            PlayerUpdateView puv = new PlayerUpdateView();

            requiredColumns.Add(GetPropertyName<int>(() => puv.Id));
            requiredColumns.Add(GetPropertyName<string>(() => puv.FirstName));
            requiredColumns.Add(GetPropertyName<string>(() => puv.MiddleName));
            requiredColumns.Add(GetPropertyName<string>(() => puv.LastName));
            requiredColumns.Add(GetPropertyName<string>(() => puv.Gender));
            requiredColumns.Add(GetPropertyName<string>(() => puv.Email));
            requiredColumns.Add(GetPropertyName<bool>(() => puv.Active));
            requiredColumns.Add(GetPropertyName<string>(() => puv.Comments));

            foreach (VolleyballAttribute va in volleyballAttributeServices.GetAllVolleyballAttributes())
            {
                requiredColumns.Add(va.Name);
                playerAttributeColumnNames.Add(va.Name);
            }
            return requiredColumns;
        }

        private IList<PlayerUpdateView> LoadPlayerUpdateViewsFromDataTable(DataTable dt)
        {
            List<PlayerUpdateView> playersToUpdate = new List<PlayerUpdateView>();
            IList<string> playerAttributeColumnNames = new List<string>();

            if (ColumnsValid(dt, ref playerAttributeColumnNames))
            {
                foreach (DataRow dr in dt.Rows)
                {
                    PlayerUpdateView puv = new PlayerUpdateView();
                    puv.Id = GetValue<int>(dr[GetPropertyName<int>(() => puv.Id)], x => Convert.ToInt32(x), -1);
                    puv.FirstName = GetValue<string>(dr[GetPropertyName<string>(() => puv.FirstName)], x => x.ToString(), null);
                    puv.MiddleName = GetValue<string>(dr[GetPropertyName<string>(() => puv.MiddleName)], x => x.ToString(), null);
                    puv.LastName = GetValue<string>(dr[GetPropertyName<string>(() => puv.LastName)], x => x.ToString(), null);
                    puv.Gender = GetValue<string>(dr[GetPropertyName<string>(() => puv.Gender)], x => x.ToString(), null);
                    puv.Email = GetValue<string>(dr[GetPropertyName<string>(() => puv.Email)], x => x.ToString(), null);
                    puv.Active = GetValue<bool>(dr[GetPropertyName<bool>(() => puv.Active)], x => Convert.ToBoolean(x), false);
                    puv.Comments = GetValue<string>(dr[GetPropertyName<string>(() => puv.Comments)], x => x.ToString(), null);

                    foreach (string paColumnName in playerAttributeColumnNames)
                    {
                        PlayerAttributeView pav = new PlayerAttributeView()
                        {
                            PlayerId = puv.Id,
                            VolleyballAttributeName = paColumnName,
                            VolleyballAttributeLevel = GetValue<double>(dr[paColumnName], x => Convert.ToDouble(x), 0)
                        };
                        puv.PlayerAttributes.Add(pav);
                    }
                    playersToUpdate.Add(puv);
                }
            }

            return playersToUpdate;
        }

        private bool InputsValid(IList<PlayerUpdateView> playersToUpdate)
        {
            int startRow = 2;
            List<string> errors = new List<string>();
            foreach (PlayerUpdateView puv in playersToUpdate)
            {
                if (String.IsNullOrEmpty(puv.FirstName)) { errors.Add(String.Format("Row {0}: First Name must be present", startRow)); }
                if (String.IsNullOrEmpty(puv.LastName)) { errors.Add(String.Format("Row {0}: Last Name must be present", startRow)); }
                if (String.IsNullOrEmpty(puv.Gender)) { errors.Add(String.Format("Row {0}: Gender must be present", startRow)); }
                else if (!new char[] { 'm', 'f' }.Contains(puv.Gender[0])) { errors.Add("Gender must be 'M' or 'F'"); }
                try { bool active = puv.Active; }
                catch (Exception) { errors.Add(String.Format("Row {0}: Active must be set", startRow)); }
            }

            bulkIEView.ErrorMessages = errors;
            return errors.Count == 0;
        }

        private IList<Player> ConvertPlayerViewsToPlayers(IList<PlayerUpdateView> playersToUpdate)
        {
            IList<Player> players = new List<Player>();
            foreach (PlayerUpdateView puv in playersToUpdate)
            {
                Player player = new Player();
                if (puv.Id > 0) { player.Id = puv.Id; }  //populate with existing data, overwrite what is present
                player.FirstName = puv.FirstName;
                player.MiddleName = puv.MiddleName;
                player.LastName = puv.LastName;
                player.Gender = puv.Gender;
                player.Email = puv.Email;
                player.Active = puv.Active;
                player.Comments = puv.Comments;

                foreach (PlayerAttributeView pav in puv.PlayerAttributes)
                {
                    PlayerAttribute pa = new PlayerAttribute();
                    if (puv.Id > 0) { pa.PlayerId = puv.Id; }
                    pa.VolleyballAttributeId = volleyballAttributeServices.GetVolleyballAttributeByName(pav.VolleyballAttributeName).Id;
                    pa.PlayerGroupId = bulkIEView.SelectedPlayerGroupId;
                    pa.VolleyballAttributeLevel = pav.VolleyballAttributeLevel;
                    player.PlayerAttributes.Add(pa);
                }

                player.PlayerGroups.Add(new PlayerGroup(){ Id = bulkIEView.SelectedPlayerGroupId });
                players.Add(player);
            }

            return players;
        }

        #endregion

        #region "Export"
        public void PerformBulkExport()
        {
            try
            {
                ValidateSelectedGroup();
                IList<string> playerAttributeColumnNames = new List<string>();
                IList<string> propertyNames = GetRequiredColumnNames(ref playerAttributeColumnNames);

                excelExporter.DocumentName = bulkIEView.ExportDocumentName;
                excelExporter.AddDataTable(null, GetDataTable(playerAttributeColumnNames, propertyNames));

                IStreamFactory.StreamType = StreamTypeEnum.MemoryStream;
                using (IStream stream = IStreamFactory.Create())
                {
                    excelExporter.SaveAsStream(stream);
                    bulkIEView.ExportDocumentResponse = new ExportDocumentResponse(){ DocumentStream = stream.GetStream(), ContentType = excelExporter.HttpResponseStreamContentType };
                }
            }
            catch (Exception ex)
            {
                bulkIEView.ErrorMessages = new List<string>() { String.Format("There was a problem while attempting to process the export: {0}", ex.Message) };
            }
        }

        private DataTable GetDataTable(IList<string> playerAttributeColumnNames, IList<string> propertyNames)
        {
            DataTable dt = new DataTable();
            foreach (string propertyName in propertyNames)
            {
                dt.Columns.Add(new DataColumn() { ColumnName = propertyName });
            }

            foreach (PlayerUpdateView puv in Mapper.Map<IList<Player>, IList<PlayerUpdateView>>(playerService.GetAllPlayersInGroup(bulkIEView.SelectedPlayerGroupId)))
            {
                DataRow dr = dt.NewRow();
                foreach (string propertyName in propertyNames)
                {
                    bool propertyNameIsVolleyballAttribute = playerAttributeColumnNames.Any(s => s.Equals(propertyName));
                    if (propertyNameIsVolleyballAttribute)
                    {
                        foreach (PlayerAttributeView pav in puv.PlayerAttributes)
                        {
                            //currently this isn't hydrating from the db for some reason
                            VolleyballAttribute va = volleyballAttributeServices.GetVolleyballAttribute(pav.VolleyballAttributeId);
                            if (propertyName.Equals(va.Name))
                            {
                                dr[propertyName] = pav.VolleyballAttributeLevel;
                                break;
                            }
                        }
                    }
                    else
                    {
                        dr[propertyName] = puv.GetType().GetProperty(propertyName).GetValue(puv, null);
                    }
                }

                dt.Rows.Add(dr);
            }
            return dt;
        }
        #endregion

        //reflection stuff
        private string GetPropertyName<T>(Expression<Func<T>> expr)
        {
            return (expr.Body as MemberExpression).Member.Name;
        }

        private T GetValue<T>(object obj, Func<object, T> conversion, T defaultValue)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return defaultValue;
            }
            else if (obj.ToString().Trim().Length == 0)
            {
                return defaultValue;
            }
            else
            {
                int intValue;
                if (typeof(T) == typeof(bool) && Int32.TryParse(obj.ToString(), out intValue))
                {
                    return conversion(intValue);
                }
                return conversion(obj);
            }
        }
    }
}