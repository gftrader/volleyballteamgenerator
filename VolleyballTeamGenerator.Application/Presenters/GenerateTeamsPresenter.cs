﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using VolleyballTeamGenerator.Application.ViewInterfaces;
using VolleyballTeamGenerator.Application.ViewModels;
using VolleyballTeamGenerator.Domain.Messages;
using VolleyballTeamGenerator.Domain.Services.Interfaces;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Application.Presenters
{
    public class GenerateTeamsPresenter
    {
        private IGenerateTeamsView view;
        private ITeamGenerationDomainService tgds;
        private IPlayerService playerService;
        private IPlayerGroupService playerGroupService;
        private ISavedPlayerListService savedPlayerListService;

        public GenerateTeamsPresenter(IGenerateTeamsView generateTeamsView, ITeamGenerationDomainService tgds, IPlayerService playerServices, IPlayerGroupService playerGroupService, ISavedPlayerListService savedPlayerListService)
        {
            this.view = generateTeamsView;
            this.tgds = tgds;
            this.playerService = playerServices;
            this.playerGroupService = playerGroupService;
            this.savedPlayerListService = savedPlayerListService;
        }

        public void Display()
        {
            view.PlayerGroupViews = Mapper.Map<IList<PlayerGroup>, IList<PlayerGroupView>>(playerGroupService.GetAllPlayerGroups());
            view.CompositionStrategyViewNames = Mapper.Map<IList<CompositionStrategy>, IList<CompositionStrategyView>>(tgds.GetAllCompositionStrategies());
        }

        public void ClearListOfPlayers()
        {
            view.PlayersToUseForTeamGeneration = new List<PlayerSummaryView>();
        }

        public void AddPlayerToListOfPlayers()
        {
            if (AddPlayerInputsValid())
            {
                IList<PlayerSummaryView> selectedPlayers = view.PlayersToUseForTeamGeneration;
                selectedPlayers.Add(Mapper.Map<Player, PlayerSummaryView>(playerService.GetPlayer(view.PlayerIDToAdd)));
                view.PlayersToUseForTeamGeneration = selectedPlayers;
            }
        }

        private bool AddPlayerInputsValid()
        {
            List<string> errors = new List<string>();
            try
            {
                int playerIDToAdd = view.PlayerIDToAdd;
            }
            catch (Exception)
            {
                errors.Add("You must add a player from the dropdown list");
            }

            if (view.PlayersToUseForTeamGeneration.Any(p => p.Id == view.PlayerIDToAdd))
            {
                errors.Add("The player you attempted to add already exists in the list of players");
            }

            if (errors.Count > 0) { view.ErrorMessages = errors; }
            return errors.Count == 0;
        }

        public void GetPlayersInSelectedGroup()
        {
            view.PlayersInSelectedGroup = Mapper.Map<IList<Player>, IList<PlayerSummaryView>>(playerGroupService.GetPlayersInPlayerGroup(view.SelectedPlayerGroupID));
            LoadSavedPlayersLists();
        }

        public void GenerateTeams()
        {
            if (GenerateTeamsInputsValid())
            {
                view.GeneratedTeams = Mapper.Map<IEnumerable<Team>, IEnumerable<TeamView>>(tgds.GenerateTeams(BuildRequest()).Teams);
            }
        }

        public bool GenerateTeamsInputsValid()
        {
            List<string> errors = new List<string>();
            bool numberOfTeamsIsInteger = false;
            try 
            { 
                int numberOfTeams = view.NumberOfTeams;
                numberOfTeamsIsInteger = true;
            }
            catch (Exception) { errors.Add("You must enter a valid number of teams"); }

            if(numberOfTeamsIsInteger)
            {
                if (view.NumberOfTeams < 2 || view.NumberOfTeams > 50) 
                { 
                    errors.Add("The number of teams to generate must be between 2 and 50"); 
                }

                if (view.PlayersToUseForTeamGeneration.Count / view.NumberOfTeams < 2)
                {
                    errors.Add("There must be at least two players per team");
                }
            }
            if (view.SelectedTeamGenerationStrategyID < 0) { errors.Add("You must select a team generation strategy to use"); }
            
            if (errors.Count > 0) { view.ErrorMessages = errors; }
            return errors.Count == 0;
        }

        private GenerateTeamsDomainRequest BuildRequest()
        {
            return new GenerateTeamsDomainRequest()
            {
                PlayerIDsToUse = view.PlayersToUseForTeamGeneration.Select(p => p.Id).ToList(),
                PlayerGroupId = view.SelectedPlayerGroupID,
                TeamCompositionStrategy = (CompositionStrategyEnum)view.SelectedTeamGenerationStrategyID,
                NumberOfTeams = view.NumberOfTeams
            };
        }

        public void LoadSavedPlayersList()
        {
            SavedPlayersList savedPlayersList = savedPlayerListService.GetSavedPlayersList(view.SelectedSavedPlayersListID);
            view.PlayersToUseForTeamGeneration = (savedPlayersList != null) ? Mapper.Map<IList<Player>, IList<PlayerSummaryView>>(savedPlayersList.Players) : new List<PlayerSummaryView>();
            if (savedPlayersList != null) { view.SavedPlayersListName = savedPlayersList.Name; }
        }

        public void LoadSavedPlayersLists()
        {
            view.SavedPlayersLists = Mapper.Map<IList<SavedPlayersList>, IList<SavedPlayersListView>>(savedPlayerListService.GetSavedPlayersListsByGroupId(view.SelectedPlayerGroupID));
        }

        public void SavePlayersList()
        {
            try
            {
                SavePlayersListToDatabase();
                view.SuccessMessages = new List<string>() { "List successfully saved" };
            }
            catch (Exception ex)
            {
                view.ErrorMessages = new List<string>() { String.Format("There was a problem while attempting to save the players list: {0}", ex.Message) };
            }
        }

        private void SavePlayersListToDatabase()
        {
            IList<Player> playersToSave = new List<Player>();
            foreach (PlayerSummaryView pv in view.PlayersToUseForTeamGeneration) { playersToSave.Add(new Player() { Id = pv.Id }); }
            SavedPlayersList dbSPL = savedPlayerListService.GetSavedPlayersListByGroupIdAndName(view.SelectedPlayerGroupID, view.SavedPlayersListName);
            SavedPlayersList spl = new SavedPlayersList()
            {
                Name = view.SavedPlayersListName,
                PlayerGroupId = view.SelectedPlayerGroupID,
                Players = playersToSave
            };

            if (dbSPL == null)
            {
                savedPlayerListService.AddSavedPlayersList(spl);
                LoadSavedPlayersLists();
            }
            else
            {
                spl.Id = dbSPL.Id;
                savedPlayerListService.UpdateSavedPlayersList(spl);
            }
        }

        public void DeletePlayersList()
        {
            try
            {
                DeletePlayersListFromDatabase();
                LoadSavedPlayersLists();
                view.SuccessMessages = new List<string>() { "List successfully deleted" };
            }
            catch (Exception ex)
            {
                view.ErrorMessages = new List<string>() { String.Format("There was a problem while attempting to save the players list: {0}", ex.Message) };
            }
        }

        private void DeletePlayersListFromDatabase()
        {
            savedPlayerListService.DeleteSavedPlayersList(view.SelectedSavedPlayersListID);
        }

        public void RemovePlayerFromPlayersList()
        {
            IList<PlayerSummaryView> players = view.PlayersToUseForTeamGeneration;
            players.RemoveAt(view.SelectedPlayerToRemoveIndex);
            view.PlayersToUseForTeamGeneration = players;
        }
    }
}