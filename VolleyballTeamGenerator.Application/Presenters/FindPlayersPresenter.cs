﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using VolleyballTeamGenerator.Application.ViewInterfaces;
using VolleyballTeamGenerator.Application.ViewModels;
using VolleyballTeamGenerator.Domain.Services.Interfaces;
using VolleyballTeamGenerator.Domain.Messages;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Application.Presenters
{
    public class FindPlayersPresenter
    {
        private IFindPlayersView findPlayersView;
        private IPlayerService playerService;
        private IPlayerGroupService playerGroupService;

        public FindPlayersPresenter(IFindPlayersView findPlayersView, IPlayerService playerService, IPlayerGroupService playerGroupService)
        {
            this.findPlayersView = findPlayersView;
            this.playerService = playerService;
            this.playerGroupService = playerGroupService;
        }

        public void Display()
        {
            this.findPlayersView.AllPlayerGroups = Mapper.Map<IList<PlayerGroup>, IList<PlayerGroupView>>(playerGroupService.GetAllPlayerGroups());
        }

        public void FindPlayers()
        {
            FindPlayersRequest fpRequest = Mapper.Map<IFindPlayersView, FindPlayersRequest>(this.findPlayersView);
            this.findPlayersView.MatchingPlayers = Mapper.Map<IList<Player>, IList<PlayerSummaryView>>(playerService.FindPlayers(fpRequest));
        }
    }
}
