﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Application.ViewInterfaces;
using VolleyballTeamGenerator.Domain.Services.Interfaces;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Application.Presenters
{
    public class AddPlayerPresenter
    {
        private IAddPlayerView addPlayerView;
        private IPlayerService playerService;

        public AddPlayerPresenter(IAddPlayerView addPlayerView, IPlayerService playerService)
        {
            this.addPlayerView = addPlayerView;
            this.playerService = playerService;
        }

        public void Display()
        {
        }

        public void AddPlayer()
        {
            Player playerToAdd = GetPlayerToAdd();
            ValidatePlayer(playerToAdd);
            AddPlayerToDataStore(playerToAdd);
        }

        private void ValidatePlayer(Player playerToAdd)
        {
            List<string> errors = new List<string>();
            if (String.IsNullOrEmpty(playerToAdd.FirstName) || String.IsNullOrEmpty(playerToAdd.LastName))
            {
                errors.Add("You must enter a first and last name to add a player");
            }
            if (String.IsNullOrEmpty(playerToAdd.Gender))
            {
                errors.Add("You must enter a gender to add a player");
            }

            if (errors.Count > 0)
            {
                throw new Exception(String.Format("The following errors prevented this player from being added: {0}", String.Join("; ", errors)));
            }
        }

        private Player GetPlayerToAdd()
        {
            try
            {
                Player player = new Player()
                {
                    FirstName = this.addPlayerView.PlayerUpdateView.FirstName,
                    MiddleName = this.addPlayerView.PlayerUpdateView.MiddleName,
                    LastName = this.addPlayerView.PlayerUpdateView.LastName,
                    Active = true,
                    Email = this.addPlayerView.PlayerUpdateView.Email,
                    Gender = this.addPlayerView.PlayerUpdateView.Gender,
                    Comments = this.addPlayerView.PlayerUpdateView.Comments
                };
                return player;
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("There was a problem while attempting to get the player data from the page: {0}", ex.Message), ex);
            }
        }

        private void AddPlayerToDataStore(Player playerToAdd)
        {
            try
            {
                playerService.AddPlayer(playerToAdd);
                this.addPlayerView.HttpResponse.Redirect(String.Format("{0}?playerID={1}", this.addPlayerView.ManagePlayerUrl, playerToAdd.Id), false);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("There was a problem while attempting to add the player to the database: {0}", ex.Message), ex);
            }
        }
    }
}