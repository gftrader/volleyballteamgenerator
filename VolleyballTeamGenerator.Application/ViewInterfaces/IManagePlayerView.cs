﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Application.ViewModels;

namespace VolleyballTeamGenerator.Application.ViewInterfaces
{
    public interface IManagePlayerView : IPresentationView
    {
        int PlayerID { get; }
        string UpdatedFirstName { get; }
        string UpdatedMiddleName { get; }
        string UpdatedLastName { get; }
        bool UpdatedActiveStatus { get; }
        string UpdatedEmail { get; }
        string UpdatedGender { get; }
        string UpdatedComments { get; }

        int UpdatedPlayerAttributeGroupID { get; }
        int UpdatedVolleyballAttributeID { get; }
        double UpdatedVolleyballAttributeLevel { get; }
        IList<int> SelectedPlayerGroupIDs { get; }

        IList<PlayerGroupView> AllPlayerGroups { set; }
        PlayerSummaryView Player { set; }
        IList<PlayerAttributeView> PlayerAttributes { set; }
        IList<PlayerGroupView> PlayerGroups { set; }
    }
}
