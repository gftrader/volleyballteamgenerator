﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Application.ViewModels;

namespace VolleyballTeamGenerator.Application.ViewInterfaces
{
    public interface IFindPlayersView
    {
        string FindPlayerFirstName { get; }
        string FindPlayerLastName { get; }
        string FindPlayerEmail { get; }
        string FindPlayerGender { get; }
        IList<int> FindPlayerGroupIDs { get; }

        IList<PlayerGroupView> AllPlayerGroups { set; }
        IList<PlayerSummaryView> MatchingPlayers { set; }
    }
}
