﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Application.ViewModels;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Application.ViewInterfaces
{
    public interface IGenerateTeamsView : IPresentationView
    {
        int SelectedPlayerGroupID { get; }
        int PlayerIDToAdd { get; }
        int NumberOfTeams { get; }
        int SelectedTeamGenerationStrategyID { get; }
        int SelectedSavedPlayersListID { get; }
        int SelectedPlayerToRemoveIndex { get; }

        string SavedPlayersListName { get; set; }
        IList<PlayerSummaryView> PlayersToUseForTeamGeneration { get; set; }

        IList<PlayerSummaryView> PlayersInSelectedGroup { set; }
        IList<PlayerGroupView> PlayerGroupViews { set; }
        IList<CompositionStrategyView> CompositionStrategyViewNames { set; }
        IEnumerable<TeamView> GeneratedTeams { set; }
        IList<SavedPlayersListView> SavedPlayersLists { set; }
    }
}
