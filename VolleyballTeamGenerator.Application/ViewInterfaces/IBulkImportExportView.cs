﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using VolleyballTeamGenerator.Application.Messaging;
using VolleyballTeamGenerator.Application.ViewModels;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Application.ViewInterfaces
{
    public interface IBulkImportExportView : IPresentationView
    {
        int SelectedPlayerGroupId { get; }
        HttpPostedFile FileUpload { get; }
        string ExportDocumentName { get; }

        IList<PlayerGroupView> AllPlayerGroups { set; }
        IList<PlayerUpdateView> UploadedPlayers { set; }
        ExportDocumentResponse ExportDocumentResponse { set; }
    }
}
