﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Application.ViewModels;

namespace VolleyballTeamGenerator.Application.ViewInterfaces
{
    public interface IAddPlayerView
    {
        string ManagePlayerUrl { get; }
        IHttpResponse HttpResponse { get; }
        PlayerUpdateView PlayerUpdateView { get; }
    }
}
