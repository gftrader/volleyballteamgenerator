﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Domain.Messages;
using VolleyballTeamGenerator.Application.ViewInterfaces;
using VolleyballTeamGenerator.Application.ViewModels;

namespace VolleyballTeamGenerator.Application
{
    public static class Bootstrapper
    {
        public static void ConfigureAutoMapper()
        {
            Mapper.CreateMap<CompositionStrategy, CompositionStrategyView>();
            Mapper.CreateMap<PlayerAttribute, PlayerAttributeView>();
            Mapper.CreateMap<PlayerGroup, PlayerGroupView>();
            Mapper.CreateMap<Player, PlayerSummaryView>()
                .ForMember(pv => pv.PlayerName, opt => opt.ResolveUsing<NameResolver>())
                .ForMember(pv => pv.OverallSkill, opt => opt.Ignore());
            Mapper.CreateMap<Player, PlayerView>()
                .ForMember(pv => pv.PlayerName, opt => opt.ResolveUsing<NameResolver>())
                .ForMember(pv => pv.OverallSkill, opt => opt.Ignore());
            Mapper.CreateMap<Player, PlayerUpdateView>()
                .ForMember(pv => pv.PlayerName, opt => opt.ResolveUsing<NameResolver>())
                .ForMember(puv => puv.OverallSkill, opt => opt.Ignore());
            Mapper.CreateMap<SavedPlayersList, SavedPlayersListView>();
            Mapper.CreateMap<SavedPlayersList, SavedPlayersListSummaryView>();
            Mapper.CreateMap<Team, TeamView>();
            Mapper.CreateMap<IFindPlayersView, FindPlayersRequest>()
                .ForMember(fpr => fpr.PlayerIds, opt => opt.Ignore());
            Mapper.CreateMap<SavedPlayersListsTeamsList, SavedPlayersListsTeamsListView>()
                .ForMember(x => x.Teams, opt => opt.ResolveUsing<SavedPlayerListsTeamsListTeamsResolver>());
        }

        public class NameResolver : ValueResolver<Player, string>
        {
            protected override string ResolveCore(Player source)
            {
                string middleNameSpaceText = String.IsNullOrEmpty(source.MiddleName) ? "" : " ";
                return String.Format("{0}{1}{2} {3}", source.FirstName, middleNameSpaceText, source.MiddleName, source.LastName);
            }
        }

        public class SavedPlayerListsTeamsListTeamsResolver : ValueResolver<SavedPlayersListsTeamsList, IEnumerable<TeamView>>
        {
            protected override IEnumerable<TeamView> ResolveCore(SavedPlayersListsTeamsList source)
            {
                var teams = new List<TeamView>();
                foreach(var team in source.SavedPlayersListsTeamsLists_Teams)
                {
                    teams.Add(new TeamView()
                    {
                        Name = team.Name,
                        Players = Mapper.Map<IEnumerable<Player>, IList<PlayerSummaryView>>(team.Players)
                    });
                }

                return teams;
            }
        }
    }
}
