﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VolleyballTeamGenerator.Application.ViewModels
{
    public class PlayerView : PlayerSummaryView
    {
        public IEnumerable<PlayerAttributeView> PlayerAttributes { get; set; }
        public IEnumerable<PlayerGroupView> PlayerGroups { get; set; }

        public PlayerView()
        {
            PlayerAttributes = new List<PlayerAttributeView>();
            PlayerGroups = new List<PlayerGroupView>();
        }
    }
}
