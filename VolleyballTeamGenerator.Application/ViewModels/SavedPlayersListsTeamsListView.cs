﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VolleyballTeamGenerator.Application.ViewModels
{
    public class SavedPlayersListsTeamsListView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<TeamView> Teams { get; set; }

        public SavedPlayersListsTeamsListView()
        {
            Teams = new List<TeamView>();
        }
    }
}
