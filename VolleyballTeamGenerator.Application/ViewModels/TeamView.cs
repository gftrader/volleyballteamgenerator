﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VolleyballTeamGenerator.Application.ViewModels
{
    public class TeamView
    {
        public string Name
        {
            get;
            set;
        }

        public IList<PlayerSummaryView> Players { get; set; }
        public double OverallSkillLevel { get; set; }
    }
}
