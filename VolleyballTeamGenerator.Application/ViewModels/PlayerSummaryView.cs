﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VolleyballTeamGenerator.Application.ViewModels
{
    public class PlayerSummaryView
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string PlayerName { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string Comments { get; set; }
        public bool Active { get; set; }
        public double OverallSkill { get; set; }

        public override string ToString()
        {
            return String.Format("Player: {0}; Overall Skill: {1}", PlayerName, OverallSkill); 
        }

        public PlayerSummaryView()
        {
        }
    }
}