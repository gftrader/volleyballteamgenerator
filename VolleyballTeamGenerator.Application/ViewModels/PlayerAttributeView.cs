﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VolleyballTeamGenerator.Application.ViewModels
{
    public class PlayerAttributeView
    {
        public int PlayerId { get; set; }
        public int PlayerGroupId { get; set; }
        public string PlayerGroupName { get; set; }
        public int VolleyballAttributeId { get; set; }
        public string VolleyballAttributeName { get; set; }
        public double VolleyballAttributeLevel { get; set; }
    }
}
