﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VolleyballTeamGenerator.Application.ViewModels
{
    public class PlayerUpdateView : PlayerSummaryView
    {
        public IList<PlayerAttributeView> PlayerAttributes { get; set; }

        public PlayerUpdateView()
        {
            PlayerAttributes = new List<PlayerAttributeView>();
        }
    }
}