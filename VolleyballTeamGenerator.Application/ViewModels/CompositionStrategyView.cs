﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VolleyballTeamGenerator.Application.ViewModels
{
    public class CompositionStrategyView
    {
        public CompositionStrategyView()
        {
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}
