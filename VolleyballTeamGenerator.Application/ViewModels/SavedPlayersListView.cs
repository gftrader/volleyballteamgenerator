﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VolleyballTeamGenerator.Application.ViewModels
{
    public class SavedPlayersListView : SavedPlayersListSummaryView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IList<PlayerSummaryView> Players { get; set; }

        public SavedPlayersListView()
        {
            Players = new List<PlayerSummaryView>();
        }
    }
}
