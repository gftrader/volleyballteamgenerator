﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Data;

namespace VolleyballTeamGenerator.Infrastructure
{
    public interface IDocumentImporter
    {
        DataTable LoadDataTable(Stream stream);
        IList<T> LoadItems<T>(Stream stream);
    }
}
