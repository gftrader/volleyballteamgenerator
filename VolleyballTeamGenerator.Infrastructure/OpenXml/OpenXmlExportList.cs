﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace VolleyballTeamGenerator.Infrastructure.OpenXml
{
    public class OpenXmlExportList<T> : OpenXmlExportDataCollection
    {
        private IList<ExportDocumentProperty> objectProperties;
        private IList<T> objectsToAdd;

        public OpenXmlExportList(IList<ExportDocumentProperty> objectProperties, IList<T> objectsToAdd)
        {
            this.objectProperties = objectProperties;
            this.objectsToAdd = objectsToAdd;
        }

        public override void AddDataToWorksheet(ref uint currentRow, SpreadsheetDocument spreadsheet, Worksheet worksheet)
        {
            List<PropertyInfo> reflectedProperties = typeof(T).GetProperties().ToList();

            uint currentColumn = 1;
            foreach (ExportDocumentProperty xdocProperty in objectProperties)
            {
                //ReflectionHelper.VerifyPropertyExists(reflectedProperties, xdocProperty.ObjectPropertyName);
                OpenXmlExcelDocument.SetCellValue(spreadsheet, worksheet, currentColumn, currentRow, xdocProperty.DocumentHeadingText, false, false);
                currentColumn++;
            }
            currentRow++;

            foreach (T objectToAdd in objectsToAdd)
            {
                currentColumn = 1;
                foreach (ExportDocumentProperty xdocProperty in objectProperties)
                {
                    AddValueToCell(spreadsheet, worksheet, currentRow, reflectedProperties, currentColumn, objectToAdd, xdocProperty);
                    currentColumn++;
                }

                currentRow++;
            }
        }

        private void AddValueToCell(SpreadsheetDocument spreadsheet, Worksheet worksheet, uint currentRow, List<PropertyInfo> reflectedProperties, uint currentColumn, T objectToAdd, ExportDocumentProperty xdocProperty)
        {
            PropertyInfo propertyInfo = reflectedProperties.FirstOrDefault(rp => rp.Name == xdocProperty.ObjectPropertyName);
            object value = propertyInfo.GetValue(objectToAdd, null);
            string stringValue = String.Format(xdocProperty.DataFormatString, value);
            OpenXmlExcelDocument.SetCellValue(spreadsheet, worksheet, currentColumn, currentRow, stringValue, false, false);

            //this just isn't ready for primetime - I can't figure out how to format currency, and other formats are inconsistent at best.
            /*if (value == null || value is string)
            {
                if (value == null) { value = ""; }
                OpenXmlExcelDocument.SetCellValue(spreadsheet, worksheet, currentColumn, currentRow, value.ToString(), false, false);
            }
            else if (value is int)
            {
                OpenXmlExcelDocument.SetCellValue(spreadsheet, worksheet, currentColumn, currentRow, Convert.ToInt32(value), null);
            }
            else if (value is double)
            {
                OpenXmlExcelDocument.SetCellValue(spreadsheet, worksheet, currentColumn, currentRow, Convert.ToDouble(value), null);
            }
            else if (value is decimal)
            {
                OpenXmlExcelDocument.SetCellValue(spreadsheet, worksheet, currentColumn, currentRow, Convert.ToDecimal(value), 2);
            }
            else if (value is DateTime)
            {
                OpenXmlExcelDocument.SetCellValue(spreadsheet, worksheet, currentColumn, currentRow, Convert.ToDateTime(value), 1);
            }
            else if (value is bool)
            {
                OpenXmlExcelDocument.SetCellValue(spreadsheet, worksheet, currentColumn, currentRow, Convert.ToBoolean(value), null);
            }*/
        }
    }
}
