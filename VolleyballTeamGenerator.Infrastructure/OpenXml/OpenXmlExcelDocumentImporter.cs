﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace VolleyballTeamGenerator.Infrastructure.OpenXml
{
    public class OpenXmlExcelDocumentImporter : IDocumentImporter
    {        
        public OpenXmlExcelDocumentImporter()
        {
        }

        public string SheetName
        {
            get;
            set;
        }

        public int StartRow
        {
            get;
            set;
        }

        public DataTable LoadDataTable(Stream stream)
        {
            return Load(stream, LoadDataTableFromDocument);
        }

        private DataTable LoadDataTableFromDocument(SpreadsheetDocument document, Workbook workbook, SharedStringTable sharedStrings)
        {
            int currentRow = StartRow;
            Dictionary<string, string> columnNameMappings = new Dictionary<string, string>();
            DataTable dt = new DataTable();

            foreach(Row xlRow in OpenXmlDocumentHelper.GetSheet(workbook, SheetName).Descendants<Row>())
            {
                currentRow = ProcessRow(sharedStrings, currentRow, columnNameMappings, dt, xlRow);
            }
            
            return dt;
        }

        private int ProcessRow(SharedStringTable sharedStrings, int currentRow, Dictionary<string, string> columnNameMappings, DataTable dt, Row xlRow)
        {
            DataRow currentDataRow = null;
            bool firstColumn = true;
            foreach (Cell cell in xlRow.ChildElements)
            {
                string columnLetter = GetCellColumnLetter(cell);
                string cellValue = GetCellValue(cell, sharedStrings.SharedStringTablePart);
                if (currentRow == StartRow)
                {
                    AddHeaderRow(columnNameMappings, dt, columnLetter, cellValue);
                }
                else
                {
                    if (firstColumn)
                    {
                        currentDataRow = AddNewDataRow(dt);
                        firstColumn = false;
                    }

                    AddCellValue(columnNameMappings, currentDataRow, columnLetter, cellValue);
                }
            }

            return ++currentRow;
        }

        private static void AddHeaderRow(Dictionary<string, string> columnNameMappings, DataTable dt, string columnLetter, string columnName)
        {
            if (!String.IsNullOrEmpty(columnName))
            {
                dt.Columns.Add(columnName);
                columnNameMappings.Add(columnLetter, columnName);
            }
        }

        private static DataRow AddNewDataRow(DataTable dt)
        {
            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            return dr;
        }

        private static void AddCellValue(Dictionary<string, string> columnNameMappings, DataRow currentDataRow, string columnLetter, string cellValue)
        {
            if (columnNameMappings.ContainsKey(columnLetter))
            {
                currentDataRow[columnNameMappings[columnLetter]] = cellValue;
            }
        }

        

        private static string GetCellColumnLetter(Cell cell)
        {
            if (cell.CellReference.HasValue)
            {
                Regex r = new Regex("[A-Za-z]+");
                return r.Match(cell.CellReference.Value).Value;
            }

            return null;
        }

        private static string GetCellValue(Cell cell, SharedStringTablePart stringTablePart)
        {
            if (cell.ChildElements.Count == 0) { return null; }
            
            string value = cell.CellValue.InnerText;
            //Look up real value from shared string table
            if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString))
            {
                value = stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            return value;
        }

        public IList<T> LoadItems<T>(Stream stream)
        {
            return Load(stream, LoadItemsList<T>);
        }

        private IList<T> LoadItemsList<T>(SpreadsheetDocument document, Workbook workbook, SharedStringTable sharedStrings)
        {
            throw new NotImplementedException();
            //IList<T> itemsToReturn = new List<T>();
            ////todo: dynamically load objects? is this even possible?
            //return itemsToReturn;
        }

        private T Load<T>(Stream stream, LoadDataDelegate<T> del)
        {
            T result = default(T);
            using(SpreadsheetDocument document = SpreadsheetDocument.Open(stream, false))
            {
                Workbook workbook = document.WorkbookPart.Workbook;
                SharedStringTable sharedStrings = document.WorkbookPart.SharedStringTablePart.SharedStringTable;

                result = del(document, workbook, sharedStrings);

                document.Close();
            }

            return result;
        }

        private delegate T LoadDataDelegate<T>(SpreadsheetDocument document, Workbook workbook, SharedStringTable sharedStrings);
    }
}