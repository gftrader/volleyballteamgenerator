﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace VolleyballTeamGenerator.Infrastructure.OpenXml
{
    public static class OpenXmlDocumentHelper
    {
        public static Worksheet GetSheet(Workbook workbook, string sheetName)
        {
            if (!String.IsNullOrEmpty(sheetName))
            {
                return GetWorkSheetPart(workbook.WorkbookPart, sheetName).Worksheet;
            }
            else
            {
                return GetWorkSheetPart(workbook.WorkbookPart, 0).Worksheet;
            }
        }

        public static WorksheetPart GetWorkSheetPart(WorkbookPart workbookPart, string sheetName)
        {
            return (WorksheetPart)workbookPart.GetPartById(workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault(s => sheetName.Equals(s.Name)).Id);
        }

        public static WorksheetPart GetWorkSheetPart(WorkbookPart workbookPart, int sheetPosition)
        {
            return (WorksheetPart)workbookPart.GetPartById(workbookPart.Workbook.Descendants<Sheet>().ToList()[sheetPosition].Id);
        }
    }
}
