﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Reflection;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace VolleyballTeamGenerator.Infrastructure.OpenXml
{
    public class OpenXmlExcelDocumentExporter : IExcelExporter
    {
        private Dictionary<string,List<OpenXmlExportDataCollection>> addedCollections = new Dictionary<string,List<OpenXmlExportDataCollection>>();

        public OpenXmlExcelDocumentExporter()
        {
            DocumentName = "Sheet1";
            InputFilters = new Dictionary<string, IDictionary<string, string>>();
        }

        public IDictionary<string, IDictionary<string, string>> InputFilters { get; set; }
        public string DocumentName { get; set; }

        public void AddInputFilters(IDictionary<string, string> inputFilters, string sheetName = "")
        {
            if (String.IsNullOrEmpty(sheetName))
            {
                sheetName = DocumentName;
            }
            if (!InputFilters.ContainsKey(sheetName))
            {
                InputFilters.Add(sheetName, new Dictionary<string, string>());
            }

            foreach (string key in inputFilters.Keys)
            {
                if (!InputFilters[sheetName].ContainsKey(key))
                {
                    InputFilters[sheetName].Add(key, inputFilters[key]);
                }
            }
        }

        public void AddList<T>(IList<ExportDocumentProperty> objectProperties, IList<T> objectsToAdd, string sheetName="")
        {
            sheetName = VerifySheetExists(sheetName);
            addedCollections[sheetName].Add(new OpenXmlExportList<T>(objectProperties, objectsToAdd));
        }

        public void AddDynamicList(IList<ExportDocumentProperty> objectProperties, IList<ExpandoObject> objectsToAdd, string sheetName="")
        {
            sheetName = VerifySheetExists(sheetName);
            addedCollections[sheetName].Add(new OpenXmlExportDynamicList(objectProperties, objectsToAdd));
        }

        public void AddDataTable(IDictionary<string, string> columnHeadingMappings, DataTable dataToAdd, string sheetName = "")
        {
            sheetName = VerifySheetExists(sheetName);
            addedCollections[sheetName].Add(new OpenXmlExportDataTable(columnHeadingMappings, dataToAdd));
        }

        private string VerifySheetExists(string sheetName)
        {
            if (String.IsNullOrEmpty(sheetName))
            {
                sheetName = DocumentName;
            }

            if (!addedCollections.ContainsKey(sheetName))
            {
                addedCollections.Add(sheetName, new List<OpenXmlExportDataCollection>());
            }

            return sheetName;
        }

        public void SaveToDisk(string saveDirectory, string saveToFilename)
        {
            string fullFilePath = String.Format(@"{0}\{1}", saveDirectory, saveToFilename);
            using (SpreadsheetDocument spreadsheet = OpenXmlExcelDocument.CreateWorkbook(fullFilePath))
            {
                SaveAs(spreadsheet);
            }
        }

        public void SaveAsStream(IStream stream)
        {
            using (SpreadsheetDocument spreadsheet = OpenXmlExcelDocument.CreateWorkbook(stream.GetStream()))
            {
                SaveAs(spreadsheet);
            }
        }

        private void SaveAs(SpreadsheetDocument spreadsheet)
        {
            OpenXmlExcelDocument.AddBasicStyles(spreadsheet);
            foreach (string worksheetName in GetAllSheetNames())
            {
                OpenXmlExcelDocument.AddWorksheet(spreadsheet, worksheetName);
            }

            AddDataToSpreadsheet(spreadsheet);

            spreadsheet.WorkbookPart.Workbook.Save();
            spreadsheet.Close();
        }

        private IList<string> GetAllSheetNames()
        {
            List<string> sheetNames = new List<string>();
            foreach (string key in InputFilters.Keys)
            {
                sheetNames.Add(key);
            }

            foreach (string key in addedCollections.Keys)
            {
                if (!sheetNames.Contains(key)) { sheetNames.Add(key); }
            }

            return sheetNames;
        }

        private void AddDataToSpreadsheet(SpreadsheetDocument spreadsheet)
        {
            foreach (string sheetName in GetAllSheetNames())
            {
                uint currentSheetRow = 1;
                Worksheet worksheet = OpenXmlDocumentHelper.GetSheet(spreadsheet.WorkbookPart.Workbook, sheetName);
                AddInputFiltersToSpreadsheet(ref currentSheetRow, spreadsheet, sheetName, worksheet);
                AddDataToSpreadsheet(ref currentSheetRow, spreadsheet, sheetName, worksheet);
                worksheet.Save();
            }
        }

        private void AddInputFiltersToSpreadsheet(ref uint currentRow, SpreadsheetDocument spreadsheet, string sheetName, Worksheet worksheet)
        {
            if (this.InputFilters.ContainsKey(sheetName))
            {
                foreach (string filterName in this.InputFilters[sheetName].Keys)
                {
                    OpenXmlExcelDocument.SetCellValue(spreadsheet, worksheet, 1, currentRow, filterName, false, false);
                    OpenXmlExcelDocument.SetCellValue(spreadsheet, worksheet, 2, currentRow, this.InputFilters[sheetName][filterName], false, false);
                    currentRow++;
                }
                currentRow++;
            }
        }

        private void AddDataToSpreadsheet(ref uint currentRow, SpreadsheetDocument spreadsheet, string sheetName, Worksheet worksheet)
        {
            if (this.addedCollections.ContainsKey(sheetName))
            {
                foreach (OpenXmlExportDataCollection xdc in addedCollections[sheetName])
                {
                    xdc.AddDataToWorksheet(ref currentRow, spreadsheet, worksheet);
                }
            }
        }

        public string HttpResponseStreamContentType
        {
            get { return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; }
        }
    }
}