﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Text;

namespace VolleyballTeamGenerator.Infrastructure.OpenXml
{
    public interface IExcelExporter : IDocumentExporter 
    {
        /// <summary>
        /// Adds a list of input filters to the document that represent the report parameters
        /// </summary>
        /// <param name="inputFilters">A key - value representation of the inputs used to generate this document</param>
        /// <param name="sheetName">The page (or sheet) where these inputs should appear</param>
        void AddInputFilters(IDictionary<string, string> inputFilters, string sheetName = "");

        /// <summary>
        /// Adds a list of the specified objects to output in the document
        /// </summary>
        /// <typeparam name="T">The type of object that is being saved</typeparam>
        /// <param name="objectProperties">A collection of property names with formatting to be used in output</param>
        /// <param name="objectsToAdd">The actual objects to be added to the document</param>
        /// <param name="sheetName">The page (or sheet) where the objects should be saved</param>
        void AddList<T>(IList<ExportDocumentProperty> objectProperties, IList<T> objectsToAdd, string sheetName = "");

        /// <summary>
        /// Adds a list of ExpandoObjects to output in the document
        /// </summary>
        /// <param name="objectProperties">A collection of property names with formatting to be used in output</param>
        /// <param name="objectsToAdd">The actual objects to be added to the document</param>
        /// <param name="sheetName">The page (or sheet) where the objects should be saved</param>
        void AddDynamicList(IList<ExportDocumentProperty> objectProperties, IList<ExpandoObject> objectsToAdd, string sheetName = "");

        /// <summary>
        /// Adds a DataTable to output in the document
        /// </summary>
        /// <param name="columnHeadingMappings">A mapping of datatable column names to document column names</param>
        /// <param name="dataToAdd">The actual datatable to be added to the document</param>
        /// <param name="sheetName">The page (or sheet) where the objects should be saved</param>
        void AddDataTable(IDictionary<string, string> columnHeadingMappings, DataTable dataToAdd, string sheetName = "");
    }
}