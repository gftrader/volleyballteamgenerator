﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace VolleyballTeamGenerator.Infrastructure.OpenXml
{
    public abstract class OpenXmlExportDataCollection
    {
        public abstract void AddDataToWorksheet(ref uint currentRow, SpreadsheetDocument spreadsheet, Worksheet worksheet);
    }
}
