﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Dynamic;
using System.Reflection;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace VolleyballTeamGenerator.Infrastructure.OpenXml
{
    public class OpenXmlExportDynamicList : OpenXmlExportDataCollection
    {
        private IList<ExportDocumentProperty> objectProperties;
        private IList<ExpandoObject> objectsToAdd;

        public OpenXmlExportDynamicList(IList<ExportDocumentProperty> objectProperties, IList<ExpandoObject> objectsToAdd)
        {
            this.objectProperties = objectProperties;
            this.objectsToAdd = objectsToAdd;
        }

        public override void AddDataToWorksheet(ref uint currentRow, SpreadsheetDocument spreadsheet, Worksheet worksheet)
        {
            uint currentColumn = 1;
            foreach (ExportDocumentProperty xdocProperty in objectProperties)
            {
                OpenXmlExcelDocument.SetCellValue(spreadsheet, worksheet, currentColumn, currentRow, xdocProperty.DocumentHeadingText, false, false);
                currentColumn++;
            }
            currentRow++;

            foreach (ExpandoObject objectToAdd in objectsToAdd)
            {
                currentColumn = 1;

                foreach (ExportDocumentProperty xdocProperty in objectProperties)
                {
                    IDictionary<string, object> dynamicObject = ((IDictionary<string, object>)objectToAdd);
                    string stringValue = String.Format(xdocProperty.DataFormatString, (dynamicObject.ContainsKey(xdocProperty.ObjectPropertyName) ? dynamicObject[xdocProperty.ObjectPropertyName] : ""));
                    OpenXmlExcelDocument.SetCellValue(spreadsheet, worksheet, currentColumn, currentRow, stringValue, false, false);
                    currentColumn++;
                }

                currentRow++;
            }
        }
    }
}
