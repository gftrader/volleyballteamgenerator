﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace VolleyballTeamGenerator.Infrastructure.OpenXml
{
    public class OpenXmlExportDataTable : OpenXmlExportDataCollection 
    {
        private IDictionary<string, string> columnHeadingMappings;
        private DataTable dataToAdd;

        public OpenXmlExportDataTable(IDictionary<string, string> columnHeadingMappings, DataTable dataToAdd)
        {
            this.columnHeadingMappings = columnHeadingMappings;
            this.dataToAdd = dataToAdd;
        }

        public override void AddDataToWorksheet(ref uint currentRow, SpreadsheetDocument spreadsheet, Worksheet worksheet)
        {
            uint currentColumn = 1;

            uint headerRow = currentRow++;
            foreach (DataRow dr in dataToAdd.Rows)
            {
                currentColumn = 1;
                foreach (DataColumn column in dataToAdd.Columns)
                {
                    if (columnHeadingMappings != null && columnHeadingMappings.ContainsKey(column.ColumnName))
                    {
                        OpenXmlExcelDocument.SetCellValue(spreadsheet, worksheet, currentColumn, headerRow, columnHeadingMappings[column.ColumnName], false, false);
                    }
                    else
                    {
                        OpenXmlExcelDocument.SetCellValue(spreadsheet, worksheet, currentColumn, headerRow, column.ColumnName, false, false);
                    }

                    string stringValue = null;
                    object value = dr[column.ColumnName];
                    stringValue = (value == null) ? null : value.ToString();

                    OpenXmlExcelDocument.SetCellValue(spreadsheet, worksheet, currentColumn, currentRow, stringValue, false, false);
                    currentColumn++;
                }

                currentRow++;
            }
        }
    }
}
