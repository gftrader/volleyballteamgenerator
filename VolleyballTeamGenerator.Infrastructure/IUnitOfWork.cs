﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Infrastructure
{
    public interface IUnitOfWork
    {
        void RegisterNew<T>(T entity, IUnitOfWorkRepository<T> uowr) where T : class, IAggregateRoot;
        void RegisterAmmended<T>(T entity, IUnitOfWorkRepository<T> uowr) where T : class, IAggregateRoot;
        void RegisterRemoved<T>(T entity, IUnitOfWorkRepository<T> uowr) where T : class, IAggregateRoot;
        void Commit();    
    }
}