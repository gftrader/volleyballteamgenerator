﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace VolleyballTeamGenerator.Infrastructure
{
    public static class UIHelper
    {
        public static void BindListControl(ListControl control, object dataSource, string dataTextField, string dataValueField, bool hasDefaultValue=false, string defaultText="",string defaultValue="-1")
        {
            control.DataSource = dataSource;
            control.DataTextField = dataTextField;
            control.DataValueField = dataValueField;
            control.DataBind();

            if (hasDefaultValue)
            {
                control.Items.Insert(0, new ListItem(defaultText, defaultValue));
            }
        }

        public static IList<int> GetSelectedIDValues(ListControl control)
        {
            List<int> selectedValues = new List<int>();
            foreach (ListItem li in control.Items)
            {
                if (li.Selected)
                {
                    selectedValues.Add(Convert.ToInt32(li.Value));
                }
            }

            return selectedValues;
        }
    }
}
