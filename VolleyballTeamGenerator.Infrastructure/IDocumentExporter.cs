﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Dynamic;
using System.IO;

namespace VolleyballTeamGenerator.Infrastructure
{
    public interface IDocumentExporter
    {
        string DocumentName { get; set; }
        string HttpResponseStreamContentType { get; }

        /// <summary>
        /// Saves the document to the provided stream
        /// </summary>
        /// <param name="stream">A stream to save the document</param>
        void SaveAsStream(IStream stream);

        /// <summary>
        /// Saves the document to a location on the server
        /// </summary>
        /// <param name="saveDirectory">The directory where the document should save</param>
        /// <param name="saveToFilename">The filename of the document</param>
        void SaveToDisk(string saveDirectory, string saveToFilename);
    }
}