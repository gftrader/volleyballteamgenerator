﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VolleyballTeamGenerator.Infrastructure
{
    public interface IUnitOfWorkRepository<T> where T : class, IAggregateRoot
    {
        void PersistCreationOf(T entity);
        void PersistUpdateOf(T entity);
        void PersistDeletionOf(T entity);
    }
}
