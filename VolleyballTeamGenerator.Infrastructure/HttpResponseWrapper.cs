﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace VolleyballTeamGenerator.Infrastructure
{
    public interface IHttpResponse
    {
        void Redirect(string url, bool endResponse);
    }

    public class HttpResponseWrapper : IHttpResponse
    {
        private HttpResponse response;
        public HttpResponseWrapper(HttpResponse response)
        {
            this.response = response;
        }

        public void Redirect(string url, bool endResponse)
        {
            this.response.Redirect(url, endResponse);
        }
    }

    public static class HttpResponseFactory
    {
        public static IHttpResponse Response { private get; set; }
        public static HttpResponse ActualResponse { private get; set; }
        public static IHttpResponse Create()
        {
            if (Response != null)
            {
                return Response;
            }
            else if(ActualResponse != null)
            {
                return new HttpResponseWrapper(ActualResponse);
            }

            throw new Exception("HttpResponseFactory requires a Response or HttpResponse object");
        }
    }
}
