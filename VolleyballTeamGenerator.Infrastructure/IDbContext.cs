﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace VolleyballTeamGenerator.Infrastructure
{
    public interface IDbContext
    {
        IDbSet<T> Set<T>() where T : class;
        void Commit();
    }

    public class FakeDbSet<T> : IDbSet<T> where T : class
    {
        private IList<T> entities = new List<T>();

        public T Add(T entity)
        {
            entities.Add(entity);
            return entity;
        }

        public T Attach(T entity)
        {
            return Add(entity);
        }

        public TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, T
        {
            throw new NotImplementedException();
        }

        public T Create()
        {
            throw new NotImplementedException();
        }

        public T Find(params object[] keyValues)
        {
            throw new NotImplementedException();
        }

        public System.Collections.ObjectModel.ObservableCollection<T> Local
        {
            get { throw new NotImplementedException(); }
        }

        public T Remove(T entity)
        {
            entities.Remove(entity);
            return entity;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return entities.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return entities.GetEnumerator();
        }

        public Type ElementType
        {
            get { return typeof(T); }
        }

        public System.Linq.Expressions.Expression Expression
        {
            get { return entities.AsQueryable().Expression; }
        }

        public IQueryProvider Provider
        {
            get { return entities.AsQueryable().Provider; }
        }
    }
}