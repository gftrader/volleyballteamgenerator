﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace VolleyballTeamGenerator.Infrastructure
{
    public interface IStream : IDisposable
    {
        Stream GetStream();
        long Position { get; set; }
        void CopyTo(Stream destinationStream);
    }

    public class StreamWrapper : IStream
    {
        private Stream stream;
        public StreamWrapper(Stream stream)
        {
            this.stream = stream;
        }

        public void Dispose()
        {
            stream.Dispose();
        }

        public long Position
        {
            get
            {
                return stream.Position;
            }
            set
            {
                stream.Position = value;
            }
        }

        public Stream GetStream()
        {
            return stream;
        }

        public void CopyTo(Stream destinationStream)
        {
            stream.CopyTo(destinationStream);
        }
    }

    public static class IStreamFactory
    {
        public static IStream Stream { get; set; }
        public static Stream FileStream { get; set; }
        public static StreamTypeEnum StreamType { get; set; }
        public static IStream Create()
        {
            if(Stream != null)
            {
                return Stream;
            }
            else if(FileStream != null)
            {
                return new StreamWrapper(FileStream);
            }
            else if(StreamType != null)
            {
                return new StreamWrapper(GetStream(StreamType));
            }

            throw new Exception("IStreamFactory requires either an instance of either IStream or StreamTypeEnum");
        }

        private static Stream GetStream(StreamTypeEnum streamType)
        {
            switch(streamType)
            {
                case StreamTypeEnum.MemoryStream: return new MemoryStream();
                default: throw new Exception("No Stream Type defined");
            }
        }
    }

    public enum StreamTypeEnum
    {
        MemoryStream
    }
}
