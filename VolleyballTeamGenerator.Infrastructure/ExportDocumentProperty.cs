﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VolleyballTeamGenerator.Infrastructure
{
    public class ExportDocumentProperty
    {
        public ExportDocumentProperty()
        {
            DataFormatString = "{0}";
        }

        public ExportDocumentProperty(string objectPropertyName, string documentHeadingTExt)
        {
            ObjectPropertyName = objectPropertyName;
            DocumentHeadingText = documentHeadingTExt;
            DataFormatString = "{0}";
        }

        public ExportDocumentProperty(string objectPropertyName, string documentHeadingText, string dataFormatString)
        {
            ObjectPropertyName = objectPropertyName;
            DocumentHeadingText = documentHeadingText;
            DataFormatString = dataFormatString;
        }

        public string ObjectPropertyName { get; set; }
        public string DocumentHeadingText { get; set; }
        public string DataFormatString { get; set; }
    }
}