﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructureMap;

namespace VolleyballTeamGenerator.Infrastructure
{
    public interface IIoCContainer
    {
        T ResolveDependency<T>();
    }

    public class StructureMapIocContainer : IIoCContainer
    {
        private IContainer container;
        public StructureMapIocContainer(IContainer container)
        {
            this.container = container;
        }

        public T ResolveDependency<T>()
        {
            return container.GetInstance<T>();
        }
    }
}
