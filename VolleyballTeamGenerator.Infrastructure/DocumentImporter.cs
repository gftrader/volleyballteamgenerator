﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VolleyballTeamGenerator.Infrastructure
{
    public static class DocumentImporter
    {
        public static IDocumentImporter CreateDocumentImporter(string filename)
        {
            string fileExtension = null;

            try
            {
                fileExtension = filename.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries)[1];
            }
            catch (Exception)
            {
                throw new Exception("Filename must have valid extension");
            }

            switch (fileExtension)
            {
                case "csv": return new CSV.CSVImporter();
                case "xlsx": return new OpenXml.OpenXmlExcelDocumentImporter();
                default: throw new Exception(String.Format("No import format available for file extension '{0}'", fileExtension));
            }
        }
    }
}
