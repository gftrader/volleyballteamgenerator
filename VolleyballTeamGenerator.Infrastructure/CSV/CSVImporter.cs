﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic.FileIO;

namespace VolleyballTeamGenerator.Infrastructure.CSV
{
    public class CSVImporter : IDocumentImporter
    {
        public DataTable LoadDataTable(Stream stream)
        {
            DataTable dt = new DataTable();
            bool firstLine = true;

            using (var csvReader = new TextFieldParser(stream))
            {
                csvReader.SetDelimiters(new string[] { "," });
                csvReader.HasFieldsEnclosedInQuotes = true;

                while (!csvReader.EndOfData)
                {
                    string[] splitLine = csvReader.ReadFields();
                    if (firstLine)
                    {
                        foreach (string columnName in splitLine)
                        {
                            dt.Columns.Add(columnName);
                        }
                        firstLine = false;
                    }
                    else
                    {
                        DataRow dr = dt.NewRow();
                        for (int i = 0; i < splitLine.Length; i++)
                        {
                            dr[i] = splitLine[i];
                        }
                        dt.Rows.Add(dr);
                    }
                }
            }

            return dt;
        }

        public IList<T> LoadItems<T>(Stream stream)
        {
            throw new NotImplementedException();
        }
    }
}
