﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VolleyballTeamGenerator.Infrastructure
{
    public interface IRepository<T, TID>
    {
        T FindById(TID id);
        IList<T> FindAll();

        void Add(T entity);
        void Save(T entity);
        void Remove(T entity);
    }
}
