﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Domain.RepositoryInterfaces
{
    public interface IVolleyballAttributeRepository : IRepository<VolleyballAttribute, int>
    {
        VolleyballAttribute FindByName(string skillName);
    }
}
