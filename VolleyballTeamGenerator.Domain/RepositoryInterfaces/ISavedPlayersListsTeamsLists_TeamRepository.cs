﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Domain.RepositoryInterfaces
{
    public interface ISavedPlayersListsTeamsLists_TeamRepository : IRepository<SavedPlayersListsTeamsLists_Team, int>
    {
    }
}
