﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain.Messages;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Domain.RepositoryInterfaces
{
    public interface IPlayerRepository : IRepository<Player, int>
    {
        Player FindByName(string firstName, string LastName);
        IList<Player> FindPlayers(FindPlayersRequest fpRequest);
    }
}
