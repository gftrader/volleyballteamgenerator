﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Domain.RepositoryInterfaces
{
    public interface ISavedPlayersListRepository : IRepository<SavedPlayersList, int>
    {
        IList<SavedPlayersList> FindByPlayerGroupId(int playerGroupId);
        SavedPlayersList FindByGroupIdAndName(int playerGroupId, string savedPlayersListName);
    }
}
