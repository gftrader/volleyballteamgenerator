﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Domain.RepositoryInterfaces
{
    public interface ISavedPlayersListsTeamsListRepository : IRepository<SavedPlayersListsTeamsList, int>
    {
        SavedPlayersListsTeamsList FindBySavedPlayersListIdAndName(int savedPlayerListId, string savedPlayersListName);
        IList<SavedPlayersListsTeamsList> FindBySavedPlayersList(int savedPlayersListId);
    }
}
