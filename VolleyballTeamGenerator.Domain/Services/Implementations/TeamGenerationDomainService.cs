﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain.Messages;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Domain.RepositoryInterfaces;
using VolleyballTeamGenerator.Domain.Services.Interfaces;
using VolleyballTeamGenerator.Domain.Strategies;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Domain.Services.Implementations
{
    public class TeamGenerationDomainService : ITeamGenerationDomainService
    {
        private IUnitOfWork uow;
        private IPlayerRepository playerRepository;
        
        public TeamGenerationDomainService(IUnitOfWork uow, IPlayerRepository playerRepository)
        {
            this.uow = uow;
            this.playerRepository = playerRepository;
        }

        public GenerateTeamsDomainResponse GenerateTeams(GenerateTeamsDomainRequest gtReq)
        {
            GenerateTeamsDomainResponse gtRes = new GenerateTeamsDomainResponse();

            var playersToUse = playerRepository.FindPlayers(new FindPlayersRequest() { PlayerIds = gtReq.PlayerIDsToUse });

            TeamCompositionStrategy tcStrategy;
            switch (gtReq.TeamCompositionStrategy)
            {
                case CompositionStrategyEnum.Random: tcStrategy = new RandomTeamStrategy(); break;
                case CompositionStrategyEnum.GagesIntermediate: tcStrategy = new BalancedTeamStrategy(); break;
                case CompositionStrategyEnum.GagesCoedIntermediate: tcStrategy = new GagesCoedIntermediate(); break;
                case CompositionStrategyEnum.BestPlayerAvailable: tcStrategy = new BPATeamStrategy(); break;
                case CompositionStrategyEnum.DansPowerSixes: tcStrategy = new DansPowerSixesStrategy(); break;
                case CompositionStrategyEnum.DansPowerFives: tcStrategy = new DansPowerFivesStrategy(); break;
                case CompositionStrategyEnum.DansPowerThreesAndFours: tcStrategy = new DansPowerThreesAndFoursStrategy(); break;
                default: throw new Exception("Team Composition Strategy not supported");
            }

            gtRes.Teams = tcStrategy.ComposeTeams(gtReq.PlayerGroupId, gtReq.NumberOfTeams, playersToUse);
            return gtRes;
        }

        public IList<CompositionStrategy> GetAllCompositionStrategies()
        {
            List<CompositionStrategy> cStrategies = new List<CompositionStrategy>();
            foreach(string enumName in Enum.GetNames(typeof(CompositionStrategyEnum)))
            {
                cStrategies.Add(new CompositionStrategy((CompositionStrategyEnum)Enum.Parse(typeof(CompositionStrategyEnum), enumName)));
            }

            return cStrategies;
        }
    }
}