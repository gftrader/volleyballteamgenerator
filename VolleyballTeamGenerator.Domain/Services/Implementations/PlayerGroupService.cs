﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain.Messages;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Domain.RepositoryInterfaces;
using VolleyballTeamGenerator.Domain.Services.Interfaces;
using VolleyballTeamGenerator.Domain.Strategies;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Domain.Services.Implementations
{
    public class PlayerGroupService : IPlayerGroupService
    {
        private IUnitOfWork uow;
        private IPlayerGroupRepository playerGroupRepository;

        public PlayerGroupService(IUnitOfWork uow, IPlayerGroupRepository playerGroupRepository)
        {
            this.uow = uow;
            this.playerGroupRepository = playerGroupRepository;
        }

        public PlayerGroup GetPlayerGroup(int id)
        {
            return playerGroupRepository.FindById(id);
        }

        public IList<PlayerGroup> GetAllPlayerGroups()
        {
            return playerGroupRepository.FindAll();
        }

        public IList<Player> GetPlayersInPlayerGroup(int playerGroupID)
        {
            PlayerGroup selectedGroup = playerGroupRepository.FindById(playerGroupID);
            return (selectedGroup != null) ? selectedGroup.Players.Where(p => p.Active).OrderBy(p => p.FirstName).ThenBy(p => p.LastName).ToList() : new List<Player>();
        }
    }
}
