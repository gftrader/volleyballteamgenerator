﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Domain.RepositoryInterfaces;
using VolleyballTeamGenerator.Domain.Services.Interfaces;
using VolleyballTeamGenerator.Domain.Strategies;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Domain.Services.Implementations
{
    public class VolleyballAttributeService : IVolleyballAttributeService
    {
        private IUnitOfWork uow;
        private IVolleyballAttributeRepository vaRepository;

        public VolleyballAttributeService(IUnitOfWork uow, IVolleyballAttributeRepository vaRepository)
        {
            this.uow = uow;
            this.vaRepository = vaRepository;
        }

        public IList<VolleyballAttribute> GetAllVolleyballAttributes()
        {
            return vaRepository.FindAll();
        }

        public VolleyballAttribute GetVolleyballAttribute(int volleyballAttributeId)
        {
            return vaRepository.FindById(volleyballAttributeId);
        }

        public VolleyballAttribute GetVolleyballAttributeByName(string volleyballAttributeName)
        {
            return vaRepository.FindByName(volleyballAttributeName);
        }
    }
}
