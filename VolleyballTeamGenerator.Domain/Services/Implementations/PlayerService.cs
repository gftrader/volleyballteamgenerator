﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain.Messages;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Domain.RepositoryInterfaces;
using VolleyballTeamGenerator.Domain.Services.Interfaces;
using VolleyballTeamGenerator.Domain.Strategies;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Domain.Services.Implementations
{
    public class PlayerService : IPlayerService
    {
        private IPlayerRepository playerRepository;
        private IPlayerGroupRepository playerGroupRepository;
        private IVolleyballAttributeRepository vaRepository;
        private IUnitOfWork uow;

        public PlayerService(IUnitOfWork uow, IPlayerRepository playerRepository, IPlayerGroupRepository playerGroupRepository, IVolleyballAttributeRepository vaRepository)
        {
            this.uow = uow;
            this.playerRepository = playerRepository;
            this.playerGroupRepository = playerGroupRepository;
            this.vaRepository = vaRepository;
        }

        public IList<Player> GetAllPlayersInGroup(int groupID)
        {
            return playerRepository.FindPlayers(new FindPlayersRequest() { PlayerGroupIds = new List<int>() { groupID } });
        }

        public Player GetPlayer(int id)
        {
            return playerRepository.FindById(id);
        }

        public IList<Player> GetAllPlayers()
        {
            return playerRepository.FindAll();
        }

        public IList<Player> FindPlayers(FindPlayersRequest fpRequest)
        {
            return playerRepository.FindPlayers(fpRequest);
        }

        public void AddOrUpdatePlayers(IList<Player> players)
        {
            if (players.Count > 0)
            {
                foreach (Player player in players)
                {
                    if (player.Id > 0)
                    {
                        UpdatePlayerInRepository(player);
                    }
                    else
                    {
                        AddPlayerToRepository(player);
                    }
                }

                uow.Commit();
            }
        }

        public void AddPlayer(Player player)
        {            
            AddPlayerToRepository(player);
            uow.Commit();
        }

        private void AddPlayerToRepository(Player playerToAdd)
        {
            ValidatePlayerName(-1, playerToAdd.FirstName, playerToAdd.LastName);

            List<int> playerGroupIDs = playerToAdd.PlayerGroups.Select(p => p.Id).ToList();
            playerToAdd.PlayerGroups.Clear();
            foreach (int playerGroupID in playerGroupIDs)
            {
                playerToAdd.PlayerGroups.Add(playerGroupRepository.FindById(playerGroupID));
            }

            foreach (PlayerAttribute pa in playerToAdd.PlayerAttributes)
            {
                pa.Player = playerToAdd;
                pa.VolleyballAttribute = vaRepository.FindById(pa.VolleyballAttributeId);
                pa.PlayerGroup = playerGroupRepository.FindById(pa.PlayerGroupId);
            }

            playerRepository.Add(playerToAdd);
        }

        public void UpdatePlayer(Player player)
        {
            UpdatePlayerInRepository(player);
            uow.Commit();
        }

        private void UpdatePlayerInRepository(Player playerToUpdate)
        {
            Player dbPlayer = playerRepository.FindById(playerToUpdate.Id);
            if (dbPlayer != null)
            {
                ValidatePlayerName(playerToUpdate.Id, playerToUpdate.FirstName, playerToUpdate.LastName);
                dbPlayer.FirstName = playerToUpdate.FirstName;
                dbPlayer.MiddleName = playerToUpdate.MiddleName;
                dbPlayer.LastName = playerToUpdate.LastName;
                dbPlayer.Email = playerToUpdate.Email;
                dbPlayer.Active = playerToUpdate.Active;
                dbPlayer.Gender = playerToUpdate.Gender;
                dbPlayer.Comments = playerToUpdate.Comments;

                AddPlayerGroupsToPlayer(dbPlayer, playerToUpdate.PlayerGroups);
                AddPlayerAttributesToPlayer(dbPlayer, playerToUpdate.PlayerAttributes, true);
            }
            else { throw new Exception(String.Format("Player '{0}' does not exist in the database", playerToUpdate.Id)); }
        }

        private void AddPlayerAttributesToPlayer(Player dbPlayer, IList<PlayerAttribute> playerAttributesToAdd, bool overwriteExistingValues)
        {
            foreach (PlayerAttribute pa in playerAttributesToAdd)
            {
                VolleyballAttribute va = vaRepository.FindById(pa.VolleyballAttributeId);
                PlayerAttribute dbPlayerAttribute = dbPlayer.PlayerAttributes.Where(dbPA => dbPA.VolleyballAttributeId == va.Id && dbPA.PlayerGroupId == pa.PlayerGroupId).FirstOrDefault();
                if (dbPlayerAttribute == null)
                {
                    pa.VolleyballAttributeId = va.Id;
                    dbPlayer.PlayerAttributes.Add(pa);
                }
                else if(overwriteExistingValues)
                {
                    dbPlayerAttribute.VolleyballAttributeLevel = pa.VolleyballAttributeLevel;
                }
            }
        }

        private void ValidatePlayerName(int id, string firstName, string lastName)
        {
            Player playerWithMatchingName = playerRepository.FindByName(firstName, lastName);
            if (playerWithMatchingName != null && playerWithMatchingName.Id != id)
            {
                throw new Exception(String.Format("There is already another player with the name '{0} {1}'", firstName, lastName));
            }
        }

        public void UpdatePlayerGroup(int playerId, int groupId)
        {
            var dbPlayer = playerRepository.FindById(playerId);
            if(dbPlayer != null)
            {
                var groupToRemove = dbPlayer.PlayerGroups.FirstOrDefault(g => g.Id == groupId);
                if(groupToRemove != null)
                {
                    dbPlayer.PlayerGroups.Remove(groupToRemove);
                    var attributesToRemove = dbPlayer.PlayerAttributes.Where(pa => pa.PlayerGroupId == groupId).ToList();
                    foreach(var attributeToRemove in attributesToRemove)
                    {
                        dbPlayer.PlayerAttributes.Remove(attributeToRemove);
                    }
                }
                else
                {
                    var dbPlayerGroup = playerGroupRepository.FindById(groupId);
                    dbPlayer.PlayerGroups.Add(dbPlayerGroup);
                    foreach(var attributeToAdd in vaRepository.FindAll())
                    {
                        dbPlayer.PlayerAttributes.Add(new PlayerAttribute()
                        {
                            PlayerId = dbPlayer.Id,
                            PlayerGroupId = dbPlayerGroup.Id,
                            VolleyballAttributeId = attributeToAdd.Id,
                            VolleyballAttributeLevel = 1,
                        });
                    }                    
                }

                uow.Commit();
            }
            else { throw new Exception(String.Format("Player with Id '{0}' does not exist in the database", playerId)); }
        }

        public void UpdatePlayerGroups(Player playerToUpdate)
        {
            Player dbPlayer = playerRepository.FindById(playerToUpdate.Id);
            if (dbPlayer != null)
            {
                AddPlayerGroupsToPlayer(dbPlayer, playerToUpdate.PlayerGroups);
                RemoveMissingGroupsFromPlayer(dbPlayer, playerToUpdate);
                AddPlayerAttributesToPlayer(dbPlayer, playerToUpdate.PlayerAttributes, false);
                uow.Commit();
            }
            else { throw new Exception(String.Format("Player '{0}' does not exist in the database", playerToUpdate.Id)); }
        }

        private static void RemoveMissingGroupsFromPlayer(Player dbPlayer, Player playerToUpdate)
        {
            List<PlayerGroup> playerGroupsToRemove = new List<PlayerGroup>();
            List<PlayerAttribute> playerAttributesToRemove = new List<PlayerAttribute>();
            foreach (PlayerGroup dbPlayerGroup in dbPlayer.PlayerGroups)
            {
                if (!playerToUpdate.PlayerGroups.Any(gp => gp.Id == dbPlayerGroup.Id))
                {
                    playerGroupsToRemove.Add(dbPlayerGroup);
                    foreach (PlayerAttribute pa in dbPlayer.PlayerAttributes.Where(dbpa => dbpa.PlayerGroupId == dbPlayerGroup.Id))
                    {
                        playerAttributesToRemove.Add(pa);
                    }
                }
            }
            foreach (PlayerGroup dbPlayerGroup in playerGroupsToRemove)
            {
                dbPlayer.PlayerGroups.Remove(dbPlayerGroup);
            }
            foreach (PlayerAttribute dbPlayerAttribute in playerAttributesToRemove)
            {
                dbPlayer.PlayerAttributes.Remove(dbPlayerAttribute);
            }
        }

        private void AddPlayerGroupsToPlayer(Player dbPlayer, IList<PlayerGroup> playerGroupsToAdd)
        {
            foreach (PlayerGroup pg in playerGroupsToAdd)
            {
                if (!dbPlayer.PlayerGroups.Any(dbPG => dbPG.Id == pg.Id))
                {
                    dbPlayer.PlayerGroups.Add(playerGroupRepository.FindById(pg.Id));
                }
            }
        }

        public void UpdatePlayerAttribute(PlayerAttribute playerAttribute)
        {
            Player dbPlayer = playerRepository.FindById(playerAttribute.PlayerId);
            if (dbPlayer != null)
            {
                PlayerAttribute dbPlayerAttribute = dbPlayer.PlayerAttributes.FirstOrDefault(pa => pa.PlayerGroupId == playerAttribute.PlayerGroupId && pa.VolleyballAttributeId == playerAttribute.VolleyballAttributeId);
                if (dbPlayerAttribute == null)
                {
                    dbPlayer.PlayerAttributes.Add(new PlayerAttribute()
                    {
                        PlayerId = dbPlayer.Id,
                        PlayerGroupId = playerAttribute.PlayerGroupId,
                        PlayerGroup = playerGroupRepository.FindById(playerAttribute.PlayerGroupId),
                        VolleyballAttributeId = playerAttribute.VolleyballAttributeId,
                        VolleyballAttribute = vaRepository.FindById(playerAttribute.VolleyballAttributeId),
                        VolleyballAttributeLevel = playerAttribute.VolleyballAttributeLevel,
                    });
                }
                else
                {
                    dbPlayerAttribute.VolleyballAttributeLevel = playerAttribute.VolleyballAttributeLevel;
                }

                uow.Commit();
            }
            else
            {
                throw new Exception(String.Format("Player with ID {0} not found in the system", playerAttribute.PlayerId));
            }
        }

        public void DeletePlayer(Player player)
        {
            DeletePlayerFromRepository(player);
            uow.Commit();
        }
        
        private void DeletePlayerFromRepository(Player player)
        {
            Player dbPlayer = playerRepository.FindById(player.Id);
            if (dbPlayer != null)
            {
                playerRepository.Remove(player);
            }
            else { throw new Exception(String.Format("Player '{0}' does not exist in the database", player.Id)); }
        }
    }
}