﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain.Messages;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Domain.RepositoryInterfaces;
using VolleyballTeamGenerator.Domain.Services.Interfaces;
using VolleyballTeamGenerator.Domain.Strategies;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Domain.Services.Implementations
{
    public class SavedPlayerListService : ISavedPlayerListService
    {
        private IUnitOfWork uow;
        private ISavedPlayersListRepository savedPlayersListRepository;
        private ISavedPlayersListsTeamsListRepository savedPlayerListTeamsListRepository;
        private ISavedPlayersListsTeamsLists_TeamRepository savedPlayerListTeamsListTeamRepository;
        private IPlayerRepository playerRepository;

        public SavedPlayerListService
        (
            IUnitOfWork uow, 
            ISavedPlayersListRepository savedPlayersListRepository, 
            ISavedPlayersListsTeamsListRepository savedPlayerListTeamsListRepository, 
            ISavedPlayersListsTeamsLists_TeamRepository savedPlayerListTeamsListTeamRepository,
            IPlayerRepository playerRepository
        )
        {
            this.uow = uow;
            this.savedPlayersListRepository = savedPlayersListRepository;
            this.savedPlayerListTeamsListRepository = savedPlayerListTeamsListRepository;
            this.savedPlayerListTeamsListTeamRepository = savedPlayerListTeamsListTeamRepository;
            this.playerRepository = playerRepository;
        }

        public SavedPlayersList GetSavedPlayersList(int savedPlayersListId)
        {
            return savedPlayersListRepository.FindById(savedPlayersListId);
        }

        public IEnumerable<SavedPlayersListsTeamsList> GetSavedPlayersListsTeamLists(int savedPlayersListId)
        {
            return savedPlayerListTeamsListRepository.FindBySavedPlayersList(savedPlayersListId);
        }

        public SavedPlayersListsTeamsList GetSavedPlayersListsTeamList(int savedPlayersListTeamsListId)
        {
            return savedPlayerListTeamsListRepository.FindById(savedPlayersListTeamsListId);
        }

        public IList<SavedPlayersList> GetSavedPlayersListsByGroupId(int playerGroupId)
        {
            return savedPlayersListRepository.FindByPlayerGroupId(playerGroupId);
        }

        public SavedPlayersList GetSavedPlayersListByGroupIdAndName(int playerGroupId, string savedPlayersListName)
        {
            return savedPlayersListRepository.FindByGroupIdAndName(playerGroupId, savedPlayersListName);
        }

        public void AddSavedPlayersList(SavedPlayersList savedPlayersList)
        {
            SavedPlayersList dbSPL = savedPlayersListRepository.FindByGroupIdAndName(savedPlayersList.PlayerGroupId, savedPlayersList.Name);
            if (dbSPL == null)
            {
                List<int> Ids = savedPlayersList.Players.Select(p => p.Id).ToList();
                savedPlayersList.Players = playerRepository.FindPlayers(new FindPlayersRequest() { PlayerIds = Ids });
                savedPlayersListRepository.Add(savedPlayersList);
                uow.Commit();
            }
            else
            {
                throw new Exception(String.Format("Saved Players List with name '{0}' already exists", savedPlayersList.Name));
            }
        }

        public void AddOrUpdateSavedPlayersList(int playerGroupId, string listName, IEnumerable<int> playerIds)
        {
            var dbSPL = savedPlayersListRepository.FindByGroupIdAndName(playerGroupId, listName);
            if(dbSPL == null)
            {
                dbSPL = new SavedPlayersList()
                {
                    PlayerGroupId = playerGroupId,
                    Name = listName
                };
                savedPlayersListRepository.Add(dbSPL);
            }

            dbSPL.Players.Clear();
            dbSPL.Players = playerRepository.FindPlayers(new FindPlayersRequest() { PlayerIds = playerIds });
            uow.Commit();
        }

        public void AddOrUpdateSavedPlayersListTeamList(int savedPlayerListId, string listName, IEnumerable<Team> inputTeams)
        {
            var dbSPLTL = savedPlayerListTeamsListRepository.FindBySavedPlayersListIdAndName(savedPlayerListId, listName);
            if(dbSPLTL == null)
            {
                dbSPLTL = new SavedPlayersListsTeamsList()
                {
                    Name = listName,
                    SavedPlayersListId = savedPlayerListId,
                    SavedPlayersListsTeamsLists_Teams = new List<SavedPlayersListsTeamsLists_Team>()
                };
                savedPlayerListTeamsListRepository.Add(dbSPLTL);
            }
            else
            {
                RemoveSavedPlayersListsTeamDependencies(dbSPLTL);
            }

            foreach(var team in inputTeams)
            {
                var dbTeam = new SavedPlayersListsTeamsLists_Team()
                {
                    Name = team.Name,
                    Players = new List<Player>()
                };
                dbTeam.Players = playerRepository.FindPlayers(new FindPlayersRequest() { PlayerIds = team.Players.Select(p => p.Id) });
                dbSPLTL.SavedPlayersListsTeamsLists_Teams.Add(dbTeam);
            }
            uow.Commit();
        }

        public void UpdateSavedPlayersList(SavedPlayersList updatedSPL)
        {
            SavedPlayersList dbSPL = savedPlayersListRepository.FindById(updatedSPL.Id);
            if (dbSPL != null)
            {
                dbSPL.Players.Clear();
                foreach (Player player in updatedSPL.Players)
                {
                    dbSPL.Players.Add(playerRepository.FindById(player.Id));
                }

                uow.Commit();
            }
            else
            {
                throw new Exception(String.Format("Saved Player List {0} not found in database", updatedSPL.Id));
            }
        }

        public void DeleteSavedPlayersList(int id)
        {
            SavedPlayersList dbSPL = savedPlayersListRepository.FindById(id);
            if (dbSPL != null)
            {
                dbSPL.Players.Clear();
                var teamLists = dbSPL.SavedPlayersListsTeamsLists.ToList();
                dbSPL.SavedPlayersListsTeamsLists.Clear();
                foreach(var spltl in teamLists)
                {
                    RemoveSavedPlayersListsTeamDependencies(spltl);
                    savedPlayerListTeamsListRepository.Remove(spltl);
                }
                
                savedPlayersListRepository.Remove(dbSPL);
                uow.Commit();
            }
            else
            {
                throw new Exception(String.Format("Saved Player List {0} not found in database", id));
            }
        }

        public void DeleteSavedPlayersListsTeamsList(int id)
        {
            var dbSPLTL = savedPlayerListTeamsListRepository.FindById(id);
            if(dbSPLTL != null)
            {
                RemoveSavedPlayersListsTeamDependencies(dbSPLTL);
                
                savedPlayerListTeamsListTeamRepository.FindById(id);
                savedPlayerListTeamsListRepository.Remove(dbSPLTL);
                uow.Commit();
            }
            else
            {
                throw new Exception(String.Format("Saved Player List Team List {0} not found in database", id));
            }
        }

        private void RemoveSavedPlayersListsTeamDependencies(SavedPlayersListsTeamsList dbSPLTL)
        {
            var teamsToDelete = new List<SavedPlayersListsTeamsLists_Team>();
            foreach (var team in dbSPLTL.SavedPlayersListsTeamsLists_Teams)
            {
                team.Players.Clear();
                teamsToDelete.Add(team);
            }

            dbSPLTL.SavedPlayersListsTeamsLists_Teams.Clear();
            foreach (var teamToDelete in teamsToDelete)
            {
                savedPlayerListTeamsListTeamRepository.Remove(teamToDelete);
            }
        }
    }
}