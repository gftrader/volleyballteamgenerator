﻿using System;
using System.Collections.Generic;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Domain.Services.Interfaces
{
    public interface IVolleyballAttributeService
    {
        IList<VolleyballAttribute> GetAllVolleyballAttributes();
        VolleyballAttribute GetVolleyballAttribute(int volleyballAttributeId);
        VolleyballAttribute GetVolleyballAttributeByName(string volleyballAttributeName);
    }
}
