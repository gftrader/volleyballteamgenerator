﻿using System;
using System.Collections.Generic;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Domain.Services.Interfaces
{
    public interface ISavedPlayerListService
    {
        void AddSavedPlayersList(SavedPlayersList savedPlayersList);
        void DeleteSavedPlayersList(int id);
        void DeleteSavedPlayersListsTeamsList(int id);
        SavedPlayersList GetSavedPlayersList(int savedPlayersListId);
        SavedPlayersList GetSavedPlayersListByGroupIdAndName(int playerGroupId, string savedPlayersListName);
        IList<SavedPlayersList> GetSavedPlayersListsByGroupId(int playerGroupId);
        IEnumerable<SavedPlayersListsTeamsList> GetSavedPlayersListsTeamLists(int savedPlayersListId);
        SavedPlayersListsTeamsList GetSavedPlayersListsTeamList(int savedPlayersListTeamsListId);
        void AddOrUpdateSavedPlayersList(int playerGroupId, string listName, IEnumerable<int> playerIds);
        void AddOrUpdateSavedPlayersListTeamList(int savedPlayerListId, string listName, IEnumerable<Team> teams);
        void UpdateSavedPlayersList(SavedPlayersList updatedSPL);
    }
}
