﻿using System;
using System.Collections.Generic;
using VolleyballTeamGenerator.Domain.Messages;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Domain.Services.Interfaces
{
    public interface ITeamGenerationDomainService
    {
        GenerateTeamsDomainResponse GenerateTeams(GenerateTeamsDomainRequest gtReq);
        IList<CompositionStrategy> GetAllCompositionStrategies();
    }
}
