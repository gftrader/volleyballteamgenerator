﻿using System;
using System.Collections.Generic;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Domain.Services.Interfaces
{
    public interface IPlayerGroupService
    {
        IList<PlayerGroup> GetAllPlayerGroups();
        PlayerGroup GetPlayerGroup(int id);
        IList<Player> GetPlayersInPlayerGroup(int playerGroupID);
    }
}
