﻿using System;
using System.Collections.Generic;
using VolleyballTeamGenerator.Domain.Messages;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Domain.Services.Interfaces
{
    public interface IPlayerService
    {
        void AddOrUpdatePlayers(IList<Player> players);
        void AddPlayer(Player player);
        void DeletePlayer(Player player);
        IList<Player> FindPlayers(FindPlayersRequest fpRequest);
        IList<Player> GetAllPlayers();
        IList<Player> GetAllPlayersInGroup(int groupID);
        Player GetPlayer(int id);
        void UpdatePlayer(Player player);
        void UpdatePlayerAttribute(PlayerAttribute playerAttribute);
        void UpdatePlayerGroup(int playerId, int groupId);
        void UpdatePlayerGroups(Player playerToUpdate);
    }
}
