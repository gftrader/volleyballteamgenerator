﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Domain.Models
{
    public class PlayerGroup : IAggregateRoot
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual IList<Player> Players { get; set; }
        public virtual IList<PlayerAttribute> PlayerAttributes { get; set; }
        public virtual IList<SavedPlayersList> SavedPlayersLists { get; set; }

        public PlayerGroup()
        {
            Players = new List<Player>();
            PlayerAttributes = new List<PlayerAttribute>();
            SavedPlayersLists = new List<SavedPlayersList>();
        }
    }
}
