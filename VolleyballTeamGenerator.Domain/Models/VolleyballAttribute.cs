﻿using System.Collections.Generic;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Domain.Models
{
    public class VolleyballAttribute : IAggregateRoot
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public VolleyballAttributeEnum VolleyballAttributeEnum
        {
            get { return (VolleyballAttributeEnum)Id; }
        }

        public override string ToString()
        {
            return Name;
        }
    }

    //note: overall is a sum of the other attributes and isn't stored in the database
    public enum VolleyballAttributeEnum
    {
        Hitting=1,
        Blocking=2,
        Setting=3,
        VolleyballIQ=4,
        Mobility=5,
        Verticality=6,
        Defense=7,
        Passing=8,
        Overall=1000,
    }
}