﻿using System;
using System.Collections.Generic;
using System.Linq;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Domain.Models
{
    public class Player : IAggregateRoot
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string Comments { get; set; }
        public bool Active { get; set; }
        public virtual IList<PlayerAttribute> PlayerAttributes { get; set; }
        public virtual IList<PlayerGroup> PlayerGroups { get; set; }
        public virtual IList<SavedPlayersList> SavedPlayersLists { get; set; }
        public virtual IList<SavedPlayersListsTeamsLists_Team> SavedPlayersListsTeamsLists_Teams { get; set; }

        public double GetAttributeLevel(int playerGroupID, VolleyballAttributeEnum attributeToCompare)
        {
            PlayerAttribute playerAttribute = this.PlayerAttributes.Where(pa => pa.PlayerGroupId == playerGroupID && pa.VolleyballAttributeId == (int)attributeToCompare).FirstOrDefault();
            if (attributeToCompare == VolleyballAttributeEnum.Overall && playerAttribute == null)
            {
                playerAttribute = new PlayerAttribute()
                {
                    Player = this,
                    PlayerGroupId = playerGroupID,
                    VolleyballAttribute = new VolleyballAttribute() { Id = (int)VolleyballAttributeEnum.Overall, Name = "Overall" },
                    VolleyballAttributeLevel = PlayerAttributes.Sum(pa => pa.VolleyballAttributeLevel)
                };
                PlayerAttributes.Add(playerAttribute);
            }
            return playerAttribute.VolleyballAttributeLevel;
        }

        public int CompareTo(Player otherPlayer, int playerGroupID, VolleyballAttributeEnum skillToCompare)
        {
            double thisPlayersSkill = this.GetAttributeLevel(playerGroupID, skillToCompare);
            double otherPlayersSkill = otherPlayer.GetAttributeLevel(playerGroupID, skillToCompare);

            return thisPlayersSkill.CompareTo(otherPlayersSkill);
        }

        public Player()
        {
            PlayerAttributes = new List<PlayerAttribute>();
            PlayerGroups = new List<PlayerGroup>();
            SavedPlayersLists = new List<SavedPlayersList>();
            SavedPlayersListsTeamsLists_Teams = new List<SavedPlayersListsTeamsLists_Team>();
        }
    }
}