﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VolleyballTeamGenerator.Domain.Models
{
    public class CompositionStrategy
    {
        private CompositionStrategyEnum cse;
        private static Dictionary<CompositionStrategyEnum, string> compositionStrategies = null;

        public CompositionStrategy(CompositionStrategyEnum cse)
        {
            this.cse = cse;
            if (compositionStrategies == null) { InitializeCompositionStrategies(); }
        }

        public CompositionStrategyEnum Enum
        {
            get { return cse; }
        }

        public int Id
        {
            get { return (int)cse; }
        }

        public string Name
        {
            get { return compositionStrategies[Enum]; }
        }

        private void InitializeCompositionStrategies()
        {
            compositionStrategies = new Dictionary<CompositionStrategyEnum, string>();
            compositionStrategies.Add(CompositionStrategyEnum.GagesIntermediate, "Gage's Intermediate/Low-Power");
            compositionStrategies.Add(CompositionStrategyEnum.GagesCoedIntermediate, "Gage's Co-Ed Intermediate");
            compositionStrategies.Add(CompositionStrategyEnum.BestPlayerAvailable, "Best Player Available");
            compositionStrategies.Add(CompositionStrategyEnum.DansPowerThreesAndFours, "Dan's Power 3s/4s");
            compositionStrategies.Add(CompositionStrategyEnum.DansPowerFives, "Dan's Power 5s");
            compositionStrategies.Add(CompositionStrategyEnum.DansPowerSixes, "Dan's Power 6s");
            compositionStrategies.Add(CompositionStrategyEnum.Random, "Random");
        }
    }

    public enum CompositionStrategyEnum
    {
        Random,
        GagesIntermediate,
        GagesCoedIntermediate,
        BestPlayerAvailable,
        DansPowerSixes,
        DansPowerFives,
        DansPowerThreesAndFours,
    }
}