﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Domain.Models
{
    public class SavedPlayersListsTeamsList : IAggregateRoot
    {
        public int Id { get; set; }
        public int SavedPlayersListId { get; set; }
        public string Name { get; set; }

        public virtual IList<SavedPlayersListsTeamsLists_Team> SavedPlayersListsTeamsLists_Teams { get; set; }
        public virtual SavedPlayersList SavedPlayersList { get; set; }

        public SavedPlayersListsTeamsList()
        {
            SavedPlayersListsTeamsLists_Teams = new List<SavedPlayersListsTeamsLists_Team>();
        }
    }
}
