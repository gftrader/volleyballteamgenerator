﻿using System.Collections.Generic;
using System.Linq;

namespace VolleyballTeamGenerator.Domain.Models
{
    //NOT AN EF CLASS!
    public class Team
    {
        public string Name { get; set; }
        public virtual IList<Player> Players { get; set; }

        public double OverallSkillLevel
        {
            get
            {
                return Players.Sum(p => p.PlayerAttributes.Sum(pa => pa.VolleyballAttributeLevel));
            }
        }

        public override string ToString()
        {
            return Name;
        }

        public Team()
        {
            Players = new List<Player>();
        }
    }
}