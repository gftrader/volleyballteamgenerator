﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Domain.Models
{
    public class SavedPlayersListsTeamsLists_Team : IAggregateRoot
    {
        public int Id { get; set; }
        public int SavedPlayersListsTeamsListId { get; set; }
        public string Name { get; set; }

        public virtual IList<Player> Players { get; set; }
        public virtual SavedPlayersListsTeamsList SavedPlayersListsTeamsList { get; set; }

        public SavedPlayersListsTeamsLists_Team()
        {
            Players = new List<Player>();
        }
    }
}
