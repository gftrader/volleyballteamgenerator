﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Domain.Models
{
    public class SavedPlayersList : IAggregateRoot
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PlayerGroupId { get; set; }

        public virtual PlayerGroup PlayerGroup { get; set; }
        public virtual IList<Player> Players { get; set; }
        public virtual IList<SavedPlayersListsTeamsList> SavedPlayersListsTeamsLists { get; set; }

        public SavedPlayersList()
        {
            Players = new List<Player>();
            SavedPlayersListsTeamsLists = new List<SavedPlayersListsTeamsList>();
        }
    }
}
