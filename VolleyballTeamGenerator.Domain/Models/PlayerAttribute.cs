﻿using System;
using System.Collections.Generic;

namespace VolleyballTeamGenerator.Domain.Models
{
    public class PlayerAttribute : IComparable
    {
        public int PlayerId { get; set; }
        public int PlayerGroupId { get; set; }
        public int VolleyballAttributeId { get; set; }
        public double VolleyballAttributeLevel { get; set; }
        public virtual Player Player { get; set; }
        public virtual PlayerGroup PlayerGroup { get; set; }
        public virtual VolleyballAttribute VolleyballAttribute { get; set; }

        public override string ToString()
        {
            return string.Format("Player: {0}, Skill: {1}, Level: {2}", Player.ToString(), VolleyballAttribute.ToString(), VolleyballAttributeLevel.ToString());
        }

        public int CompareTo(object obj)
        {
            if (obj is PlayerAttribute)
            {
                return this.VolleyballAttributeLevel.CompareTo(((PlayerAttribute)obj).VolleyballAttributeLevel);
            }
            else
            {
                throw new Exception(String.Format("Cannot compare Player Attribute to non-player Attribute type {0}", obj.GetType()));
            }
        }
    }
}