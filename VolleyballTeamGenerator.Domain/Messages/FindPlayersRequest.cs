﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VolleyballTeamGenerator.Domain.Messages
{
    public class FindPlayersRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public IEnumerable<int> PlayerIds { get; set; }
        public IEnumerable<int> PlayerGroupIds { get; set; }

        public FindPlayersRequest()
        {
            PlayerIds = new List<int>();
            PlayerGroupIds = new List<int>();
        }
    }
}
