﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Domain.Messages
{
    public class GenerateTeamsDomainRequest
    {
        public CompositionStrategyEnum TeamCompositionStrategy { get; set; }
        public int PlayerGroupId { get; set; }
        public int NumberOfTeams { get; set; }
        public IEnumerable<int> PlayerIDsToUse { get; set; }
    }
}
