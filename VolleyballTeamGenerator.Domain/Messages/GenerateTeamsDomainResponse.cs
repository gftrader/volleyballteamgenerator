﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Domain.Messages
{
    public class GenerateTeamsDomainResponse
    {
        public IEnumerable<Team> Teams { get; set; }
    }
}
