﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Domain.Strategies
{
    public class BPATeamStrategy : TeamCompositionStrategy
    {
        public override void AddPlayersToTeams(int playerGroupId, IList<Player> players, IEnumerable<Team> teams)
        {
            var remainingPlayers = players;

            bool takeFromHeadOfList = true;
            while (remainingPlayers.Count > 0)
            {
                SortAndAdd(teams, remainingPlayers, playerGroupId, new List<VolleyballAttributeEnum>(){ VolleyballAttributeEnum.Overall, VolleyballAttributeEnum.Blocking, VolleyballAttributeEnum.Hitting, VolleyballAttributeEnum.Setting, VolleyballAttributeEnum.Defense, VolleyballAttributeEnum.Verticality, VolleyballAttributeEnum.VolleyballIQ, VolleyballAttributeEnum.Mobility }, takeFromHeadOfList);
                takeFromHeadOfList = !takeFromHeadOfList;
            }
        }
    }
}
