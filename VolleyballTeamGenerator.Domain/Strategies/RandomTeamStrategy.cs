﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Domain.Strategies
{
    public class RandomTeamStrategy : TeamCompositionStrategy
    {
        public override void AddPlayersToTeams(int playerGroupId, IList<Player> players, IEnumerable<Team> teams)
        {
            var playersListCopy = new List<Player>();
            foreach(Player player in players)
            {
                playersListCopy.Add(player);
            }

            int lastTeam = 0;
            while(playersListCopy.Count > 0)
            {
                Team team = teams.ElementAt(lastTeam);
                int playerIndex = new Random().Next(playersListCopy.Count-1);
                team.Players.Add(playersListCopy.ElementAt(playerIndex));
                playersListCopy.RemoveAt(playerIndex);
                lastTeam = (lastTeam + 1) % teams.Count();
            }
        }
    }
}