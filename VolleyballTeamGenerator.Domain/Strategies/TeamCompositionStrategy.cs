﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Domain.RepositoryInterfaces;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Domain.Strategies
{
    public abstract class TeamCompositionStrategy
    {
        public TeamCompositionStrategy()
        {
        }

        public IEnumerable<Team> ComposeTeams(int playerGroupId, int numberOfTeams, IList<Player> players)
        {
            var teams = new List<Team>();
            for (int i = 0; i < numberOfTeams; i++)
            {
                teams.Add(new Team() { Name = String.Format("Team {0}", i + 1) });
            }

            AddPlayersToTeams(playerGroupId, players, teams);

            return teams;
        }

        public abstract void AddPlayersToTeams(int playerGroupId, IList<Player> players, IEnumerable<Team> teams);

        public static void SortAndAdd(IEnumerable<Team> teams, IList<Player> remainingPlayers, int playerGroupID, IEnumerable<VolleyballAttributeEnum> attributesToSortBy, bool takeFromHeadOfList)
        {
            SortPlayersBySkill(playerGroupID, remainingPlayers, attributesToSortBy);

            double minPlayersPerTeam = (teams.Sum(t => t.Players.Count()) + remainingPlayers.Count) / teams.Count();
            if (minPlayersPerTeam >= 5 || (remainingPlayers.Count / teams.Count()) > 0 || ((remainingPlayers.Count % teams.Count()) == 0))
            {
                int teamIndex = (takeFromHeadOfList) ? 0 : teams.Count() - 1;
                int playersAllocated = 0;
                while (playersAllocated < teams.Count() && remainingPlayers.Count > 0)
                {
                    teams.ElementAt(teamIndex).Players.Add(remainingPlayers[0]);
                    remainingPlayers.RemoveAt(0);
                    playersAllocated++;

                    teamIndex = (takeFromHeadOfList) ? teamIndex + 1 : teamIndex - 1;
                }
            }
            else
            {
                foreach (Team team in teams.OrderByDescending(t => t.OverallSkillLevel))
                {
                    if (remainingPlayers.Count > 0)
                    {
                        team.Players.Add(remainingPlayers[remainingPlayers.Count-1]);
                        remainingPlayers.RemoveAt(remainingPlayers.Count-1);
                    }
                }
            }
        }

        private static void SortPlayersBySkill(int playerGroupID, IList<Player> playersToSort, IEnumerable<VolleyballAttributeEnum> attributesToSortBy)
        {
            if (playersToSort.Count > 0)
            {
                SortedList<double, Player> playersSortedByAttributeLevel = new SortedList<double, Player>();
                foreach (Player player in playersToSort)
                {
                    double level = 0;
                    double subLevel = 1;

                    //the order of the attributes to sort by matters very much - the further down the list, the less it matters for breaking ties
                    foreach (VolleyballAttributeEnum attributeToSortBy in attributesToSortBy)
                    {
                        level += subLevel * player.GetAttributeLevel(playerGroupID, attributeToSortBy);
                        subLevel *= .1;
                    }

                    //if there is still a tie, then just pick one (last person wins)
                    do
                    {
                        level += subLevel;
                        subLevel *= .1;
                    }
                    while (playersSortedByAttributeLevel.ContainsKey(level));
                    
                    playersSortedByAttributeLevel.Add(level, player);
                }

                List<Player> sortedPlayersList = playersSortedByAttributeLevel.Values.ToList();

                playersToSort.Clear();
                for (int i = sortedPlayersList.Count-1; i >= 0; i--)
                {
                    playersToSort.Add(sortedPlayersList[i]);
                }
            }
        }
    }
}