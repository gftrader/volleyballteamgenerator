﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Domain.RepositoryInterfaces;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.Domain.Strategies
{
    public class BalancedTeamStrategy : TeamCompositionStrategy
    {
        public override void AddPlayersToTeams(int playerGroupId, IList<Player> players, IEnumerable<Team> teams)
        {
            var remainingPlayers = players;

            SortAndAdd(teams, remainingPlayers, playerGroupId, new List<VolleyballAttributeEnum>() { VolleyballAttributeEnum.Hitting, VolleyballAttributeEnum.Blocking, VolleyballAttributeEnum.Overall }, true);
            SortAndAdd(teams, remainingPlayers, playerGroupId, new List<VolleyballAttributeEnum>() { VolleyballAttributeEnum.Hitting, VolleyballAttributeEnum.Blocking, VolleyballAttributeEnum.Setting }, false);
            SortAndAdd(teams, remainingPlayers, playerGroupId, new List<VolleyballAttributeEnum>() { VolleyballAttributeEnum.Setting, VolleyballAttributeEnum.Hitting, VolleyballAttributeEnum.Blocking }, true);
            SortAndAdd(teams, remainingPlayers, playerGroupId, new List<VolleyballAttributeEnum>() { VolleyballAttributeEnum.Mobility, VolleyballAttributeEnum.Overall }, false);

            bool takeFromHead = true;
            while (remainingPlayers.Count > 0)
            {
                SortAndAdd(teams, remainingPlayers, playerGroupId, new List<VolleyballAttributeEnum>() { VolleyballAttributeEnum.Overall, VolleyballAttributeEnum.VolleyballIQ }, takeFromHead);
                takeFromHead = !takeFromHead;
            }
        }
    }
}