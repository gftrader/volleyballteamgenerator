﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Domain.Strategies
{
    public class GagesCoedIntermediate : TeamCompositionStrategy
    {
        public override void AddPlayersToTeams(int playerGroupId, IList<Player> players, IEnumerable<Team> teams)
        {
            var remainingPlayers = players;

            var femalePlayers = remainingPlayers.Where(p => p.Gender.Equals("F")).ToList();
            var coedPlayers = remainingPlayers.Where(p => p.Gender.Equals("M")).ToList();
            SortAndAdd(teams, femalePlayers, playerGroupId, new List<VolleyballAttributeEnum>() { VolleyballAttributeEnum.Overall, VolleyballAttributeEnum.Setting }, true);
            foreach (Player unusedFemalePlayer in femalePlayers)
            {
                coedPlayers.Add(unusedFemalePlayer);
            }
            SortAndAdd(teams, coedPlayers, playerGroupId, new List<VolleyballAttributeEnum>() { VolleyballAttributeEnum.Hitting, VolleyballAttributeEnum.Blocking, VolleyballAttributeEnum.Overall }, false);
            SortAndAdd(teams, coedPlayers, playerGroupId, new List<VolleyballAttributeEnum>() { VolleyballAttributeEnum.Hitting, VolleyballAttributeEnum.Blocking, VolleyballAttributeEnum.Setting }, true);
            SortAndAdd(teams, coedPlayers, playerGroupId, new List<VolleyballAttributeEnum>() { VolleyballAttributeEnum.Setting, VolleyballAttributeEnum.Hitting, VolleyballAttributeEnum.Blocking }, false);

            bool takeFromHead = true;
            while (coedPlayers.Count > 0)
            {
                SortAndAdd(teams, coedPlayers, playerGroupId, new List<VolleyballAttributeEnum>() { VolleyballAttributeEnum.Overall, VolleyballAttributeEnum.VolleyballIQ }, takeFromHead);
                takeFromHead = !takeFromHead;
            }
        }
    }
}
