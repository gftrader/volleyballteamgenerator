﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolleyballTeamGenerator.Domain.Models;

namespace VolleyballTeamGenerator.Domain.Strategies
{
    public class DansPowerFivesStrategy : TeamCompositionStrategy
    {
        public override void AddPlayersToTeams(int playerGroupId, IList<Player> players, IEnumerable<Team> teams)
        {
            var remainingPlayers = players;

            SortAndAdd(teams, remainingPlayers, playerGroupId, new List<VolleyballAttributeEnum>() { VolleyballAttributeEnum.Blocking, VolleyballAttributeEnum.Hitting, VolleyballAttributeEnum.Verticality, VolleyballAttributeEnum.Setting, VolleyballAttributeEnum.Defense }, true);
            SortAndAdd(teams, remainingPlayers, playerGroupId, new List<VolleyballAttributeEnum>() { VolleyballAttributeEnum.Blocking, VolleyballAttributeEnum.Hitting, VolleyballAttributeEnum.Verticality, VolleyballAttributeEnum.Setting, VolleyballAttributeEnum.Defense }, false);
            SortAndAdd(teams, remainingPlayers, playerGroupId, new List<VolleyballAttributeEnum>() { VolleyballAttributeEnum.Setting, VolleyballAttributeEnum.VolleyballIQ }, true);
            SortAndAdd(teams, remainingPlayers, playerGroupId, new List<VolleyballAttributeEnum>() { VolleyballAttributeEnum.Setting, VolleyballAttributeEnum.VolleyballIQ }, false);
            SortAndAdd(teams, remainingPlayers, playerGroupId, new List<VolleyballAttributeEnum>() { VolleyballAttributeEnum.Hitting, VolleyballAttributeEnum.Overall }, true);
        }
    }
}
