﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VolleyballTeamGenerator.MVC.Startup))]
namespace VolleyballTeamGenerator.MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
