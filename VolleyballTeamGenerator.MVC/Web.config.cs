﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.MVC.ProjectConfiguration
{
    public static class AppSettings
    {
        public static string AdminUserId
        {
            get { return ConfigurationManager.AppSettings["AdminUserId"]; }
        }

        public static string Environment
        {
            get { return ConfigurationManager.AppSettings["Environment"]; }
        }

        public static EnvironmentEnum EnvironmentEnum
        {
            get { return (EnvironmentEnum)Enum.Parse(typeof(EnvironmentEnum), Environment); }
        }
    }

    public static class ConnectionStrings
    {
        public static string VolleyballTeamGeneratorEntities
        {
            get { return ConfigurationManager.ConnectionStrings["VolleyballTeamGeneratorEntities"].ConnectionString; }
        }
    }

    public enum EnvironmentEnum
    {
        Development,
        Production
    }
}