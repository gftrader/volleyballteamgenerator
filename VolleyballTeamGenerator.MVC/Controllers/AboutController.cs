﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VolleyballTeamGenerator.MVC.Controllers
{
    public class AboutController : AuthorizedController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}