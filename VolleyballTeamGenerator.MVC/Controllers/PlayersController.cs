﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using VolleyballTeamGenerator.Domain.Messages;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Domain.Services.Interfaces;
using VolleyballTeamGenerator.Application.ViewModels;
using VolleyballTeamGenerator.Application.Messaging;
using VolleyballTeamGenerator.Infrastructure.OpenXml;
using VolleyballTeamGenerator.Infrastructure;
using Newtonsoft.Json;
using System.Linq.Expressions;
using VolleyballTeamGenerator.MVC.Mvc;
using System.IO;

namespace VolleyballTeamGenerator.MVC.Controllers
{
    public class PlayersController : AuthorizedController
    {
        private IPlayerService playerService { get; set; }
        private ISavedPlayerListService savedPlayerListService { get; set; }
        private IVolleyballAttributeService volleyballAttributeService { get; set; }
        private IExcelExporter excelExporter { get; set; }
        public PlayersController() : this(MvcApplication.IoCContainer.ResolveDependency<IPlayerService>(),
            MvcApplication.IoCContainer.ResolveDependency<ISavedPlayerListService>(),
            MvcApplication.IoCContainer.ResolveDependency<IVolleyballAttributeService>(),
            MvcApplication.IoCContainer.ResolveDependency<IExcelExporter>()) { }

        public PlayersController(IPlayerService playerService, ISavedPlayerListService savedPlayerListService, IVolleyballAttributeService volleyballAttributeService, IExcelExporter excelExporter)
        {
            this.playerService = playerService;
            this.savedPlayerListService = savedPlayerListService;
            this.volleyballAttributeService = volleyballAttributeService;
            this.excelExporter = excelExporter;
        }

        public ActionResult Index()
        {
            return View("FindPlayers");
        }

        public ActionResult BulkImportExport()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ImportPlayers(HttpPostedFileBase file)
        {
            if (file.ContentLength > 0)
            {
                var documentImporter = DocumentImporter.CreateDocumentImporter(file.FileName);
                IStreamFactory.FileStream = file.InputStream;
                using (var stream = IStreamFactory.Create())
                {
                    DataTable dt = documentImporter.LoadDataTable(stream.GetStream());
                    return Json(LoadPlayerUpdateViewsFromDataTable(dt));
                }
            }
            else
            {
                throw new Exception("Uploaded file does not exist or has no content");
            }
        }

        public FileResult ExportPlayers(int playerGroupId)
        {
            IList<string> playerAttributeColumnNames = new List<string>();
            var propertyNames = GetRequiredColumnNames(ref playerAttributeColumnNames);

            excelExporter.DocumentName = "PlayerExport";
            excelExporter.AddDataTable(null, GetDataTable(playerGroupId, playerAttributeColumnNames, propertyNames));

            IStreamFactory.StreamType = StreamTypeEnum.MemoryStream;
            var stream = IStreamFactory.Create();
            excelExporter.SaveAsStream(stream);
            return new FileGeneratingResult("volleyball-player-export.xlsx", excelExporter.HttpResponseStreamContentType, stream);
        }

        public ActionResult FindPlayers()
        {
            return View();
        }

        public ActionResult ManagePlayer(int id)
        {
            ViewBag.PlayerId = id;
            return View();
        }

        public ActionResult AddPlayer()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Get(int playerId)
        {
            return Json(Mapper.Map<Player, PlayerView>(playerService.GetPlayer(playerId)), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAllPlayerGroups()
        {
            return Json(Mapper.Map<IEnumerable<PlayerGroup>, IEnumerable<PlayerGroupView>>(MvcApplication.IoCContainer.ResolveDependency<IPlayerGroupService>().GetAllPlayerGroups()), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAllPlayersInGroup(int groupId)
        {
            return Json(Mapper.Map<IEnumerable<Player>, IEnumerable<PlayerView>>(playerService.GetAllPlayersInGroup(groupId)), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSavedPlayerListsInGroup(int groupId)
        {
            return Json(Mapper.Map<IEnumerable<SavedPlayersList>, IEnumerable<SavedPlayersListSummaryView>>(savedPlayerListService.GetSavedPlayersListsByGroupId(groupId)), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAllPlayersInList(int listId)
        {
            return Json(Mapper.Map<IEnumerable<Player>, IEnumerable<PlayerSummaryView>>(savedPlayerListService.GetSavedPlayersList(listId).Players), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSavedPlayersListsTeamList(int savedPlayersListId)
        {
            return Json(Mapper.Map<IEnumerable<SavedPlayersListsTeamsList>, IEnumerable<SavedPlayersListsTeamsListView>>(savedPlayerListService.GetSavedPlayersListsTeamLists(savedPlayersListId)), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSavedPlayersListsTeamListTeam(int id)
        {
            return Json(Mapper.Map<SavedPlayersListsTeamsList, SavedPlayersListsTeamsListView>(savedPlayerListService.GetSavedPlayersListsTeamList(id)), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetPlayerAttributes(int playerId)
        {
            return Json(Mapper.Map<Player, PlayerView>(playerService.GetPlayer(playerId)).PlayerAttributes, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult FindMatchingPlayers(string findPlayersRequest)
        {
            var findPlayersRequestObject = JsonConvert.DeserializeObject<FindPlayersRequest>(findPlayersRequest);
            return Json(Mapper.Map<IEnumerable<Player>, IEnumerable<PlayerSummaryView>>(playerService.FindPlayers(findPlayersRequestObject)), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Post(PlayerView playerView)
        {
            var player = GetPlayer(playerView);
            playerService.AddPlayer(player);
            return Json(Mapper.Map<Player, PlayerView>(player));
        }

        [HttpPut]
        //api/Players/Put/1/PlayerGroups/2
        public void UpdatePlayerGroup(int playerId, int groupId)
        {
            playerService.UpdatePlayerGroup(playerId, groupId);
        }

        [HttpPut]
        public void Put(int id, PlayerSummaryView player)
        {
            playerService.UpdatePlayer(GetPlayer(player));
        }

        [HttpPut]
        public void UpdatePlayerAttribute(int playerId, PlayerAttributeView playerAttribute)
        {
            playerService.UpdatePlayerAttribute(GetPlayerAttribute(playerAttribute));
        }

        [HttpPut]
        public void SavePlayerList(int playerGroupId, string listName, IEnumerable<int> playerIds)
        {
            savedPlayerListService.AddOrUpdateSavedPlayersList(playerGroupId, listName, playerIds);
        }

        [HttpPut]
        public void SavePlayerListsTeamList(SavePlayerListsTeamListParameters inputParams)
        {
            savedPlayerListService.AddOrUpdateSavedPlayersListTeamList(inputParams.SavedPlayerListId, inputParams.ListName, GetTeamsFromTeamViews(inputParams.Teams));
        }

        [HttpDelete]
        public void RemoveSavedPlayersList(int savedPlayersListId)
        {
            savedPlayerListService.DeleteSavedPlayersList(savedPlayersListId);
        }

        [HttpDelete]
        public void RemoveSavedPlayersListsTeamsList(int id)
        {
            savedPlayerListService.DeleteSavedPlayersListsTeamsList(id);
        }


        private Player GetPlayer(PlayerSummaryView player)
        {
            return new Player()
            {
                Id = player.Id,
                FirstName = player.FirstName,
                MiddleName = player.MiddleName,
                LastName = player.LastName,
                Email = player.Email,
                Active = player.Active,
                Gender = player.Gender,
                Comments = player.Comments,
            };
        }

        private PlayerAttribute GetPlayerAttribute(PlayerAttributeView playerAttribute)
        {
            return new PlayerAttribute()
            {
                PlayerId = playerAttribute.PlayerId,
                VolleyballAttributeId = playerAttribute.VolleyballAttributeId,
                PlayerGroupId = playerAttribute.PlayerGroupId,
                VolleyballAttributeLevel = playerAttribute.VolleyballAttributeLevel
            };
        }

        private IList<PlayerUpdateView> LoadPlayerUpdateViewsFromDataTable(DataTable dt)
        {
            List<PlayerUpdateView> playersToUpdate = new List<PlayerUpdateView>();
            IList<string> playerAttributeColumnNames = new List<string>();

            var columnValidationErrors = ColumnsValid(dt, ref playerAttributeColumnNames);
            if (columnValidationErrors.Count == 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    PlayerUpdateView puv = new PlayerUpdateView();
                    puv.Id = GetValue<int>(dr[GetPropertyName<int>(() => puv.Id)], x => Convert.ToInt32(x), -1);
                    puv.FirstName = GetValue<string>(dr[GetPropertyName<string>(() => puv.FirstName)], x => x.ToString(), null);
                    puv.MiddleName = GetValue<string>(dr[GetPropertyName<string>(() => puv.MiddleName)], x => x.ToString(), null);
                    puv.LastName = GetValue<string>(dr[GetPropertyName<string>(() => puv.LastName)], x => x.ToString(), null);
                    puv.Gender = GetValue<string>(dr[GetPropertyName<string>(() => puv.Gender)], x => x.ToString(), null);
                    puv.Email = GetValue<string>(dr[GetPropertyName<string>(() => puv.Email)], x => x.ToString(), null);
                    puv.Active = GetValue<bool>(dr[GetPropertyName<bool>(() => puv.Active)], x => Convert.ToBoolean(x), false);
                    puv.Comments = GetValue<string>(dr[GetPropertyName<string>(() => puv.Comments)], x => x.ToString(), null);

                    foreach (string paColumnName in playerAttributeColumnNames)
                    {
                        PlayerAttributeView pav = new PlayerAttributeView()
                        {
                            PlayerId = puv.Id,
                            VolleyballAttributeName = paColumnName,
                            VolleyballAttributeLevel = GetValue<double>(dr[paColumnName], x => Convert.ToDouble(x), 0)
                        };
                        puv.PlayerAttributes.Add(pav);
                    }
                    playersToUpdate.Add(puv);
                }
            }
            else
            {
                throw new Exception(String.Format("The following validation errors were encountered: {0}", String.Join("\r\n", columnValidationErrors)));
            }

            return playersToUpdate;
        }

        private IList<string> ColumnsValid(DataTable dt, ref IList<string> playerAttributeColumnNames)
        {
            var errors = new List<string>();
            foreach (string requiredProperty in GetRequiredColumnNames(ref playerAttributeColumnNames))
            {
                bool propertyFound = false;
                foreach (DataColumn dc in dt.Columns)
                {
                    if (requiredProperty.ToLower().Equals(dc.ColumnName.ToLower()))
                    {
                        propertyFound = true;
                        break;
                    }
                }

                if (!propertyFound)
                {
                    errors.Add(String.Format("Column matching property '{0}' not found in import file", requiredProperty));
                }
            }

            return errors;
        }

        private IList<string> GetRequiredColumnNames(ref IList<string> playerAttributeColumnNames)
        {
            var requiredColumns = new List<string>();
            var puv = new PlayerUpdateView();

            requiredColumns.Add(GetPropertyName<int>(() => puv.Id));
            requiredColumns.Add(GetPropertyName<string>(() => puv.FirstName));
            requiredColumns.Add(GetPropertyName<string>(() => puv.MiddleName));
            requiredColumns.Add(GetPropertyName<string>(() => puv.LastName));
            requiredColumns.Add(GetPropertyName<string>(() => puv.Gender));
            requiredColumns.Add(GetPropertyName<string>(() => puv.Email));
            requiredColumns.Add(GetPropertyName<bool>(() => puv.Active));
            requiredColumns.Add(GetPropertyName<string>(() => puv.Comments));

            foreach (VolleyballAttribute va in volleyballAttributeService.GetAllVolleyballAttributes())
            {
                requiredColumns.Add(va.Name);
                playerAttributeColumnNames.Add(va.Name);
            }
            return requiredColumns;
        }

        private DataTable GetDataTable(int selectedPlayerGroupId, IList<string> playerAttributeColumnNames, IList<string> propertyNames)
        {
            var dt = new DataTable();
            foreach (string propertyName in propertyNames)
            {
                dt.Columns.Add(new DataColumn() { ColumnName = propertyName });
            }

            foreach (PlayerUpdateView puv in Mapper.Map<IList<Player>, IList<PlayerUpdateView>>(playerService.GetAllPlayersInGroup(selectedPlayerGroupId)))
            {
                DataRow dr = dt.NewRow();
                foreach (string propertyName in propertyNames)
                {
                    bool propertyNameIsVolleyballAttribute = playerAttributeColumnNames.Any(s => s.Equals(propertyName));
                    if (propertyNameIsVolleyballAttribute)
                    {
                        foreach (var pav in puv.PlayerAttributes)
                        {
                            //currently this isn't hydrating from the db for some reason
                            var va = volleyballAttributeService.GetVolleyballAttribute(pav.VolleyballAttributeId);
                            if (propertyName.Equals(va.Name))
                            {
                                dr[propertyName] = pav.VolleyballAttributeLevel;
                                break;
                            }
                        }
                    }
                    else
                    {
                        dr[propertyName] = puv.GetType().GetProperty(propertyName).GetValue(puv, null);
                    }
                }

                dt.Rows.Add(dr);
            }

            if(dt.Rows.Count == 0)
            {
                SeedTableWithEmptyRow(dt);
            }

            return dt;
        }

        private static void SeedTableWithEmptyRow(DataTable dt)
        {
            dt.Rows.Add(dt.NewRow());
        }

        //reflection stuff
        private string GetPropertyName<T>(Expression<Func<T>> expr)
        {
            return (expr.Body as MemberExpression).Member.Name;
        }

        private T GetValue<T>(object obj, Func<object, T> conversion, T defaultValue)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return defaultValue;
            }
            else if (obj.ToString().Trim().Length == 0)
            {
                return defaultValue;
            }
            else
            {
                int intValue;
                if (typeof(T) == typeof(bool) && Int32.TryParse(obj.ToString(), out intValue))
                {
                    return conversion(intValue);
                }
                return conversion(obj);
            }
        }

        private IEnumerable<Team> GetTeamsFromTeamViews(IEnumerable<TeamView> teamViews)
        {
            var teams = new List<Team>();
            foreach(var teamView in teamViews)
            {
                var team = new Team() { Name = teamView.Name };
                foreach(var playerView in teamView.Players)
                {
                    team.Players.Add(new Player()
                    {
                        Id = playerView.Id
                    });
                }
                teams.Add(team);
            }
            return teams;
        }

        public class SavePlayerListsTeamListParameters
        {
            public int SavedPlayerListId { get; set; }
            public string ListName { get; set; }
            public IEnumerable<TeamView> Teams { get; set; }

            public SavePlayerListsTeamListParameters()
            {
                Teams = new List<TeamView>();
            }
        }
    }
}