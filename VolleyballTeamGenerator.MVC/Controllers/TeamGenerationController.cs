﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using VolleyballTeamGenerator.Domain.Messages;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Domain.Services.Interfaces;
using VolleyballTeamGenerator.Application.ViewModels;
using VolleyballTeamGenerator.Application.Messaging;
using Newtonsoft.Json;

namespace VolleyballTeamGenerator.MVC.Controllers
{
    public class TeamGenerationController : AuthorizedController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PrintSavedPlayerList(int id)
        {
            ViewBag.Id = id;

            return View();
        }

        private ITeamGenerationDomainService teamGenerationService;
        public TeamGenerationController(ITeamGenerationDomainService teamGenerationService)
        {
            this.teamGenerationService = teamGenerationService;
        }

        public TeamGenerationController() : this(MvcApplication.IoCContainer.ResolveDependency<ITeamGenerationDomainService>()) { }

        [HttpGet]
        public JsonResult Get()
        {
            return Json(Mapper.Map<IEnumerable<CompositionStrategy>, IEnumerable<CompositionStrategyView>>(teamGenerationService.GetAllCompositionStrategies()), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GenerateTeams(int groupId, int numberOfTeams, IEnumerable<int> playerIds, int strategyId)
        {
            return Json(Mapper.Map<IEnumerable<Team>, IEnumerable<TeamView>>(teamGenerationService.GenerateTeams(new GenerateTeamsDomainRequest()
            {
                PlayerGroupId = groupId,
                NumberOfTeams = numberOfTeams,
                PlayerIDsToUse = playerIds,
                TeamCompositionStrategy = (CompositionStrategyEnum)strategyId
            }).Teams), JsonRequestBehavior.AllowGet);
        }
    }
}