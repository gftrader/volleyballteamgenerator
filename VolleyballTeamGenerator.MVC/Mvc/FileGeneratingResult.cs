﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using VolleyballTeamGenerator.Infrastructure;

//http://stackoverflow.com/questions/943122/writing-to-output-stream-from-action
namespace VolleyballTeamGenerator.MVC.Mvc
{
    /// <summary>
    /// MVC action result that generates the file content using a delegate that writes the content directly to the output stream.
    /// </summary>
    public class FileGeneratingResult : FileResult
    {
        /// <summary>
        /// The delegate that will generate the file content.
        /// </summary>
        private IStream content;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileGeneratingResult" /> class.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="contentType">Type of the content.</param>
        public FileGeneratingResult(string fileName, string contentType, IStream content)
            : base(contentType)
        {
            if (content == null)
                throw new ArgumentNullException("content");

            this.content = content;
            this.FileDownloadName = fileName;
        }

        ~FileGeneratingResult()
        {
            content.Dispose();
        }

        /// <summary>
        /// Writes the file to the response.
        /// </summary>
        /// <param name="response">The response object.</param>
        protected override void WriteFile(System.Web.HttpResponseBase response)
        {
            content.Position = 0;
            content.CopyTo(response.OutputStream);
        }
    }
}