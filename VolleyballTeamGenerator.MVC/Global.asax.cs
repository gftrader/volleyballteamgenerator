﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.MVC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static IIoCContainer IoCContainer { get; private set; }
        static MvcApplication()
        {
          IoCContainer = StructureMapConfig.ConfigureStuctureMap();
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            VolleyballTeamGenerator.Application.Bootstrapper.ConfigureAutoMapper();
        }
    }
}
