﻿using System.Web;
using System.Web.Optimization;

namespace VolleyballTeamGenerator.MVC
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //BundleTable.EnableOptimizations = true;
            RegisterSharedScripts(bundles);
            RegisterPageSpecificScripts(bundles);
            RegisterSharedStyles(bundles);
            RegisterPageSpecificStyles(bundles);
        }

        private static void RegisterSharedScripts(BundleCollection bundles)
        {
            bundles.UseCdn = true;

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/angular", "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.13/angular.min.js").Include("~/Scripts/angular.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/angular-animate", "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.13/angular-animate.min.js").Include("~/Scripts/angular-animate.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/required").Include(
                      "~/Scripts/jquery-{version}.js",
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      //"~/Scripts/angular-animate.js",
                      "~/Scripts/angularHelper.js",
                      "~/Scripts/underscore.js",
                      "~/Scripts/sprintf.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                    "~/Scripts/jquery.validate.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular/app").Include(
                      "~/Scripts/app/app.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular/app/common").Include(
                      "~/Scripts/app/directives/focusMe.js"));
        }

        private static void RegisterPageSpecificScripts(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/angular/app/generateTeams").Include(
                      "~/Scripts/app/controllers/generateTeamsController.js",
                      "~/Scripts/app/services/playerService.js",
                      "~/Scripts/app/services/teamGenerationService.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular/app/printSavedPlayerList").Include(
                      "~/Scripts/app/controllers/printSavedPlayerListController.js",
                      "~/Scripts/app/services/playerService.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular/app/managePlayer").Include(
                      "~/Scripts/app/controllers/managePlayerController.js",
                      "~/Scripts/app/services/playerService.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular/app/findPlayers").Include(
                      "~/Scripts/app/controllers/findPlayersController.js",
                      "~/Scripts/app/services/playerService.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular/app/bulkImportExport").Include(
                      "~/Scripts/app/controllers/bulkImportExportController.js",
                      "~/Scripts/app/services/playerService.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular/app/addPlayer").Include(
                      "~/Scripts/app/controllers/addPlayerController.js",
                      "~/Scripts/app/services/playerService.js"));
        }

        private static void RegisterSharedStyles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap.flatly.css",
                      "~/Content/Site.css"));
        }

        private static void RegisterPageSpecificStyles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/pages/generateTeams").Include(
                      "~/Content/pages/TeamGeneration.css"));

            bundles.Add(new StyleBundle("~/Content/pages/printSavedPlayerList").Include(
                      "~/Content/pages/PrintSignInSheet.css"));
        }
    }
}