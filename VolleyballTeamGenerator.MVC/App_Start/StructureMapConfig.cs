﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StructureMap;
using StructureMap.Configuration.DSL;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Infrastructure.OpenXml;
using VolleyballTeamGenerator.Repository.EF;
using VolleyballTeamGenerator.Repository.EF.Repositories;
using VolleyballTeamGenerator.Domain;
using VolleyballTeamGenerator.Domain.RepositoryInterfaces;
using VolleyballTeamGenerator.Domain.Services.Implementations;
using VolleyballTeamGenerator.Domain.Services.Interfaces;

namespace VolleyballTeamGenerator.MVC
{
    public class StructureMapConfig
    {
        public static IIoCContainer ConfigureStuctureMap()
        {
            return new StructureMapIocContainer(new Container(x => x.AddRegistry<VolleyballTeamGeneratorRegistry>()));
        }

        private class VolleyballTeamGeneratorRegistry : Registry
        {
            public VolleyballTeamGeneratorRegistry()
            {
                ConfigureEntityFramework();
                ConfigureRepositories();
                ConfigureServices();
                ConfigureImportExport();
            }

            private void ConfigureEntityFramework()
            {
                For<IUnitOfWork>().Use<EFUnitOfWork>();
                For<IDbContext>().Use<VolleyballTeamGeneratorDataContext>()
                    .Ctor<string>("connectionString").Is(ProjectConfiguration.ConnectionStrings.VolleyballTeamGeneratorEntities);
            }

            private void ConfigureRepositories()
            {
                For<IPlayerRepository>().Use<PlayerRepository>();
                For<IPlayerGroupRepository>().Use<PlayerGroupRepository>();
                For<ISavedPlayersListRepository>().Use<SavedPlayersListRepository>();
                For<ISavedPlayersListsTeamsListRepository>().Use<SavedPlayersListsTeamsListRepository>();
                For<ISavedPlayersListsTeamsLists_TeamRepository>().Use<SavedPlayersListsTeamsLists_TeamRepository>();
                For<IVolleyballAttributeRepository>().Use<VolleyballAttributeRepository>();
            }

            private void ConfigureServices()
            {
                For<IPlayerGroupService>().Use<PlayerGroupService>();
                For<IPlayerService>().Use<PlayerService>();
                For<ISavedPlayerListService>().Use<SavedPlayerListService>();
                For<ITeamGenerationDomainService>().Use<TeamGenerationDomainService>();
                For<IVolleyballAttributeService>().Use<VolleyballAttributeService>();
            }

            private void ConfigureImportExport()
            {
                For<IDocumentImporter>().Use<OpenXmlExcelDocumentImporter>();
                For<IExcelExporter>().Use<OpenXmlExcelDocumentExporter>();
            }
        }
    }
}