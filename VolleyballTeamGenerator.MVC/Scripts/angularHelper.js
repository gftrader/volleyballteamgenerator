﻿function angularAsyncGet(httpService, promiseService, url, params) {
    return callAsync(httpService, promiseService, "GET", url, null, params);
}

function angularAsyncPost(httpService, promiseService, url, data) {
    return callAsync(httpService, promiseService, "POST", url, data, null)
}

function angularAsyncPut(httpService, promiseService, url, params, data) {
    return callAsync(httpService, promiseService, "PUT", url, data, params)
}

function angularAsyncDelete(httpService, promiseService, url, params) {
    return callAsync(httpService, promiseService, "DELETE", url, null, params);
}

function callAsync(httpService, promiseService, method, url, data, params) {
    var deferred = promiseService.defer();

    httpService({
        method: method,
        url: url,
        data: data,
        params: params
    })
    .then(function (response) { deferred.resolve(response.data); }, function (response) { deferred.reject(response.data.ExceptionMessage); });

    return deferred.promise;
}