﻿vbApp.controller("managePlayerController",  ['$scope', 'playerService', function (scope, playerService) {
    scope.editingPlayer = false;

    playerService.getPlayer(playerId).then(function (player) {
        scope.viewPlayer = player;
        scope.editPlayer = clonePlayer(player);
        playerService.getAllPlayerGroups().then(function (playerGroups) {
            for (var i = 0; i < playerGroups.length; i++) {
                var playerGroup = playerGroups[i];
                playerGroup.Selected = (_.any(player.PlayerGroups, function (pg) { return pg.Id == playerGroup.Id }));
            }
            scope.allPlayerGroups = playerGroups;

        }, function (error) {
            alert("error retrieving player groups");
        });
    }, function (error) {
        alert("error retrieving player");
    });

    scope.switchPlayerToEditMode = function () {
        scope.editingPlayer = true;
    };

    scope.updatePlayerAndCancelEditMode = function () {
        playerService.updatePlayer(scope.editPlayer).then(function () {
            copyPlayerProperties(scope.editPlayer, scope.viewPlayer);
            scope.editingPlayer = false;
        }, function (error) {
            alert("error updating player");
        });
    }

    scope.cancelPlayerEditMode = function () {
        scope.editPlayer = clonePlayer(scope.viewPlayer);
        scope.editingPlayer = false;
    }

    scope.attributeIsSelected = function (playerAttribute) {
        return (scope.selectedAttribute != null
             && scope.selectedAttribute.VolleyballAttributeId == playerAttribute.VolleyballAttributeId
             && scope.selectedAttribute.PlayerGroupId == playerAttribute.PlayerGroupId);
    }

    scope.switchPlayerAttributeToEditMode = function (playerAttribute) {
        scope.selectedAttribute = playerAttribute;
        scope.editPlayerAttribute = clonePlayerAttribute(playerAttribute);
        scope.focusInput = true;
    };

    scope.updatePlayerAttributeAndCancelEditMode = function () {
        playerService.updatePlayerAttribute(scope.viewPlayer.Id, scope.editPlayerAttribute).then(function () {
            scope.selectedAttribute.VolleyballAttributeLevel = scope.editPlayerAttribute.VolleyballAttributeLevel;
            scope.selectedAttribute = null;
            scope.editPlayerAttribute = null;
        }, function (error) {
            alert("error updating player attribute");
        });
    }

    scope.cancelPlayerAttributeEditMode = function () {
        scope.editPlayerAttribute = null;
        scope.selectedAttribute = null;
    }

    scope.groupSelected = function (group) {
        if (confirm(sprintf("Are you sure you want to %s %s %s %s", group.Selected ? "add" : "remove", scope.viewPlayer.PlayerName, group.Selected ? "to" : "from", group.Name)))
        {
            playerService.updatePlayerGroup(scope.viewPlayer.Id, group.Id).then(function () {
                playerService.getPlayerAttributes(playerId).then(function (attributes) {
                    scope.viewPlayer.PlayerAttributes = attributes;
                }, function (error) {
                    alert("error refreshing player attributes");
                });
                
            }, function (error) {
                alert("error updating player group");
            });
        }
        else
        {
            group.Selected = !group.Selected;
        }
    }

    function clonePlayer(player){
        var newPlayer = {};
        copyPlayerProperties(player, newPlayer);
        return newPlayer;
    }

    function copyPlayerAttributeProperties(playerAttributeToCopyFrom, playerAttributeToCopyTo) {
        playerAttributeToCopyTo.PlayerId = playerAttributeToCopyFrom.PlayerId
        playerAttributeToCopyTo.VolleyballAttributeId = playerAttributeToCopyFrom.VolleyballAttributeId;
        playerAttributeToCopyTo.PlayerGroupId = playerAttributeToCopyFrom.PlayerGroupId,
        playerAttributeToCopyTo.VolleyballAttributeLevel = playerAttributeToCopyFrom.VolleyballAttributeLevel
    }

    function clonePlayerAttribute(playerAttribute) {
        var newAttribute = {};
        copyPlayerAttributeProperties(playerAttribute, newAttribute);
        return newAttribute;
    }

    function copyPlayerProperties(playerToCopyFrom, playerToCopyTo) {
        playerToCopyTo.Id = playerToCopyFrom.Id;
        playerToCopyTo.FirstName = playerToCopyFrom.FirstName;
        playerToCopyTo.MiddleName = playerToCopyFrom.MiddleName;
        playerToCopyTo.LastName = playerToCopyFrom.LastName;
        playerToCopyTo.Email = playerToCopyFrom.Email;
        playerToCopyTo.Gender = playerToCopyFrom.Gender;
        playerToCopyTo.Active = playerToCopyFrom.Active;
        playerToCopyTo.Comments = playerToCopyFrom.Comments;
    }
}]);