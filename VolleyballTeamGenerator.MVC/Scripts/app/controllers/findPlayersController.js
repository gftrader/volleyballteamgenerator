﻿vbApp.controller("findPlayersController", ['$scope', 'playerService', function (scope, playerService) {
    playerService.getAllPlayerGroups().then(function (allPlayerGroups) {
        scope.allPlayerGroups = allPlayerGroups;
    }, function (error) {
        alert("error");
    });

    scope.findPlayers = function () {
        playerService.findPlayers(scope.filterFirstName, scope.filterLastName, scope.filterGender, scope.filterEmail, _.map(scope.filterGroups, function(x){ return x.Id })).then(function (matchingPlayers) {
            scope.matchingPlayers = matchingPlayers;
        }, function (error) {
            alert("error");
        });
    }
}]);