﻿vbApp.controller("printSavedPlayerListController", ['$scope', 'playerService', 'dateFilter', function (scope, playerService, dateFilter) {
    playerService.getAllPlayersInList(listId).then(function (players) {
        while(players.length < 26) {
            players.push({
                PlayerName: ""
            });
        }
        scope.playersInList = players;
    }, function (error) {
        alert("There was a problem attempting to retrieve the player list");
    });

    scope.printPage = function () {
        print();
    }

    scope.setPrintDateDay = function(){
        scope.printDateDay = (!isNaN(Date.parse(scope.printDate))) ? Date.parse(scope.printDate) : "";
    }

    scope.printDate = dateFilter(new Date(), "MM/dd/yyyy");
    scope.setPrintDateDay();
}]);