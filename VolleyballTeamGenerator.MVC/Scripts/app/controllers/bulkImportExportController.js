﻿vbApp.controller("bulkImportExportController", ['$scope', 'playerService', function (scope, playerService) {
    playerService.getAllPlayerGroups().then(function (groups) {
        scope.allGroups = groups;
    }, function (error) {
        alert("There was a problem retrieving player groups");
    });

    scope.importPlayers = function () {
        alert("Bulk importing of players is currently unavailable.");
    }
}]);