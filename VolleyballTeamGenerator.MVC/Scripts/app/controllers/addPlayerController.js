﻿vbApp.controller("addPlayerController", ['$scope', '$window', 'playerService', function (scope, window, playerService) {
    scope.addedPlayer = { Active: true };

    scope.addPlayer = function () {
        playerService.addPlayer(scope.addedPlayer).then(function (player) {
            window.location.href = rootPath + "Players/ManagePlayer/" + player.Id;
        }, function (error) {
            alert("There was a problem attempting to add the player");
        });
    }
}]);