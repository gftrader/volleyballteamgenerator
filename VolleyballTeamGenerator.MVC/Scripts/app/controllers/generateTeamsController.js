﻿vbApp.controller("generateTeamsController", ['$scope', '$timeout', 'playerService', 'teamGenerationService', function (scope, timeout, playerService, teamGenerationService) {
    scope.playerList = [];
    scope.selectedNumberOfTeams = 4;
    scope.editing = false;
    scope.printMode = false;
    scope.currentSavedListName = "";
    scope.listChanged = false;

    window.onbeforeunload = function () {
        if (scope.listChanged) {
            return "You have made changes to the players list. Are you sure you want to exit?";
        }
        
        return null;
    }

    playerService.getAllPlayerGroups().then(function (groups) {
        scope.allGroups = groups;
    }, function (error) {
        alert("There was a problem retrieving player groups");
    });

    teamGenerationService.getAllGenerationStrategies().then(function (generationStrategies) {
        scope.allGenerationStrategies = generationStrategies;
    }, function (error) {
        alert("There was a problem retrieving the team generation strategies");
    });

    scope.selectedGroupChanged = function () {
        playerService.getAllPlayersInGroup(scope.selectedGroup.Id).then(function (players) {
            scope.playerList = [];
            scope.selectedGroupPlayers = players;
            scope.listChanged = false;
        }, function (error) {
            alert("There was a problem retrieving players for group " + scope.selectedGroup.Name);
        });

        refreshPlayerListsInGroup();
    }

    scope.selectedSavedPlayerListChanged = function () {
        playerService.getAllPlayersInList(scope.selectedSavedPlayerList.Id).then(function (players) {
            scope.playerList = players;
            scope.currentSavedListName = scope.selectedSavedPlayerList.Name;
            removeAddedPlayersFromAllPlayersInGroup();
            loadTeamsLists();
            scope.generatedTeams = null;
            scope.listChanged = false;
        }, function (error) {
            alert("There was a problem attempting to retrieve the players in the selected list");
        });
    }

    scope.selectedSavedPlayersListTeamListChanged = function () {
        playerService.getSavedTeamsListTeamList(scope.selectedSavedPlayersListTeamList.Id).then(function (teamList) {
            scope.generatedTeams = teamList.Teams;
            setSavedTeamsLoadedFeedback();
            scope.listChanged = false;
        }, function (error) {
            alert("There was a problem attempting to load the saved team list");
        });
    }

    scope.deleteSelectedSavedPlayerList = function () {
        if (confirm("Are you sure you want to remove this list?")) {
            playerService.removePlayerList(scope.selectedSavedPlayerList.Id).then(function () {
                scope.selectedSavedPlayerList = null;
                refreshPlayerListsInGroup();
                scope.listChanged = true;
            }, function (error) {
                alert("There was a problem attempting to delete this player list");
            });
        }
    }

    scope.addPlayerToList = function () {
        var success = false;
        if (_.find(scope.playerList, function (x) { return x.PlayerName == scope.addedPlayerName }) === undefined) {
            var player = _.find(scope.selectedGroupPlayers, function (x) { return x.PlayerName == scope.addedPlayerName });
            if(player !== undefined) {
                scope.playerList[scope.playerList.length] = player;
                scope.lastPlayerAddedName = player.PlayerName;
                setLastPlayerNameFeedback(player.PlayerName);                
                removeAddedPlayersFromAllPlayersInGroup();
                success = true;
            }
        }
        else {
            success = true;
        }

        if (success) {
            scope.addedPlayerName = "";
            scope.listChanged = true;
        }
    }

    scope.savePlayerList = function () {
        var listName = prompt("Enter a list name", scope.currentSavedListName);
        if(listName !== null && listName.length > 0){
            playerService.savePlayerList(scope.selectedGroup.Id, listName, _.pluck(scope.playerList, "Id")).then(function () {
                scope.currentSavedListName = listName;
                setSavedListNameFeedback();
                refreshPlayerListsInGroup(listName);
                scope.listChanged = false;
            }, function (error) {
                alert("There was an error attempting to save the player list");
            });
        }
    }

    scope.clearPlayerList = function () {
        if (confirm("Are you sure you want to remove all players in the players table?")) {
            while (scope.playerList.length > 0) {
                scope.removePlayerFromList(0); //do it this way i/o scope.playerList = [] so we can add the players back to our group list
            }
            scope.listChanged = false;
        }
    }

    scope.removePlayerFromList = function (listIndex) {
        var removedPlayer = scope.playerList.splice(listIndex, 1)[0]; //should only be one removed
        addPlayerBackToPlayersInGroupList(removedPlayer);
        scope.listChanged = true;
    }

    scope.generateTeams = function () {
        teamGenerationService.generateTeams(scope.selectedGroup.Id,
            scope.selectedNumberOfTeams,
            _.pluck(scope.playerList, "Id"),
            scope.selectedGenerationStrategy.Id).then(function (teams) {
                scope.generatedTeams = teams;
        }, function (error) {
            alert("There was an error attempting to generate teams");
        });
    }

    scope.toggleEditTeams = function () {
        scope.editing = !scope.editing;
        if (!scope.editing) {
            scope.selectedTeam = null;
            scope.selectedPlayer = null;
        }
    }

    scope.swapPlayer = function (team, player) {
        if (scope.editing) {
            if(scope.selectedPlayer == null && player != null){
                scope.selectedPlayer = player;
                scope.selectedTeam = team;
            }
            else if(scope.selectedPlayer != null) {
                var playerToSwap = scope.selectedPlayer;
                if (player != null) {
                    scope.selectedTeam.Players[_.indexOf(scope.selectedTeam.Players, scope.selectedPlayer)] = player;
                    team.Players[_.indexOf(team.Players, player)] = playerToSwap;
                }
                else {
                    scope.selectedTeam.Players = _.filter(scope.selectedTeam.Players, function(p){ return p.Id !== playerToSwap.Id });
                    team.Players[team.Players.length] = playerToSwap;
                }
                scope.selectedTeam = null;
                scope.selectedPlayer = null;
            }
        }
    }

    scope.deleteSavedTeamsList = function () {
        if (confirm("Are you sure you want to remove this list?")) {
            playerService.removeSavedTeamsList(scope.selectedSavedPlayersListTeamList.Id).then(function () {
                loadTeamsLists();
            }, function (error) {
                alert("There was an error attmepting to delete this team arrangement");
            });
        }
    }

    scope.saveTeams = function () {
        playerService.savePlayerList(scope.selectedGroup.Id, scope.selectedSavedPlayerList.Name, _.pluck(scope.playerList, "Id")).then(function () {
            var listName = prompt("Enter a name for this team arrangement", scope.selectedSavedPlayersListTeamList != null ? scope.selectedSavedPlayersListTeamList.Name : "");
            if (listName != null && listName.length > 0) {
                playerService.savePlayerListTeamList(scope.selectedSavedPlayerList.Id, listName, scope.generatedTeams).then(function () {
                    loadTeamsLists(function () {
                        scope.selectedSavedPlayersListTeamList = _.find(scope.savedTeamsList, function (l) { return l.Name === listName });
                    });
                    setTeamsSavedFeedback();
                }, function (error) {
                    alert("There was a problem attempting to save this team arrangement");
                });
            }
        }, function (error) {
            alert("There was an error attempting to save the players list before saving the teams. No updates were made.");
        });
    }

    scope.printTeams = function () {
        scope.printMode = !scope.printMode;
    }

    scope.printPage = function () {
        print();
    }

    function refreshPlayerListsInGroup(listName) {
        playerService.getSavedPlayerListsInGroup(scope.selectedGroup.Id).then(function (lists) {
            scope.selectedGroupPlayerLists = lists;
            if (listName) {
                scope.selectedSavedPlayerList = _.find(scope.selectedGroupPlayerLists, function (list) { return list.Name == listName });
            }
        }, function (error) {
            alert("There was a problem attempting to retrieve the lists available to this group");
        });
    }
    
    function removeAddedPlayersFromAllPlayersInGroup() {
        scope.selectedGroupPlayers = _.filter(scope.selectedGroupPlayers, function (groupPlayer) {
            return !_.any(scope.playerList, function (listPlayer) {
                return listPlayer.Id == groupPlayer.Id;
            });
        });
    }

    function addPlayerBackToPlayersInGroupList(removedPlayer) {
        scope.selectedGroupPlayers[scope.selectedGroupPlayers.length] = removedPlayer;
        scope.selectedGroupPlayers = _.sortBy(scope.selectedGroupPlayers, function (player) { return player.PlayerName; });
    }

    function loadTeamsLists(postLoadCallback) {
        playerService.getSavedTeamsInSelectedSavedPlayersList(scope.selectedSavedPlayerList.Id).then(function (teamsList) {
            scope.savedTeamsList = teamsList;
            if (postLoadCallback) {
                postLoadCallback();
            }
        }, function (error) {
            alert("There was a problem attempting to retrieve the saved team groupings for this list");
        });
    }

    function setLastPlayerNameFeedback() {
        fadeText(function () {
            scope.playerAdded = true;
        }, function () {
            scope.playerAdded = false;
        });
    }

    function setSavedListNameFeedback() {
        fadeText(function () {
            scope.playerListSaved = true;
        }, function () {
            scope.playerListSaved = false;
        });
    }

    function setSavedTeamsLoadedFeedback() {
        fadeText(function () {
            scope.savedTeamsLoaded = true;
        }, function () {
            scope.savedTeamsLoaded = false;
        });
    }

    function setTeamsSavedFeedback() {
        fadeText(function () {
            scope.teamsSaved = true;
        }, function () {
            scope.teamsSaved = false;
        });
    }

    function fadeText(showAction, hideAction) {
        showAction();
        timeout(function () {
            hideAction();
        }, 3000);
    }
}]);