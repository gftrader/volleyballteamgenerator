﻿//http://stackoverflow.com/questions/14833326/how-to-set-focus-on-input-field-in-angularjs
vbApp.directive("focusMe", function () {
    return {
        restrict: "A",
        scope: { trigger: "=focusMe" },
        link: function (scope, element) {
            scope.$watch("trigger", function (value) {
                if (value === true) {
                    element[0].focus();
                    element[0].select();
                    scope.trigger = false;
                }
            });
        }
    };
});