﻿vbApp.service("teamGenerationService", ['$http', '$q', function (httpService, promiseService) {
    return {
        getAllGenerationStrategies: function () {
            return angularAsyncGet(httpService, promiseService, rootPath + "TeamGeneration/Get");
        },
        generateTeams: function (groupId, numberOfTeams, playerIds, strategyId) {
            return angularAsyncGet(httpService, promiseService, rootPath + "TeamGeneration/GenerateTeams", {
                groupId: groupId,
                numberOfTeams: numberOfTeams,
                playerIds: playerIds,
                strategyId: strategyId
            });
        }
    }
}]);