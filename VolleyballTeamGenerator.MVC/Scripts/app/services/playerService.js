﻿vbApp.service("playerService", ['$http','$q', function (httpService, promiseService) {
    return {
        getAllPlayerGroups: function(){
            return angularAsyncGet(httpService, promiseService, rootPath + "Players/GetAllPlayerGroups");
        },
        getPlayer: function(id){
            return angularAsyncGet(httpService, promiseService, rootPath + "Players/Get", { playerId: id });
        },
        getPlayerAttributes: function(id){
            return angularAsyncGet(httpService, promiseService, rootPath + "Players/GetPlayerAttributes", { playerId: id });
        },
        getAllPlayersInGroup: function(id){
            return angularAsyncGet(httpService, promiseService, rootPath + "Players/GetAllPlayersInGroup", { groupId: id });
        },
        getSavedPlayerListsInGroup: function(id){
            return angularAsyncGet(httpService, promiseService, rootPath + "Players/GetSavedPlayerListsInGroup", { groupId: id });
        },
        getAllPlayersInList: function(id){
            return angularAsyncGet(httpService, promiseService, rootPath + "Players/GetAllPlayersInList", { listId: id });
        },
        getSavedTeamsInSelectedSavedPlayersList: function (savedPlayersListId) {
            return angularAsyncGet(httpService, promiseService, rootPath + "Players/GetSavedPlayersListsTeamList", { savedPlayersListId: savedPlayersListId });
        },
        getSavedTeamsListTeamList: function(id){
            return angularAsyncGet(httpService, promiseService, rootPath + "Players/GetSavedPlayersListsTeamListTeam", { id: id });
        },
        findPlayers: function (firstName, lastName, gender, email, ids) {
            return angularAsyncGet(httpService, promiseService, rootPath + "Players/FindMatchingPlayers", {
                findPlayersRequest: {
                        FirstName: firstName,
                        LastName: lastName,
                        Gender: gender,
                        Email: email,
                        PlayerGroupIds: ids
                    }
                }
            );
        },
        addPlayer: function(player){
            return angularAsyncPost(httpService, promiseService, rootPath + "Players/Post", player);
        },
        exportPlayers: function(playerGroupId){
            return angularAsyncGet(httpService, promiseService, rootPath + "Players/ExportPlayers", { playerGroupId: playerGroupId });
        },
        removePlayerList: function(savedPlayersListId){
            return angularAsyncDelete(httpService, promiseService, rootPath + "Players/RemoveSavedPlayersList", { savedPlayersListId: savedPlayersListId });
        },
        removeSavedTeamsList: function (teamsListId) {
            return angularAsyncDelete(httpService, promiseService, rootPath + "Players/RemoveSavedPlayersListsTeamsList", { id: teamsListId });
        },
        savePlayerList: function(playerGroupId, listName, playerIds){
            return angularAsyncPut(httpService, promiseService, rootPath + "Players/SavePlayerList", {
                playerGroupId: playerGroupId,
                listName: listName,
                playerIds: playerIds
            });
        },
        savePlayerListTeamList: function (savedPlayerListId, listName, teams) {
            return angularAsyncPut(httpService, promiseService, rootPath + "Players/SavePlayerListsTeamList", null, {
                SavedPlayerListId: savedPlayerListId,
                ListName: listName,
                Teams: teams
            });
        },
        updatePlayerGroup: function(playerId, groupId){
            return angularAsyncPut(httpService, promiseService, rootPath + "Players/UpdatePlayerGroup", { playerId: playerId, groupId: groupId });
        },
        updatePlayer: function(player){
            return angularAsyncPut(httpService, promiseService, rootPath + "Players/Put", { id: player.Id }, player);
        },
        updatePlayerAttribute: function (playerId, playerAttribute) {
            return angularAsyncPut(httpService, promiseService, rootPath + "Players/UpdatePlayerAttribute", null, { playerId: playerId, playerAttribute: playerAttribute });
        }
    }
}]);