﻿<%@ Page Title="Volleyball Team Generator" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="VolleyballTeamGenerator.WebForms._Default" %>
<%@ Import Namespace="VolleyballTeamGenerator.Application.ViewModels" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <link type="text/css" rel="stylesheet" href="Content/css/Default.css" />
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div id="topSection" class="row">
        <div class="col-xs-12">
            <h1>Generate Teams</h1>
            <p>Choose a pool of players to select from: <asp:DropDownList ID="drpPlayerGroups" runat="server" ClientIDMode="Static" OnSelectedIndexChanged="drpPlayerGroups_SelectedIndexChanged" AutoPostBack="true" /></p>
        </div>
    </div>
    <div id="divAdditionalFilters" class="hide row">
        <div id="divConfigureOptions" class="panel panel-default col-xs-12 col-sm-6 col-md-5">
            <div class="panel-body">
                <h2>Load Saved List</h2>
                <asp:UpdatePanel ID="upSavedPlayersLists" runat="server" RenderMode="Inline">
                    <ContentTemplate>
                        <p><asp:DropDownList ID="drpSavedPlayersLists" runat="server" OnSelectedIndexChanged="drpSavedPlayersLists_SelectedIndexChanged" AutoPostBack="true" /> <span id="spanPlayerListOptions" runat="server"><asp:LinkButton id="btnDeleteSavedPlayersList" runat="server" Text="Delete" OnClick="btnDeleteSavedPlayersList_Click" OnClientClick="return confirm('Are you sure you want to remove this list?');" /> | <a id="aPrintSignInSheet" runat="server" href="#" target="_blank">Print Player List</a></span></p>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnClearPlayers" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnDeleteSavedPlayersList" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnSavePlayersList" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <h2>Players List</h2>
                <p><asp:DropDownList id="drpPlayers" runat="server" CssClass="hide" /><asp:TextBox ID="txtAddPlayerName" runat="server" class="longInput" ClientIDMode="Static" /> <asp:Button ID="btnAddPlayer" CssClass="btn btn-default" runat="server" text="Add Player" OnClick="btnAddPlayer_Click" OnClientClick="return validateName();" /></p>
                <asp:UpdatePanel ID="upSavePlayersList" runat="server" RenderMode="Inline">
                    <ContentTemplate>
                        <asp:TextBox ID="txtSavePlayersListName" runat="server" CssClass="hide" /> <p><asp:Button ID="btnSavePlayersList" CssClass="btn btn-default" runat="server" Text="Save As List" OnClick="btnSavePlayersList_Click" OnClientClick="return PromptForListName();" /> <asp:Button ID="btnClearPlayers" CssClass="btn btn-default" runat="server" Text="Clear All Players" OnClick="btnClearPlayers_Click" OnClientClick="confirm('Are you sure you want to remove all players in the players table?');" /></p>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="drpSavedPlayersLists" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>  
            </div>
        </div>
        <div id="divAddedPlayers" class="col-xs-12 col-sm-6 col-md-3">
            <h2>Added Players</h2>
            <asp:UpdatePanel ID="upAddedPlayers" runat="server" ChildrenAsTriggers="true">
                <ContentTemplate>
                    <asp:GridView ID="gvPlayers" ClientIDMode="Static" runat="server" GridLines="None" AutoGenerateColumns="false" ShowHeader="false" AlternatingRowStyle-CssClass="alternateRow" OnRowDeleting="gvPlayers_RowDeleting">
                        <Columns>
                            <asp:TemplateField ShowHeader="false" ItemStyle-CssClass="text-right">
                                <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate><span id="spanPlayerName" runat="server" data-id="<%# ((PlayerSummaryView)Container.DataItem).Id %>"> <%# ((PlayerSummaryView)Container.DataItem).PlayerName %></span></ItemTemplate>
                            </asp:TemplateField>
                            <asp:ButtonField ButtonType="Link" Text="Remove" CommandName="Delete" />
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnAddPlayer" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>      
        </div>
        <div id="divGenerationOptions" class="panel panel-default col-xs-12 col-sm-6 col-md-4">
            <div class="panel-body">
                <h2>Generation Options</h2>
                <table>
                    <tr><td>Number of Teams</td><td><asp:TextBox type="number" min="2" id="txtNumberOfTeams" runat="server" class="longInput" /></td></tr>
                    <tr><td>Generation Strategy</td><td><asp:DropDownList ID="drpTeamGenerationStrategy" runat="server" ClientIDMode="Static" CssClass="longInput" /></td></tr>
                </table>
                <p><asp:Button id="btnGenerateTeams" CssClass="btn btn-primary" runat="server" Text="Generate Teams" OnClick="btnGenerateTeams_Click" /></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <p id="pTeamGenerationStrategyText" class="hide">Team Generation Strategy</p>
            <asp:UpdatePanel ID="upTeamOutput" runat="server">
                <ContentTemplate>
                    <div id="divOutputTeamsSection" runat="server">
                        <h2>Generated Teams</h2>
                        <p><a id="aMakeEditable" href="javascript:ToggleEditTeams();">Make Teams Editable</a></p>
                        <div id="divTeamOutput" runat="server"></div>
                        <div class="clear"></div>
                        <p><a href="#" onclick="PrintTeams();" id="printLink">Print Teams</a></p>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnGenerateTeams" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="ScriptContent" runat="server">
    <script type="text/javascript" src="Scripts/GenerateTeams.js?v=3"></script>
</asp:Content>