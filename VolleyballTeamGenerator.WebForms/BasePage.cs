﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VolleyballTeamGenerator.WebForms
{
    public class BasePage : System.Web.UI.Page
    {
        public bool HasErrors { get; private set; }

        public BasePage()
        {
            HasErrors = false;
        }

        public IList<string> ErrorMessages
        {
            set
            {
                ((SiteMaster)Master).DisplayMessages(value, "error");
                HasErrors = true;
            }
        }

        public IList<string> SuccessMessages
        {
            set
            {
                ((SiteMaster)Master).DisplayMessages(value, "success");
            }
        }

        public bool IsAdmin()
        {
            if (User != null && User.Identity != null)
            {
                return User.Identity.Name.Equals(ProjectConfiguration.AppSettings.AdminUserId);
            }
            return false;
        }
    }
}