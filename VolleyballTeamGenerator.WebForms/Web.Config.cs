﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VolleyballTeamGenerator.WebForms.ProjectConfiguration
{
    public static class AppSettings
    {
        public static string AdminUserId
        {
            get { return ConfigurationManager.AppSettings["AdminUserId"]; }
        }

        public static string Environment
        {
            get { return ConfigurationManager.AppSettings["Environment"]; }
        }

        public static EnvironmentEnum EnvironmentEnum
        {
            get { return (EnvironmentEnum)Enum.Parse(typeof(EnvironmentEnum), Environment); }
        }
    }

    public static class ConnectionStrings
    {
        public static string VolleyballTeamGeneratorEntities
        {
            get { return ConfigurationManager.ConnectionStrings["VolleyballTeamGeneratorEntities"].ConnectionString; }
        }
    }

    public enum EnvironmentEnum
    {
        Development,
        Production
    }
}
