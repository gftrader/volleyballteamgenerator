﻿var selectedRowButton = null;

function pageLoad()
{
    initializeDataGridEvents();
}

function initializeDataGridEvents(){
    $("#numericOverlay button").each(function () {
        var btn = $(this);
        btn.click(function buttonClick() {
            if(btn.text() !== "Cancel")
            {
                $(".ipt-attribute-value").val(btn.text());
                $(selectedRowButton).parents("tr").find(".btn-update-attribute").click();
            }
            else
            {
                $(selectedRowButton).parents("tr").find(".span-attribute-value").show();
                $(selectedRowButton).parents("tr").find(".btn-edit-attribute").show();
            }
            selectedRowButton = null;
            $("#numericOverlay").css("display", "none");
        });
    });

    $(".btn-update-attribute").hide();
    $(".btn-edit-attribute").click(function () {
        var tr = $(this).parents("tr");
        tr.find(".span-attribute-value").hide();
        tr.find(".btn-update-attribute").hide();
        $(this).hide();
        displayOverlay(this);
    });
    $(".btn-update-attribute").click(function () {
        var tr = $(this).parents("tr");
        tr.find(".span-attribute-value").show();
        tr.find(".btn-update-attribute").hide();
        tr.find(".btn-edit-attribute").show();
        tr.find(".ipt-attribute-value").val(tr.find(".span-attribute-value").text());
        $(this).hide();
    });
};

function displayOverlay(ele) {
    selectedRowButton = ele;
    $("#numericOverlay").css("display", "block");
    $("#numericOverlay").css("width", window.outerWidth + 100); //add extra 100 pixels or so to account for scrollbar widths
    $("#numericOverlay").css("height", window.outerHeight + 100);
    $("#numericOverlay table").css("margin-left", (window.innerWidth / 2) - ($("#numericOverlay table").width() / 2));
    $("#numericOverlay table").css("margin-top", (window.innerHeight / 2) - ($("#numericOverlay table").height() / 2));
}

function showWarning(cbx) {
    if (!cbx.checked) {
        cbx.checked = !confirm("Are you sure you want to remove this player from the group? If so, all their attributes and levels will be removed upon clicking the 'Update Groups' button");
    }
}