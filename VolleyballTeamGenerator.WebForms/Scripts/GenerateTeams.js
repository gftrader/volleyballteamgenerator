﻿var playersInSelectedGroup = {};
var playerNames = [];
var editable = false;
var playerSwapCell = null;

function pageLoad(sender, args) {
    $("[id*='txtAddPlayerName']").val("");
}

$(document).ready(function () {
    ShowOrHideFilters();
    LoadAllPlayerNames();
    $("[id*='txtAddPlayerName']").ready(SetupAutocomplete);
});

function ShowOrHideFilters() {
    if ($("#drpPlayerGroups").val() > 0) {
        $("#divAdditionalFilters").removeClass("hide");
    }
    else {
        $("#divAdditionalFilters").addClass("hide");
    }
}

function LoadAllPlayerNames() {
    $("select[id*='drpPlayers'] option").each(function () {
        playerNames[playerNames.length] = $(this).text();
    });
}

function SetupAutocomplete() {
    $("[id*='txtAddPlayerName']").autocomplete({ source: playerNames });
}

function PrintTeams() {
    $("#topSection").toggleClass("hide");
    $("#divAdditionalFilters").toggleClass("hide");
    $("#pTeamGenerationStrategyText").toggleClass("hide");
    $(".teamTable").toggleClass("largeFont");

    if ($("#topSection").hasClass("hide")) {
        $("#printLink").text("Return");
        $("#pTeamGenerationStrategyText").text(sprintf("Team Generation Strategy: %s", $("#drpTeamGenerationStrategy option:selected").text()));
        print();
    }
    else {
        $("#printLink").text("Print Teams");
    }
}

function SwapPlayers(tableCell) {
    if (editable) {
        if (playerSwapCell == null && tableCell.parentNode.tagName.toLowerCase() === "td") {
            $(tableCell).addClass("swapPlayer");
            playerSwapCell = $(tableCell);
        }
        else {
            if (tableCell.parentNode.tagName.toLowerCase() === "td") {
                var selectedPlayerName = $(tableCell).text();
                $(tableCell).text(playerSwapCell.text());
            }
            else {
                playerSwapCell.parents("tr").remove();
                $(tableCell).parents("tr").parent().append(playerSwapCell.parents("tr"));

            }
            playerSwapCell.text(selectedPlayerName);
            ResetSwapPlayers();
        }
    }
}

function PromptForListName() {
    var listName = prompt("Please enter a list name", $("[id*='txtSavePlayersListName']").val());
    if (listName !== null && listName.length > 0) {
        $("[id*='txtSavePlayersListName']").val(listName);
        return true;
    }

    return false;
}

/* unused hereafter */
function LoadTeams(response) {
    var teams = response.d;
    $("#teamOutput").children().remove();
    for (var i in teams) {
        var teamTable = $("<table class=\"teamTable\"></table>");
        $("#teamOutput").append(teamTable);
        var team = teams[i];
        teamTable.append($(sprintf("<tr><th>%s</th></tr>", team.Name)));
        var playersOnTeam = [];
        for (var j in team.PlayerViews) {
            playersOnTeam[playersOnTeam.length] = team.PlayerViews[j].PlayerName;
        }

        playersOnTeam.sort();
        for (var j = 0; j < playersOnTeam.length; j++) {
            teamTable.append($(sprintf("<tr><td>%s</td></tr>", playersOnTeam[j])));
        }
    }

    $("#outputTeamsSection").removeClass("hide");
}

function validateName() {
    for (var i = 0; i < playerNames.length; i++) {
        if (playerNames[i] === $("#txtAddPlayerName").val()) { return true; }
    }
    alert(sprintf("Name '%s' not found in list of players. Please try again.", $("#txtAddPlayerName").val()));
    return false;
}

function ValidateInputs() {
    var errors = [];
    if ($("#txtNumberOfTeams").val() <= 1) { errors[errors.length] = "Number of Teams entered must be at least 2"; }
    if ($("#drpPlayerGroups").val() < 0) { errors[errors.length] = "You must select a player group before you can generate teams"; }
    if ($("#drpTeamGenerationStrategy").val() < 0) { errors[errors.length] = "You must select a generation strategy before you can generate teams"; }
    if ($("#tblPlayers tr.dataRow").length < 5) { errors[errors.length] = "You must enter more players to generate teams"; }
    return errors;
}

function DisplayErrors(errors) {
    var errorMessage = "The following errors must be corrected before you can generate teams: ";
    for (var i = 0; i < errors.length; i++) {
        errorMessage += "\n\t" + errors[i];
    }

    alert(errorMessage);
}

function ClearPlayers() {
    var reallyClear = confirm("Are you sure you want to remove all players in the players table?");
    if (reallyClear) {
        $("#tblPlayers tr.dataRow").remove();
    }

    return reallyClear;
}

function SetupPlayerGroupSelectionDropdown() {
    $("#drpPlayerGroups").change(GetPlayersInSelectedGroup);
    if ($("#drpPlayerGroups").val() > 0) {
        $("#drpPlayerGroups").change();
    }
}

function ToggleEditTeams() {
    editable = !editable;

    if (!editable) {
        $("#aMakeEditable").text("Make Players Editable");
        ResetSwapPlayers();
    }
    else {
        $("#aMakeEditable").text("Disable Editing");
    }
}

function ResetSwapPlayers() {
    $(".swapPlayer").removeClass("swapPlayer");
    playerSwapCell = null;
}

function ClearPlayerName() {
    $("[id*='txtAddPlayerName']").val("");
}