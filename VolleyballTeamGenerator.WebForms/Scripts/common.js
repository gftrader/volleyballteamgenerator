﻿function MakeAJAXCall(ajaxURL, dataArray, successFunction, async) {
    $.ajax({
        traditional: true,
        type: "POST",
        url: ajaxURL,
        data: JSON.stringify(dataArray),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: successFunction,
        async: async
    });
}