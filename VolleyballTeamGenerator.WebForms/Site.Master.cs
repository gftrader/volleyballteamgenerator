﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace VolleyballTeamGenerator.WebForms
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.DataBind();
        }

        public void DisplayMessages(IList<string> messages, string cssClass)
        {
            if (messages == null || messages.Count == 0) { return; }

            HtmlGenericControl ul = new HtmlGenericControl("ul");
            foreach (string message in messages)
            {
                ul.Controls.Add(new HtmlGenericControl("li") { InnerText = message });
            }

            divMessages.Controls.Add(ul);
            if (!String.IsNullOrEmpty(cssClass)) { divMessages.Attributes["class"] = cssClass; }
            divMessages.Visible = true;
        }
    }
}
