﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StructureMap;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Infrastructure.OpenXml;
using VolleyballTeamGenerator.Domain.Services.Interfaces;
using VolleyballTeamGenerator.Application.Messaging;
using VolleyballTeamGenerator.Application.ViewInterfaces;
using VolleyballTeamGenerator.Application.ViewModels;
using VolleyballTeamGenerator.Application.Presenters;

namespace VolleyballTeamGenerator.WebForms.Players
{
    public partial class BulkImportExport : BasePage, IBulkImportExportView
    {
        BulkImportExportPresenter bulkIEPresenter;

        protected override void OnInit(EventArgs e)
        {
            bulkIEPresenter = new BulkImportExportPresenter(this, ObjectFactory.GetInstance<IExcelExporter>(), ObjectFactory.GetInstance<IPlayerService>(), ObjectFactory.GetInstance<IPlayerGroupService>(), ObjectFactory.GetInstance<IVolleyballAttributeService>());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                bulkIEPresenter.Display();
            }
        }

        protected void btnImportPlayers_Click(object sender, EventArgs e)
        {
            bulkIEPresenter.PerformBulkImport();
        }

        protected void btnExportPlayers_Click(object sender, EventArgs e)
        {
            bulkIEPresenter.PerformBulkExport();
        }

        protected void drpGroups_SelectedIndexChanged(object sender, EventArgs e)
        {
            divContent.Visible = SelectedPlayerGroupId > 0;
        }

        public int SelectedPlayerGroupId
        {
            get 
            {
                return Convert.ToInt32(drpGroups.SelectedValue);
            }
        }

        public HttpPostedFile FileUpload
        {
            get
            {
                return fuImportPlayers.PostedFile;
            }
        }

        public string ExportDocumentName
        {
            get { return "VolleyballPlayerExport"; }
        }

        public ExportDocumentResponse ExportDocumentResponse
        {
            set
            {
                Response.Clear();
                Response.ContentType = value.ContentType;
                Response.AddHeader("content-disposition", "attachment;filename=volleyball-player-export.xlsx");
                value.DocumentStream.Position = 0;
                value.DocumentStream.CopyTo(Response.OutputStream);
                Response.End();
            }
        }

        public IList<PlayerGroupView> AllPlayerGroups
        {
            set 
            {
                UIHelper.BindListControl(drpGroups, value, "Name", "Id", true, "", "-1");
                divContent.Visible = false;
                divImportResults.Visible = false;
            }
        }

        public IList<PlayerUpdateView> UploadedPlayers
        {
            set
            {
                gvContent.DataSource = value;
                gvContent.DataBind();
                divImportResults.Visible = value.Count > 0;
            }
        }
    }
}