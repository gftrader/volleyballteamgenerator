﻿<%@ Page Title="Manage Player" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="ManagePlayer.aspx.cs" Inherits="VolleyballTeamGenerator.WebForms.Players.ManagePlayer" %>
<%@ Import Namespace="VolleyballTeamGenerator.Application.ViewModels" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h1>Manage Player</h1>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
            <h2>Player Details</h2>
            <asp:DetailsView ID="dvPlayerDetails" runat="server" CssClass="detailsView" AlternatingRowStyle-CssClass="alternateRow" GridLines="None" AutoGenerateRows="false"
                OnDataBound="dvPlayerDetails_DataBound" OnModeChanging="dvPlayerDetails_ModeChanging" OnItemUpdating="dvPlayerDetails_ItemUpdating">
                <Fields>
                    <asp:TemplateField HeaderText="First Name">
                        <ItemTemplate><%# ((PlayerSummaryView)Container.DataItem).FirstName %></ItemTemplate>
                        <EditItemTemplate><asp:TextBox ID="txtFirstName" runat="server" Text="<%# ((PlayerSummaryView)Container.DataItem).FirstName %>" /></EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Middle Name">
                        <ItemTemplate><%# ((PlayerSummaryView)Container.DataItem).MiddleName %></ItemTemplate>
                        <EditItemTemplate><asp:TextBox ID="txtMiddleName" runat="server" Text="<%# ((PlayerSummaryView)Container.DataItem).MiddleName %>" /></EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Last Name">
                        <ItemTemplate><%# ((PlayerSummaryView)Container.DataItem).LastName %></ItemTemplate>
                        <EditItemTemplate><asp:TextBox ID="txtLastName" runat="server" Text="<%# ((PlayerSummaryView)Container.DataItem).LastName %>" /></EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Gender">
                        <ItemTemplate><%# ((PlayerSummaryView)Container.DataItem).Gender %></ItemTemplate>
                        <EditItemTemplate><asp:DropDownList ID="drpGender" runat="server">
                            <asp:ListItem Text="F" Value="F" />
                            <asp:ListItem Text="M" Value="M" />
                        </asp:DropDownList></EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email">
                        <ItemTemplate><%# ((PlayerSummaryView)Container.DataItem).Email %></ItemTemplate>
                        <EditItemTemplate><asp:TextBox ID="txtEmail" runat="server" Text="<%# ((PlayerSummaryView)Container.DataItem).Email %>" /></EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <ItemTemplate><%# ((PlayerSummaryView)Container.DataItem).Active %></ItemTemplate>
                        <EditItemTemplate><asp:CheckBox ID="cbxActive" runat="server" Checked="<%# ((PlayerSummaryView)Container.DataItem).Active %>" /></EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Comments">
                        <ItemTemplate><%# ((PlayerSummaryView)Container.DataItem).Comments %></ItemTemplate>
                        <EditItemTemplate><asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Rows="5" Text="<%# ((PlayerSummaryView)Container.DataItem).Comments %>" /></EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate><asp:Button ID="btnEdit" CssClass="btn btn-default" runat="server" CommandName="Edit" Text="Edit" /></ItemTemplate>
                        <EditItemTemplate><asp:Button ID="btnUpdate" CssClass="btn btn-primary" runat="server" CommandName="Update" Text="Update" /> <asp:Button ID="btnCancel" CssClass="btn btn-default" runat="server" CommandName="Cancel" Text="Cancel" /></EditItemTemplate>
                    </asp:TemplateField>
                </Fields>
            </asp:DetailsView>
            <h2>Player Groups</h2>
            <asp:GridView ID="gvPlayerGroups" runat="server" CssClass="gridView" AlternatingRowStyle-CssClass="alternateRow" AutoGenerateColumns="false" GridLines="None" ShowHeader="false">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate><input type="checkbox" ID="cbxGroupID" runat="server" onclick="showWarning(this);" value="<%# ((PlayerGroupView)Container.DataItem).Id %>" /> <%# ((PlayerGroupView)Container.DataItem).Name %></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <p><asp:Button CssClass="btn btn-primary" ID="btnUpdatePlayerGroups" runat="server" Text="Update Groups" OnClick="btnUpdatePlayerGroups_Click" /></p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-8">
            <h2>Player Attributes</h2>
            <asp:UpdatePanel ID="upPlayerAttributes" runat="server" ChildrenAsTriggers="true">
                <ContentTemplate>
                    <asp:GridView ID="gvPlayerAttributes" runat="server" CssClass="gridView" AlternatingRowStyle-CssClass="alternateRow" AutoGenerateColumns="false" GridLines="None" EmptyDataText="There are no volleyball attributes assigned to this player"
                    OnRowEditing="gvPlayerAttributes_RowEditing" OnRowCancelingEdit="gvPlayerAttributes_RowCancelingEdit" OnRowUpdating="gvPlayerAttributes_RowUpdating">
                        <Columns>
                            <asp:TemplateField HeaderText="Group Name">
                                <ItemTemplate><%# ((PlayerAttributeView)Container.DataItem).PlayerGroupName %></ItemTemplate>
                                <EditItemTemplate><input type="hidden" id="iptPlayerGroupID" runat="server" value="<%# ((PlayerAttributeView)Container.DataItem).PlayerGroupId %>" /><%# ((PlayerAttributeView)Container.DataItem).PlayerGroupName %></EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Attribute Name">
                                <ItemTemplate><%# ((PlayerAttributeView)Container.DataItem).VolleyballAttributeName %></ItemTemplate>
                                <EditItemTemplate><input type="hidden" id="iptVolleyballAttributeID" runat="server" value="<%# ((PlayerAttributeView)Container.DataItem).VolleyballAttributeId %>" /><%# ((PlayerAttributeView)Container.DataItem).VolleyballAttributeName %></EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Level" HeaderStyle-CssClass="right" ItemStyle-CssClass="right">
                                <ItemTemplate><%# ((PlayerAttributeView)Container.DataItem).VolleyballAttributeLevel %></ItemTemplate>
                                <EditItemTemplate><asp:TextBox type="number" min="1" max="10" ID="txtVolleyballAttributeLevel" runat="server" CssClass="shortText" Text="<%# ((PlayerAttributeView)Container.DataItem).VolleyballAttributeLevel %>" /></EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate><asp:Button ID="btnEdit" CssClass="btn btn-default" runat="server" CommandName="Edit" Text="Edit" /></ItemTemplate>
                                <EditItemTemplate><asp:Button ID="btnUpdate" CssClass="btn btn-primary" runat="server" CommandName="Update" Text="Update" /> <asp:Button ID="btnCancel" CssClass="btn btn-default" runat="server" CommandName="Cancel" Text="Cancel" /></EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="clear col-xs-12">
            <h2>Related Actions</h2>
            <ul>
                <li><a href="AddPlayer.aspx">Add a Player</a></li>
                <li><a href="FindPlayers.aspx">Find Players</a></li>
            </ul>
        </div>
    </div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ScriptContent">
    <script type="text/javascript" src="../Scripts/ManagePlayer.js"></script>
</asp:Content>