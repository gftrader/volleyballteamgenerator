﻿<%@ Page Title="Bulk Import/Export" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BulkImportExport.aspx.cs" Inherits="VolleyballTeamGenerator.WebForms.Players.BulkImportExport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h1>Bulk Import/Export</h1>
<p>This page is used to import or export player skills to/from the database.</p>
<p>Choose a group to Import/Export players to/from: <asp:DropDownList ID="drpGroups" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpGroups_SelectedIndexChanged" /></p>
<div id="divContent" runat="server">
    <h2>Import</h2>
    <div id="divImportList">
        <p>To import a list of players and skills, use the file upload and click the button below: <asp:FileUpload ID="fuImportPlayers" runat="server" /></p>
        <p><asp:Button ID="btnImportPlayers" runat="server" Text="Import Players" OnClick="btnImportPlayers_Click" /></p>
    </div>
    <div id="divImportResults" runat="server">
        <h3>Players Added/Updated</h3>
        <asp:GridView ID="gvContent" runat="server" GridLines="None" CssClass="gridView" AlternatingRowStyle-CssClass="alternateRow" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundField HeaderText="First Name" DataField="FirstName" />
                <asp:BoundField HeaderText="Last Name" DataField="LastName" />
                <asp:BoundField HeaderText="Email" DataField="Email" />
                <asp:BoundField HeaderText="Gender" DataField="Gender" />
                <asp:BoundField HeaderText="Active" DataField="Active" />
                <asp:BoundField HeaderText="Comments" DataField="Comments" />
            </Columns>
        </asp:GridView>
    </div>
    <h2>Export</h2>
    <p>To export the current list of players click the button below:</p>
    <p><asp:Button ID="btnExportPlayers" runat="server" Text="Export Players" OnClick="btnExportPlayers_Click" /></p>
</div>
</asp:Content>