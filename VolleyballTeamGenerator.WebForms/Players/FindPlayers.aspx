﻿<%@ Page Title="Find Players" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FindPlayers.aspx.cs" Inherits="VolleyballTeamGenerator.WebForms.Players.FindPlayers" %>
<%@ Import Namespace="VolleyballTeamGenerator.Application.ViewModels" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .filtersColumn select, .filtersColumn input { width: 200px; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Find Players</h1>
    <div class="row">
        <div class="filtersColumn col-xs-12 col-sm-6 col-md-4">
            <h2>Filters</h2>
            <div class="panel panel-default">
                <div class="panel-body">
                <table>
                    <tr><th>First Name</th><td><asp:TextBox ID="txtFindFirstName" runat="server" /></td></tr>
                    <tr><th>Last Name</th><td><asp:TextBox ID="txtFindLastName" runat="server" /></td></tr>
                    <tr><th>Email</th><td><asp:TextBox ID="txtFindEmail" runat="server" /></td></tr>
                    <tr><th>Gender</th><td><asp:DropDownList ID="drpGender" runat="server" CssClass="overrideWidth"><asp:ListItem Text="" Value="" /><asp:ListItem Text="F" Value="F" /><asp:ListItem Text="M" Value="M" /></asp:DropDownList></td></tr>
                    <tr><th>Group(s)</th><td><asp:ListBox ID="lbxPlayerGroups" runat="server" SelectionMode="Multiple" /></td></tr>
                </table>
                <p><asp:Button ID="btnFindPlayers" runat="server" Text="Find Players" OnClick="btnFindPlayers_Click" CssClass="btn btn-primary" /></p>
                </div>
            </div>
        </div>
        <div id="divResults" runat="server" class="col-xs-12 col-sm-6 col-md-8">
            <h2>Results</h2>
            <div class="resultsColumn">
                <p>Search returned <span id="spanNumberOfResults" runat="server" /> Results</p>
                <asp:GridView ID="gvPlayers" runat="server" CssClass="gridView" AutoGenerateColumns="false" AlternatingRowStyle-CssClass="alternateRow" AutoGenerateFields="false" GridLines="None" EmptyDataText="There are no players matching the search results">
                    <Columns>
                        <asp:BoundField HeaderText="Name" DataField="PlayerName" />
                        <asp:TemplateField HeaderText="Gender">
                            <ItemTemplate><%# ((PlayerSummaryView)Container.DataItem).Gender %></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email">
                            <ItemTemplate><%# ((PlayerSummaryView)Container.DataItem).Email %></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Active">
                            <ItemTemplate><%# ((PlayerSummaryView)Container.DataItem).Active %></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Comments">
                            <ItemTemplate><%# ((PlayerSummaryView)Container.DataItem).Comments %></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <a href="ManagePlayer.aspx?PlayerID=<%# ((PlayerSummaryView)Container.DataItem).Id %>"><button type="button" class="btn btn-default">View</button></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>