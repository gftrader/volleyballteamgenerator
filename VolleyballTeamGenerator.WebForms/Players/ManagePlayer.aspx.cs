﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using StructureMap;
using VolleyballTeamGenerator.Application.ViewInterfaces;
using VolleyballTeamGenerator.Application.Presenters;
using VolleyballTeamGenerator.Application.ViewModels;
using VolleyballTeamGenerator.Domain.Services.Interfaces;
using VolleyballTeamGenerator.Infrastructure;

namespace VolleyballTeamGenerator.WebForms.Players
{
    public partial class ManagePlayer : BasePage, IManagePlayerView
    {
        private ManagePlayerPresenter managePlayerPresenter;
        private GridViewRow selectedRow;
        private IList<PlayerGroupView> playerGroups = new List<PlayerGroupView>();

        protected override void OnInit(EventArgs e)
        {
            managePlayerPresenter = new ManagePlayerPresenter(this, ObjectFactory.GetInstance<IPlayerService>(), ObjectFactory.GetInstance<IPlayerGroupService>(), ObjectFactory.GetInstance<IVolleyballAttributeService>());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) { managePlayerPresenter.Display(); }
        }

        protected void dvPlayerDetails_DataBound(object sender, EventArgs e)
        {
            if (dvPlayerDetails.CurrentMode == DetailsViewMode.Edit)
            {
                ((DropDownList)dvPlayerDetails.FindControl("drpGender")).SelectedValue = ((PlayerSummaryView)dvPlayerDetails.DataItem).Gender;
            }
        }

        protected void dvPlayerDetails_ModeChanging(object sender, DetailsViewModeEventArgs e)
        {
            dvPlayerDetails.ChangeMode(e.NewMode);
            managePlayerPresenter.RefreshPlayerInformation();
        }

        protected void dvPlayerDetails_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            managePlayerPresenter.UpdatePlayer();
            if (!HasErrors)
            {
                dvPlayerDetails_ModeChanging(null, new DetailsViewModeEventArgs(DetailsViewMode.ReadOnly, false));
            }
        }

        protected void btnUpdatePlayerGroups_Click(object sender, EventArgs e)
        {
            managePlayerPresenter.UpdatePlayerGroups();
        }

        protected void gvPlayerAttributes_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvPlayerAttributes.EditIndex = e.NewEditIndex;
            managePlayerPresenter.RefreshPlayerAttributes();
        }

        protected void gvPlayerAttributes_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            managePlayerPresenter.UpdatePlayerAttributeLevel();
            if (!this.HasErrors)
            {
                gvPlayerAttributes_RowCancelingEdit(null, null);
            }
        }

        protected void gvPlayerAttributes_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvPlayerAttributes.EditIndex = -1;
            managePlayerPresenter.RefreshPlayerAttributes();
        }

        public int PlayerID
        {
            get { return Convert.ToInt32(Request.QueryString["playerID"]); }
        }

        public IList<PlayerGroupView> AllPlayerGroups
        {
            set
            {
                gvPlayerGroups.DataSource = value;
                gvPlayerGroups.DataBind();
            }
        }

        public PlayerSummaryView Player
        {
            set
            {
                if (value != null)
                {
                    dvPlayerDetails.DataSource = new List<PlayerSummaryView>(){ value };
                    dvPlayerDetails.DataBind();
                }
            }
        }

        public IList<PlayerAttributeView> PlayerAttributes
        {
            set
            {
                gvPlayerAttributes.DataSource = value;
                gvPlayerAttributes.DataBind();
            }
        }

        public IList<PlayerGroupView> PlayerGroups
        {
            set
            {
                playerGroups = value;
                SelectPlayerGroupsPresent();
            }
        }

        private void SelectPlayerGroupsPresent()
        {
            foreach (GridViewRow gvr in gvPlayerGroups.Rows)
            {
                HtmlInputCheckBox cbxGroupID = (HtmlInputCheckBox)gvr.FindControl("cbxGroupID");
                if (playerGroups.Any(pg => pg.Id == (Convert.ToInt32(cbxGroupID.Value))))
                {
                    cbxGroupID.Checked = true;
                }
            }
        }

        public IList<int> SelectedPlayerGroupIDs
        {
            get
            {
                List<int> selectedPlayerGroupIDs = new List<int>();
                foreach (GridViewRow gvr in gvPlayerGroups.Rows)
                {
                    if (gvr.RowType == DataControlRowType.DataRow)
                    {
                        HtmlInputCheckBox cbxGroupID = (HtmlInputCheckBox)gvr.FindControl("cbxGroupID");
                        if (cbxGroupID.Checked)
                        {
                            selectedPlayerGroupIDs.Add(Convert.ToInt32(cbxGroupID.Value));
                        }
                    }
                }

                return selectedPlayerGroupIDs;
            }
        }

        public string UpdatedFirstName
        {
            get { return ((TextBox)dvPlayerDetails.FindControl("txtFirstName")).Text.Trim(); }
        }

        public string UpdatedMiddleName
        {
            get { return ((TextBox)dvPlayerDetails.FindControl("txtMiddleName")).Text.Trim(); }
        }

        public string UpdatedLastName
        {
            get { return ((TextBox)dvPlayerDetails.FindControl("txtLastName")).Text.Trim(); }
        }

        public bool UpdatedActiveStatus
        {
            get { return ((CheckBox)dvPlayerDetails.FindControl("cbxActive")).Checked; }
        }

        public string UpdatedEmail
        {
            get { return ((TextBox)dvPlayerDetails.FindControl("txtEmail")).Text.Trim(); }
        }

        public string UpdatedGender
        {
            get { return ((DropDownList)dvPlayerDetails.FindControl("drpGender")).SelectedValue; }
        }

        public string UpdatedComments
        {
            get { return ((TextBox)dvPlayerDetails.FindControl("txtComments")).Text.Trim(); }
        }

        public int UpdatedPlayerAttributeGroupID
        {
            get { return Convert.ToInt32(((HtmlInputHidden)gvPlayerAttributes.Rows[gvPlayerAttributes.EditIndex].FindControl("iptPlayerGroupID")).Value); }
        }

        public int UpdatedVolleyballAttributeID
        {
            get { return Convert.ToInt32(((HtmlInputHidden)gvPlayerAttributes.Rows[gvPlayerAttributes.EditIndex].FindControl("iptVolleyballAttributeID")).Value); }
        }

        public double UpdatedVolleyballAttributeLevel
        {
            get { return Convert.ToDouble(((TextBox)gvPlayerAttributes.Rows[gvPlayerAttributes.EditIndex].FindControl("txtVolleyballAttributeLevel")).Text); }
        }
    }
}