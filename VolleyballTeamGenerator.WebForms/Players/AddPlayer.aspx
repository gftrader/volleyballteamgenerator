﻿<%@ Page Title="Add Player" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddPlayer.aspx.cs" Inherits="VolleyballTeamGenerator.WebForms.Players.AddPlayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Add Player</h1>
    <div class="row">
        <div class="col-xs-12 col-md-4">
            <div class="panel">
                <div class="panel-body">
                    <table>
                        <tr><th>First Name*</th><td><asp:TextBox id="txtFirstName" runat="server" /></td></tr>
                        <tr><th>Middle Name</th><td><asp:TextBox id="txtMiddleName" runat="server" /></td></tr>
                        <tr><th>Last Name*</th><td><asp:TextBox id="txtLastName" runat="server" /></td></tr>
                        <tr><th>Gender*</th><td><asp:DropDownList id="drpGender" runat="server"><asp:ListItem Text="" Value="" /><asp:ListItem Text="F" Value="F" /><asp:ListItem Text="M" Value="M" /></asp:DropDownList></td></tr>
                        <tr><th>Email</th><td><asp:TextBox ID="txtEmail" runat="server" /></td></tr>
                        <tr><th>Comments</th><td><asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" /></td></tr>
                    </table>
                </div>
            </div>
            <p><asp:Button ID="btnSubmit" CssClass="btn btn-primary" runat="server" Text="Add Player" OnClick="btnSubmit_Click" /><br />
            <span style="font-size: 10px; font-style: italic;">* = required</span></p>
        </div>
    </div>
</asp:Content>
