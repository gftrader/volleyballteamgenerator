﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StructureMap;
using VolleyballTeamGenerator.Application.ViewInterfaces;
using VolleyballTeamGenerator.Application.Presenters;
using VolleyballTeamGenerator.Application.ViewModels;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Domain.Services.Interfaces;

namespace VolleyballTeamGenerator.WebForms.Players
{
    public partial class AddPlayer : BasePage, IAddPlayerView
    {
        private AddPlayerPresenter addPlayerPresenter;
        protected override void OnInit(EventArgs e)
        {
            addPlayerPresenter = new AddPlayerPresenter(this, ObjectFactory.GetInstance<IPlayerService>());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            addPlayerPresenter.Display();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                addPlayerPresenter.AddPlayer();
            }
            catch (Exception ex)
            {
                this.ErrorMessages = new List<string>() { ex.Message };
            }
        }

        public PlayerUpdateView PlayerUpdateView
        {
            get 
            {
                return new PlayerUpdateView()
                {
                    FirstName = txtFirstName.Text,
                    MiddleName = txtMiddleName.Text,
                    LastName = txtLastName.Text,
                    Gender = drpGender.SelectedValue,
                    Email = txtEmail.Text,
                    Active = true,
                    Comments = txtComments.Text
                };
            }
        }

        public string ManagePlayerUrl
        {
            get { return "ManagePlayer.aspx"; }
        }

        public IHttpResponse HttpResponse
        {
            get
            {
                HttpResponseFactory.ActualResponse = Response;
                return HttpResponseFactory.Create();
            }
        }
    }
}