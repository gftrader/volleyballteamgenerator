﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StructureMap;
using VolleyballTeamGenerator.Application.Presenters;
using VolleyballTeamGenerator.Application.ViewModels;
using VolleyballTeamGenerator.Application.ViewInterfaces;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Domain.Services.Interfaces;

namespace VolleyballTeamGenerator.WebForms.Players
{
    public partial class FindPlayers : BasePage, IFindPlayersView
    {
        private FindPlayersPresenter findPlayersPresenter;

        protected override void OnInit(EventArgs e)
        {
            this.findPlayersPresenter = new FindPlayersPresenter(this, ObjectFactory.GetInstance<IPlayerService>(), ObjectFactory.GetInstance<IPlayerGroupService>());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.divResults.Visible = false;
                this.findPlayersPresenter.Display();
            }
        }

        protected void btnFindPlayers_Click(object sender, EventArgs e)
        {
            this.findPlayersPresenter.FindPlayers();
        }



        public IList<int> FindPlayerGroupIDs
        {
            get
            {
                return UIHelper.GetSelectedIDValues(lbxPlayerGroups);
            }
        }

        public IList<PlayerGroupView> AllPlayerGroups
        {
            set
            {
                UIHelper.BindListControl(lbxPlayerGroups, value, "Name", "Id");
            }
        }

        public IList<PlayerSummaryView> MatchingPlayers
        {
            set 
            {
                gvPlayers.DataSource = value;
                gvPlayers.DataBind();

                spanNumberOfResults.InnerText = String.Format("{0}",value.Count);
                divResults.Visible = true;
            }
        }

        public string FindPlayerFirstName
        {
            get { return txtFindFirstName.Text.Trim(); }
        }

        public string FindPlayerLastName
        {
            get { return txtFindLastName.Text.Trim(); }
        }

        public string FindPlayerEmail
        {
            get { return txtFindEmail.Text.Trim(); }
        }

        public string FindPlayerGender
        {
            get { return drpGender.SelectedValue; }
        }
    }
}