﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StructureMap;
using VolleyballTeamGenerator.Domain.Services.Interfaces;
using VolleyballTeamGenerator.Application.Presenters;
using VolleyballTeamGenerator.Application.ViewInterfaces;
using VolleyballTeamGenerator.Application.ViewModels;

namespace VolleyballTeamGenerator.WebForms
{
    public partial class PrintSignInSheet : BasePage, IPrintSignInSheetView
    {
        private PrintSignInSheetPresenter presenter;
        protected override void OnInit(EventArgs e)
        {
            presenter = new PrintSignInSheetPresenter(this, ObjectFactory.GetInstance<ISavedPlayerListService>());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                presenter.Display();
            }
        }

        public int? PlayerListID
        {
            get 
            {
                if (String.IsNullOrWhiteSpace(Request.QueryString["playerListID"]))
                {
                    return null;
                }
                return Convert.ToInt32(Request.QueryString["playerListID"]); 
            }
        }

        public IEnumerable<PlayerSummaryView> PlayersInList
        {
            set 
            {
                rptPlayers.DataSource = value;
                rptPlayers.DataBind();
            }
        }
    }
}