﻿<%@ Page Title="Print Sign In Sheet" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PrintSignInSheet.aspx.cs" Inherits="VolleyballTeamGenerator.WebForms.PrintSignInSheet" %>
<%@ Import Namespace="VolleyballTeamGenerator.Application.ViewModels" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="Content/css/PrintSignInSheet.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Volleyball Sign-in Sheet</h1>
    <div class="signInList row">
        <table class="col-xs-12 headerTable">
            <tr class="row"><th class="col-xs-7">Date/Day: <input type="text" value="<%= String.Format("{0:d}",DateTime.Today) %>" /> Time Period: <input type="time" /></th><th class="col-xs-5">Group Leader: <input type="text" /></th></tr>
        </table>
        <asp:Repeater ID="rptPlayers" runat="server">
            <HeaderTemplate>
                <table class="col-xs-12 playersTable">
                    <tr>
                        <th colspan="2"></th>
                        <th colspan="2" class="text-center">Paid</th>
                    </tr>
                    <tr>
                        <th class="text-right">#</th>
                        <th>Player: (1 player per line, first & last name, please print clearly)</th>
                        <th class="text-center small-column-width">Cash</th>
                        <th class="text-center small-column-width">Coupon</th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr>
                        <td class="small-column-width text-right"><%# (Container.ItemIndex + 1) %></td>
                        <td><%# ((Container.DataItem) as PlayerView).PlayerName %></td>
                        <td></td>
                        <td></td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <table class="col-xs-12 headerTable">
            <tr class="row">
                <th class="col-xs-4">Cash:</th>
                <th class="col-xs-4">Coupons:</th>
                <th class="col-xs-4">Other:</th>
            </tr>
        </table>
        <p class="printButton"><button class="btn btn-primary">Print</button></p>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".printButton button").click(function () {
                print();
            });
        });
    </script>
</asp:Content>
