﻿<%@ Page Title="About Us" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="About.aspx.cs" Inherits="VolleyballTeamGenerator.WebForms.About" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>About</h2>
    <p>This application exists to create volleyball teams based on given player skill sets.</p>
    <p>Players are uploaded through the Bulk Import/Export interface and their skills are updated as a result of the updates.</p>
</asp:Content>
