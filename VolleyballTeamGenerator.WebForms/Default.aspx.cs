﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using StructureMap;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Application.ViewModels;
using VolleyballTeamGenerator.Application.ViewInterfaces;
using VolleyballTeamGenerator.Application.Presenters;
using VolleyballTeamGenerator.Domain.Services.Interfaces;

namespace VolleyballTeamGenerator.WebForms
{
    public partial class _Default : BasePage, IGenerateTeamsView
    {
        private GenerateTeamsPresenter gtPresenter;
        private int selectedPlayerToRemoveIndex = -1;

        protected override void OnInit(EventArgs e)
        {
            gtPresenter = new GenerateTeamsPresenter(this, ObjectFactory.GetInstance<ITeamGenerationDomainService>(), ObjectFactory.GetInstance<IPlayerService>(), ObjectFactory.GetInstance<IPlayerGroupService>(), ObjectFactory.GetInstance<ISavedPlayerListService>());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                gtPresenter.Display();
            }
        }

        #region "Event Handlers"
        protected void btnClearPlayers_Click(object sender, EventArgs e)
        {
            gtPresenter.ClearListOfPlayers();
            drpSavedPlayersLists.SelectedIndex = 0;
        }

        protected void btnAddPlayer_Click(object sender, EventArgs e)
        {
            gtPresenter.AddPlayerToListOfPlayers();
        }

        protected void drpPlayerGroups_SelectedIndexChanged(object sender, EventArgs e)
        {
            gtPresenter.GetPlayersInSelectedGroup();
        }

        protected void btnGenerateTeams_Click(object sender, EventArgs e)
        {
            gtPresenter.GenerateTeams();
        }

        protected void drpSavedPlayersLists_SelectedIndexChanged(object sender, EventArgs e)
        {
            gtPresenter.LoadSavedPlayersList();
        }

        protected void btnSavePlayersList_Click(object sender, EventArgs e)
        {
            gtPresenter.SavePlayersList();
        }

        protected void btnDeleteSavedPlayersList_Click(object sender, EventArgs e)
        {
            gtPresenter.DeletePlayersList();
        }

        protected void gvPlayers_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            selectedPlayerToRemoveIndex = e.RowIndex;
            gtPresenter.RemovePlayerFromPlayersList();
        }
        #endregion

        #region "IGenerateTeamsView Members"
        public int SelectedPlayerGroupID 
        {
            get { return Convert.ToInt32(drpPlayerGroups.SelectedValue); }
        }
        public int PlayerIDToAdd 
        { 
            get 
            { 
                return Convert.ToInt32(drpPlayers.Items.FindByText(txtAddPlayerName.Text).Value);
            }
        }
        public int NumberOfTeams 
        {
            get { return Convert.ToInt32(txtNumberOfTeams.Text); } 
        }
        public int SelectedTeamGenerationStrategyID 
        {
            get { return Convert.ToInt32(drpTeamGenerationStrategy.SelectedValue); }
        }

        public int SelectedPlayerToRemoveIndex
        {
            get { return selectedPlayerToRemoveIndex;  }
        }

        public int SelectedSavedPlayersListID
        {
            get { return Convert.ToInt32(drpSavedPlayersLists.SelectedValue); }
        }

        public string SavedPlayersListName
        {
            get { return txtSavePlayersListName.Text.Trim(); }
            set { txtSavePlayersListName.Text = value; }
        }

        public IList<PlayerSummaryView> PlayersToUseForTeamGeneration 
        { 
            get
            {
                IList<PlayerSummaryView> pvs = new List<PlayerSummaryView>();
                foreach (GridViewRow gvr in gvPlayers.Rows)
                {
                    if (gvr.RowType == DataControlRowType.DataRow)
                    {
                        HtmlGenericControl spanPlayerName = (HtmlGenericControl)gvr.FindControl("spanPlayerName");
                        pvs.Add(new PlayerSummaryView(){ Id = Convert.ToInt32(spanPlayerName.Attributes["data-id"]), PlayerName = spanPlayerName.InnerText });
                    }
                }

                return pvs;
            } 
            set
            {
                gvPlayers.DataSource = value;
                gvPlayers.DataBind();

                aPrintSignInSheet.HRef = String.Format("PrintSignInSheet.aspx?playerListID={0}", this.SelectedSavedPlayersListID);
                spanPlayerListOptions.Visible = this.SelectedSavedPlayersListID > 0;
            } 
        }

        public IList<PlayerGroupView> PlayerGroupViews
        {
            set 
            { 
                UIHelper.BindListControl(drpPlayerGroups, value, "Name", "Id", true);
                divOutputTeamsSection.Visible = false;
            }
        }

        public IList<CompositionStrategyView> CompositionStrategyViewNames
        {
            set { UIHelper.BindListControl(drpTeamGenerationStrategy, value, "Name", "Id", true); }
        }

        public IEnumerable<TeamView> GeneratedTeams
        {
            set 
            {
                foreach (TeamView team in value)
                {
                    GridView gv = new GridView()
                    {
                        ID = String.Format("gv{0}", team.Name),
                        AutoGenerateColumns = false,
                        CssClass = "teamTable col-xs-6",
                        UseAccessibleHeader = true,
                        GridLines = GridLines.None,
                    };

                    gv.RowDataBound += new GridViewRowEventHandler(gv_RowDataBound);
                    gv.AlternatingRowStyle.CssClass = "alternateRow";
                    gv.Columns.Add(new TemplateField() { HeaderTemplate = new HeaderTemplateField(team.Name), ItemTemplate = new PlayerTemplateField() });
                    gv.DataSource = team.Players;
                    gv.DataBind();
                    divTeamOutput.Controls.Add(gv);
                }

                divOutputTeamsSection.Visible = true;
            }
        }

        private void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                PlayerSummaryView player = (PlayerSummaryView)(e.Row.DataItem);
                ((HtmlGenericControl)e.Row.FindControl("playerCell")).InnerText = player.PlayerName;
            }
        }

        public IList<PlayerSummaryView> PlayersInSelectedGroup
        {
            set { UIHelper.BindListControl(drpPlayers, value, "PlayerName", "Id"); }
        }

        public IList<SavedPlayersListView> SavedPlayersLists
        {
            set 
            {
                UIHelper.BindListControl(drpSavedPlayersLists, value, "Name", "Id", true);
                spanPlayerListOptions.Visible = false;
            }
        }
        #endregion

        private class HeaderTemplateField : ITemplate
        {
            private string headerText;
            public HeaderTemplateField(string headerText)
            {
                this.headerText = headerText;
            }

            public void InstantiateIn(Control container)
            {
                (container as WebControl).Attributes["onclick"] = "SwapPlayers(this);";
                (container as TableCell).Text = headerText;
            }
        }

        private class PlayerTemplateField : ITemplate
        {
            public void InstantiateIn(Control container)
            {
                HtmlGenericControl divPlayer = new HtmlGenericControl("div");
                divPlayer.ID = "playerCell";
                divPlayer.Attributes["class"] = "playerCell";
                divPlayer.Attributes["onclick"] = "SwapPlayers(this);";
                container.Controls.Add(divPlayer);
            }
        }
    }
}