﻿using System.Linq;
using NUnit.Framework;

namespace VolleyballTeamGenerator.Repository.EF.Tests.Repositories
{
    [TestFixture]
    public class PlayerRepositoryTests : PlayerRepositoryTests_Base
    {
        [Test]
        public void FindById_IdFound_ReturnsExpectedAttribute()
        {
            var fakeId = allFakePlayers.FirstOrDefault().Id;

            var result = autoMocker.ClassUnderTest.FindById(fakeId);

            Assert.AreEqual(fakeId, result.Id);
        }

        [Test]
        public void FindById_IdNotFound_ReturnsNull()
        {
            var fakeId = -1;

            var result = autoMocker.ClassUnderTest.FindById(fakeId);

            Assert.IsNull(result);
        }

        [Test]
        public void FindByName_NameFound_ReturnsExpectedVolleyballAttribute()
        {
            var fakeFirstName = allFakePlayers.FirstOrDefault().FirstName;
            var fakeLastName = allFakePlayers.FirstOrDefault().LastName;

            var result = autoMocker.ClassUnderTest.FindByName(fakeFirstName, fakeLastName);

            Assert.AreEqual(fakeFirstName, result.FirstName);
            Assert.AreEqual(fakeLastName, result.LastName);
        }

        [Test]
        public void FindByName_NameNotFound_ReturnsNull()
        {
            var fakeName = "";

            var result = autoMocker.ClassUnderTest.FindByName(fakeName, fakeName);

            Assert.IsNull(result);
        }
    }
}