﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Rhino.Mocks;
using StructureMap.AutoMocking;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Repository.EF.Repositories;

namespace VolleyballTeamGenerator.Repository.EF.Tests.Repositories
{
    [TestFixture]
    public class VolleyballAttributeRepositoryTests
    {
        private RhinoAutoMocker<VolleyballAttributeRepository> autoMocker;
        private IDbSet<VolleyballAttribute> allFakeVolleyballAttributes;

        [SetUp]
        public void SetupTests()
        {
            autoMocker = new RhinoAutoMocker<VolleyballAttributeRepository>();
            allFakeVolleyballAttributes = GetAllFakeVolleyballAttributes();
            autoMocker.Get<IDbContext>().Stub(c => c.Set<VolleyballAttribute>()).Return(allFakeVolleyballAttributes);
        }

        [Test]
        public void FindByName_NameFound_ReturnsExpectedVolleyballAttribute()
        {
            var fakeSkillName = allFakeVolleyballAttributes.FirstOrDefault().Name;

            var result = autoMocker.ClassUnderTest.FindByName(fakeSkillName);

            Assert.AreEqual(fakeSkillName, result.Name);
        }

        [Test]
        public void FindByName_NameNotFound_ReturnsNull()
        {
            var fakeSkillName = "";

            var result = autoMocker.ClassUnderTest.FindByName(fakeSkillName);

            Assert.IsNull(result);
        }

        [Test]
        public void FindById_IdFound_ReturnsExpectedAttribute()
        {
            var fakeID = allFakeVolleyballAttributes.FirstOrDefault().Id;

            var result = autoMocker.ClassUnderTest.FindById(fakeID);

            Assert.AreEqual(fakeID, result.Id);
        }

        [Test]
        public void FindById_IdNotFound_ReturnsNull()
        {
            var fakeID = -1;

            var result = autoMocker.ClassUnderTest.FindById(fakeID);

            Assert.IsNull(result);
        }

        private IDbSet<VolleyballAttribute> GetAllFakeVolleyballAttributes()
        {
            var db = new FakeDbSet<VolleyballAttribute>();
            db.Add(new VolleyballAttribute() { Id = 1, Name = "Attribute 1" });
            db.Add(new VolleyballAttribute() { Id = 2, Name = "Hitting" });
            db.Add(new VolleyballAttribute() { Id = 3, Name = "Blocking" });
            return db;
        }
    }
}