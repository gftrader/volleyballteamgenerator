﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Rhino.Mocks;
using StructureMap.AutoMocking;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Repository.EF.Repositories;

namespace VolleyballTeamGenerator.Repository.EF.Tests.Repositories
{
    public class PlayerRepositoryTests_Base
    {
        protected RhinoAutoMocker<PlayerRepository> autoMocker;
        protected IDbSet<Player> allFakePlayers;

        [SetUp]
        protected virtual void SetupTests()
        {
            autoMocker = new RhinoAutoMocker<PlayerRepository>();
            allFakePlayers = GetAllFakePlayers();
            autoMocker.Get<IDbContext>().Stub(c => c.Set<Player>()).Return(allFakePlayers);
        }

        protected IDbSet<Player> GetAllFakePlayers()
        {
            var db = new FakeDbSet<Player>();
            db.Add(new Player()
            {
                Id = 1,
                FirstName = "Danaerys",
                LastName = "Targaryen",
                PlayerGroups = new List<PlayerGroup>() { 
                    new PlayerGroup(){ Id = 1 },
                    new PlayerGroup(){ Id = 2 }
                },
                Gender = "F",
                Email = "danaerys.targaryen@got.com"
            });
            db.Add(new Player()
            {
                Id = 2,
                FirstName = "Jon",
                LastName = "Snow",
                Gender = "M",
                Email = "jon.snow@got.com",
                PlayerGroups = new List<PlayerGroup>() { 
                    new PlayerGroup(){ Id = 2 },
                    new PlayerGroup(){ Id = 3 }
                }
            });
            db.Add(new Player()
            {
                Id = 3,
                FirstName = "Robert",
                LastName = "Barathian",
                Gender = "M",
                Email = "robert.barathian@got.com",
                PlayerGroups = new List<PlayerGroup>() { 
                    new PlayerGroup(){ Id = 3 },
                    new PlayerGroup(){ Id = 4 }
                }
            });
            return db;
        }
    }
}
