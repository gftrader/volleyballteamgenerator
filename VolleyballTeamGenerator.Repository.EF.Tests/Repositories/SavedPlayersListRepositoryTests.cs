﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Rhino.Mocks;
using StructureMap.AutoMocking;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Repository.EF.Repositories;

namespace VolleyballTeamGenerator.Repository.EF.Tests.Repositories
{
    [TestFixture]
    public class SavedPlayersListRepositoryTests
    {
        private RhinoAutoMocker<SavedPlayersListRepository> autoMocker;
        private IDbSet<SavedPlayersList> allFakeSavedPlayersLists;

        [SetUp]
        public void SetupTests()
        {
            autoMocker = new RhinoAutoMocker<SavedPlayersListRepository>();
            allFakeSavedPlayersLists = GetAllFakeSavedPlayersLists();
            autoMocker.Get<IDbContext>().Stub(c => c.Set<SavedPlayersList>()).Return(allFakeSavedPlayersLists);
        }

        [Test]
        public void FindByName_NameFound_ReturnsExpectedSavedPlayersList()
        {
            var result = autoMocker.ClassUnderTest.FindByGroupIdAndName(allFakeSavedPlayersLists.FirstOrDefault().PlayerGroupId, allFakeSavedPlayersLists.FirstOrDefault().Name);

            Assert.AreEqual(allFakeSavedPlayersLists.FirstOrDefault().Id, result.Id);
        }

        [Test]
        public void FindByName_NameNotFound_ReturnsNull()
        {
            var result = autoMocker.ClassUnderTest.FindByGroupIdAndName(-3, "");

            Assert.IsNull(result);
        }

        [Test]
        public void FindById_IdFound_ReturnsExpectedSavedPlayersList()
        {
            var fakeID = allFakeSavedPlayersLists.FirstOrDefault().Id;

            var result = autoMocker.ClassUnderTest.FindById(fakeID);

            Assert.AreEqual(fakeID, result.Id);
        }

        [Test]
        public void FindById_IdNotFound_ReturnsNull()
        {
            var fakeID = -1;

            var result = autoMocker.ClassUnderTest.FindById(fakeID);

            Assert.IsNull(result);
        }

        [Test]
        public void FindByPlayerGroupId_IdFound_ReturnsExpectedSavedPlayersLists()
        {
            var fakePlayerGroupId = allFakeSavedPlayersLists.FirstOrDefault().PlayerGroupId;

            var results = autoMocker.ClassUnderTest.FindByPlayerGroupId(fakePlayerGroupId);

            Assert.IsTrue(results.Count > 0);
            Assert.IsTrue(results.All(l => l.PlayerGroupId == fakePlayerGroupId));
        }

        private IDbSet<SavedPlayersList> GetAllFakeSavedPlayersLists()
        {
            var db = new FakeDbSet<SavedPlayersList>();
            db.Add(new SavedPlayersList() { Id = 1, Name = "List 1" });
            db.Add(new SavedPlayersList() { Id = 2, Name = "List 2" });
            db.Add(new SavedPlayersList() { Id = 3, Name = "List 3" });
            return db;
        }
    }
}