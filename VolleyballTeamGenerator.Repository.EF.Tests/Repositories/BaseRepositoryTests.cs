﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Rhino.Mocks;
using StructureMap.AutoMocking;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Repository.EF.Repositories;

namespace VolleyballTeamGenerator.Repository.EF.Tests.Repositories
{
    [TestFixture]
    public class BaseRepositoryTests
    {
        private RhinoAutoMocker<BaseRepositoryTestClass> autoMocker;
        private IDbSet<Player> allFakePlayers;

        [SetUp]
        public void SetupTests()
        {
            autoMocker = new RhinoAutoMocker<BaseRepositoryTestClass>();
            allFakePlayers = GetAllFakePlayers();
            autoMocker.Get<IDbContext>().Stub(c => c.Set<Player>()).Return(allFakePlayers);
        }

        [Test]
        public void FindAll_Always_ReturnsAllEntitiesInSet()
        {
            var results = autoMocker.ClassUnderTest.FindAll();

            Assert.IsTrue(results.Count > 0);
            foreach(var result in results)
            {
                Assert.IsTrue(allFakePlayers.Any(p => p.Id == result.Id));
            }
        }

        [Test]
        public void Add_Always_CallsRegisterNew()
        {
            var fakePlayer = new Player();

            autoMocker.ClassUnderTest.Add(fakePlayer);

            autoMocker.Get<IUnitOfWork>().AssertWasCalled(uow => uow.RegisterNew(Arg<Player>.Is.Anything, Arg<IUnitOfWorkRepository<Player>>.Is.Anything));
        }

        [Test]
        public void Save_Always_CallsRegisterAmmended()
        {
            autoMocker.ClassUnderTest.Save(allFakePlayers.FirstOrDefault());

            autoMocker.Get<IUnitOfWork>().AssertWasCalled(uow => uow.RegisterAmmended(Arg<Player>.Is.Anything, Arg<IUnitOfWorkRepository<Player>>.Is.Anything));
        }

        [Test]
        public void Remove_Always_CallsRegisterRemoved()
        {
            autoMocker.ClassUnderTest.Remove(allFakePlayers.FirstOrDefault());

            autoMocker.Get<IUnitOfWork>().AssertWasCalled(uow => uow.RegisterRemoved(Arg<Player>.Is.Anything, Arg<IUnitOfWorkRepository<Player>>.Is.Anything));
        }

        [Test]
        public void PersistCreationOf_Always_CallsAddOnContext()
        {
            autoMocker.ClassUnderTest.PersistCreationOf(allFakePlayers.FirstOrDefault());

            autoMocker.Get<IDbContext>().AssertWasCalled(dbc => dbc.Set<Player>().Add(allFakePlayers.FirstOrDefault()));
        }

        [Test]
        public void PersistDeletionOf_Always_CallsRemoveOnContext()
        {
            autoMocker.ClassUnderTest.PersistCreationOf(allFakePlayers.FirstOrDefault());

            autoMocker.Get<IDbContext>().AssertWasCalled(dbc => dbc.Set<Player>().Remove(allFakePlayers.FirstOrDefault()));
        }

        private IDbSet<Player> GetAllFakePlayers()
        {
            return new FakeDbSet<Player>()
            {
                new Player(){ Id = 1 },
                new Player(){ Id = 2 }
            };
        }

        private class BaseRepositoryTestClass : BaseRepository<Player, int>
        {
            public BaseRepositoryTestClass(EFUnitOfWork uow) : base(uow) { }

            public override Player FindById(int id)
            {
                throw new NotImplementedException();
            }
        }
    }
}
