﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Rhino.Mocks;
using StructureMap.AutoMocking;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Repository.EF.Repositories;
using VolleyballTeamGenerator.Domain.Messages;

namespace VolleyballTeamGenerator.Repository.EF.Tests.Repositories
{
    [TestFixture]
    public class PlayerRepositoryTests_FindPlayersTests : PlayerRepositoryTests_Base
    {
        [Test]
        public void FindPlayers_FirstNamePresent_ReturnsAllPlayersMatchingFirstName()
        {
            var fakeFirstName = allFakePlayers.FirstOrDefault().FirstName;

            var results = autoMocker.ClassUnderTest.FindPlayers(new FindPlayersRequest() { FirstName = fakeFirstName });

            Assert.IsTrue(results.Count > 0);
            Assert.IsTrue(results.All(p => p.FirstName.Equals(fakeFirstName)));
        }

        [Test]
        public void FindPlayers_LastNamePresent_ReturnsAllPlayersMatchingLastName()
        {
            var fakeLastName = allFakePlayers.FirstOrDefault().LastName;

            var results = autoMocker.ClassUnderTest.FindPlayers(new FindPlayersRequest() { LastName = fakeLastName });

            Assert.IsTrue(results.Count > 0);
            Assert.IsTrue(results.All(p => p.LastName.Equals(fakeLastName)));
        }

        [Test]
        public void FindPlayers_GenderPresent_ReturnsAllPlayersMatchingGender()
        {
            var fakeGender = allFakePlayers.FirstOrDefault().Gender;

            var results = autoMocker.ClassUnderTest.FindPlayers(new FindPlayersRequest() { Gender = fakeGender });

            Assert.IsTrue(results.Count > 0);
            Assert.IsTrue(results.All(p => p.Gender.Equals(fakeGender)));
        }

        [Test]
        public void FindPlayers_EmailPresent_ReturnsAllPlayersMatchingEmail()
        {
            var fakeEmail = allFakePlayers.FirstOrDefault().Email;

            var results = autoMocker.ClassUnderTest.FindPlayers(new FindPlayersRequest() { Email = fakeEmail });

            Assert.IsTrue(results.Count > 0);
            Assert.IsTrue(results.All(p => p.Email.Equals(fakeEmail)));
        }

        [Test]
        public void FindPlayers_PlayerIdsPresent_ReturnsAllPlayersMatchingPlayerIds()
        {
            var fakePlayerSublist = allFakePlayers.Take(2);
            var fakePlayerIds = fakePlayerSublist.Select(p => p.Id).ToList();

            var results = autoMocker.ClassUnderTest.FindPlayers(new FindPlayersRequest() { PlayerIds = fakePlayerIds });

            Assert.AreEqual(fakePlayerSublist.Count(), results.Count);
            foreach (var result in results)
            {
                Assert.IsTrue(fakePlayerSublist.Any(p => p.Id == result.Id));
            }
        }

        [Test]
        public void FindPlayers_PlayerGroupIdsPresent_ReturnsAllPlayersMatchingPlayerGroupIds()
        {
            var fakePlayerGroupIds = allFakePlayers.FirstOrDefault().PlayerGroups.Select(pg => pg.Id).ToList();

            var results = autoMocker.ClassUnderTest.FindPlayers(new FindPlayersRequest() { PlayerGroupIds = fakePlayerGroupIds });

            Assert.IsTrue(results.Count > 0);
            foreach (var result in results)
            {
                Assert.IsTrue(result.PlayerGroups.Any(pg => fakePlayerGroupIds.Contains(pg.Id)));
            }
        }
    }
}