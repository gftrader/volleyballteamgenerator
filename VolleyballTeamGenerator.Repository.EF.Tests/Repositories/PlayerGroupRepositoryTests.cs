﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Rhino.Mocks;
using StructureMap.AutoMocking;
using VolleyballTeamGenerator.Infrastructure;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Repository.EF.Repositories;

namespace VolleyballTeamGenerator.Repository.EF.Tests.Repositories
{
    [TestFixture]
    public class PlayerGroupRepositoryTests
    {
        private RhinoAutoMocker<PlayerGroupRepository> autoMocker;
        private IDbSet<PlayerGroup> allFakePlayerGroups;

        [SetUp]
        public void SetupTests()
        {
            autoMocker = new RhinoAutoMocker<PlayerGroupRepository>();
            allFakePlayerGroups = GetAllFakePlayerGroups();
            autoMocker.Get<IDbContext>().Stub(c => c.Set<PlayerGroup>()).Return(allFakePlayerGroups);
        }

        [Test]
        public void FindById_IdFound_ReturnsExpectedAttribute()
        {
            var fakeID = allFakePlayerGroups.FirstOrDefault().Id;

            var result = autoMocker.ClassUnderTest.FindById(fakeID);

            Assert.AreEqual(fakeID, result.Id);
        }

        [Test]
        public void FindById_IdNotFound_ReturnsNull()
        {
            var fakeID = -1;

            var result = autoMocker.ClassUnderTest.FindById(fakeID);

            Assert.IsNull(result);
        }

        private IDbSet<PlayerGroup> GetAllFakePlayerGroups()
        {
            var db = new FakeDbSet<PlayerGroup>();
            db.Add(new PlayerGroup() { Id = 1, Name = "Group 1" });
            db.Add(new PlayerGroup() { Id = 2, Name = "Group 2" });
            db.Add(new PlayerGroup() { Id = 3, Name = "Group 3" });
            return db;
        }
    }
}