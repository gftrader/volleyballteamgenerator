﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Rhino.Mocks;
using StructureMap.AutoMocking;
using VolleyballTeamGenerator.Domain.Models;
using VolleyballTeamGenerator.Domain.RepositoryInterfaces;
using VolleyballTeamGenerator.Domain.Services.Implementations;

namespace VolleyballTeamGenerator.Domain.Tests.Services
{
    [TestFixture]
    public class PlayerGroupServiceTests
    {
        private RhinoAutoMocker<PlayerGroupService> autoMocker;
        private PlayerGroup fakePlayerGroup;

        [SetUp]
        public void SetupTests()
        {
            autoMocker = new RhinoAutoMocker<PlayerGroupService>();
            autoMocker.Get<IPlayerGroupRepository>().Stub(pgr => pgr.FindById(fakePlayerGroup.Id)).Return(fakePlayerGroup);
            fakePlayerGroup = GetFakePlayerGroup();
        }

        public void GetPlayerGroup_Always_CallsFindById()
        {
            autoMocker.ClassUnderTest.GetPlayerGroup(fakePlayerGroup.Id);

            autoMocker.Get<IPlayerGroupRepository>().AssertWasCalled(pgr => pgr.FindById(fakePlayerGroup.Id));
        }

        public void GetAllPlayerGroups_Always_CallsFindAll()
        {
            autoMocker.ClassUnderTest.GetAllPlayerGroups();

            autoMocker.Get<IPlayerGroupRepository>().AssertWasCalled(pgr => pgr.FindAll());
        }

        public void GetPlayersInPlayerGroup_PlayerGroupFoundInSystem_ReturnsPlayersInGroup()
        {
            var results = autoMocker.ClassUnderTest.GetPlayersInPlayerGroup(fakePlayerGroup.Id);

            Assert.AreEqual(fakePlayerGroup.Players.Count, results.Count);
            foreach(var player in results)
            {
                Assert.IsTrue(fakePlayerGroup.Players.Any(p => p.Id == player.Id));
            }
        }

        [ExpectedException (typeof(NullReferenceException))]
        public void GetPlayersInPlayerGroup_PlayerGroupNotFound_ThrowsNullReferenceException()
        {
            autoMocker.ClassUnderTest.GetPlayersInPlayerGroup(fakePlayerGroup.Id);
        }

        private PlayerGroup GetFakePlayerGroup()
        {
            return new PlayerGroup()
            {
                Id = 1,
                Players = new List<Player>()
                {
                    new Player(){ Id = 1 },
                    new Player(){ Id = 2 }
                }
            };
        }
    }
}
